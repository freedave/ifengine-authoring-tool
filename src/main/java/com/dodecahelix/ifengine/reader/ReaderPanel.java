/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.reader;

import java.awt.Dimension;

import javax.swing.JSplitPane;

import com.alee.laf.panel.WebPanel;
import com.alee.laf.splitpane.WebSplitPane;

public class ReaderPanel extends WebPanel {

    private static final long serialVersionUID = 390260111712130523L;

    private StoryAppPanel storyAppPanel;

    public ReaderPanel() {

        storyAppPanel = new StoryAppPanel();
        AdPanel adPanel = new AdPanel();

        WebSplitPane splitPane = new WebSplitPane(JSplitPane.HORIZONTAL_SPLIT, storyAppPanel, adPanel);
        splitPane.setOneTouchExpandable(false);
        splitPane.setDividerLocation(600);

        // Provide minimum sizes for the two components in the split pane
        storyAppPanel.setMinimumSize(new Dimension(600, 50));
        adPanel.setMinimumSize(new Dimension(200, 50));

        // don't let it expand
        storyAppPanel.setMaximumSize(new Dimension(600, 50));

        this.add(splitPane);
    }

    public void exit() {
        // properly dispose of the GDX reader app
        storyAppPanel.exit();
    }

}
