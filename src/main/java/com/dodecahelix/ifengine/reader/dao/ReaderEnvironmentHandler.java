/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.reader.dao;

import com.dodecahelix.ifengine.data.serializer.JsonStoryBundle;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.LoadEnvironmentEvent;
import com.dodecahelix.ifengine.main.Events;
import com.google.common.eventbus.Subscribe;
import com.google.gson.Gson;

public class ReaderEnvironmentHandler {

    static final Logger logger = LoggerFactory.getLogger(ReaderEnvironmentHandler.class.getName());

    private Environment environment;

    public ReaderEnvironmentHandler() {
        Events.getInstance().getBus().register(this);
    }

    @Subscribe
    public void loadEnvironment(LoadEnvironmentEvent event) {
        logger.info("loaded environment into reader");
        this.environment = event.getEnvironment();
    }

    public StoryBundle loadStoryFile(String storyName) throws PlatformException {
        StoryBundle story = loadStory();
        return story;
    }

    private StoryBundle loadStory() {
        Gson gson = new Gson();
        String jsonEnv = gson.toJson(environment);
        return new JsonStoryBundle(jsonEnv);
    }

}
