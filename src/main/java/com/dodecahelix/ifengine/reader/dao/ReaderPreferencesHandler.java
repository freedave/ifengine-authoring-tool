/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.reader.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map.Entry;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.main.PreferencesHandler;
import com.dodecahelix.libgdx.template.config.UserPreferences;

public class ReaderPreferencesHandler {

    static final Logger logger = LoggerFactory.getLogger(PreferencesHandler.class.getName());
    private static final String READER_PROPERTIES_FILE_NAME = "reader.properties";

    public ReaderPreferencesHandler() {
    }

    public UserPreferences loadReaderPreferences() {

        UserPreferences prefs = new UserPreferences();
        try {
            File file = getPreferencesFile();
            FileReader reader = new FileReader(file);
            if (reader != null) {
                Properties props = new Properties();
                props.load(reader);

                reader.close();

                for (Entry<Object, Object> property : props.entrySet()) {
                    String propertyName = property.getKey().toString();
                    String propertyType = propertyName.substring(0, 3);
                    propertyName = propertyName.substring(4);

                    if ("bln".equalsIgnoreCase(propertyType)) {
                        prefs.getBooleanPreferences().put(propertyName, getBoolean(property));
                    }
                    if ("str".equalsIgnoreCase(propertyType)) {
                        prefs.getStringPreferences().put(propertyName, property.getValue().toString());
                    }
                    if ("int".equalsIgnoreCase(propertyType)) {
                        prefs.getIntegerPreferences().put(propertyName, getInteger(property));
                    }
                    if ("flt".equalsIgnoreCase(propertyType)) {
                        prefs.getFloatPreferences().put(propertyName, getFloat(property));
                    }
                    if ("sit".equalsIgnoreCase(propertyType)) {
                        prefs.getScalableIntegerPreferences().put(propertyName, getInteger(property));
                    }
                    if ("sft".equalsIgnoreCase(propertyType)) {
                        prefs.getScalableFloatPreferences().put(propertyName, getFloat(property));
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return prefs;
    }

    private Integer getInteger(Entry<Object, Object> property) {
        Integer intVal = null;
        try {
            intVal = Integer.parseInt(property.getValue().toString());
        } catch (NumberFormatException e) {
            // not an int.  could be a float, string or boolean
        }
        return intVal;
    }

    private Float getFloat(Entry<Object, Object> property) {
        Float floatVal = null;
        try {
            floatVal = Float.parseFloat(property.getValue().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return floatVal;
    }

    private Boolean getBoolean(Entry<Object, Object> property) {
        Boolean boolVal = null;
        try {
            boolVal = Boolean.parseBoolean(property.getValue().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return boolVal;
    }

    public void saveUserPreferences(UserPreferences preferences) {

        try {
            Properties props = new Properties();

            for (Entry<String, Boolean> preference : preferences.getBooleanPreferences().entrySet()) {
                props.setProperty("bln." + preference.getKey(), preference.getValue().toString());
            }
            for (Entry<String, String> preference : preferences.getStringPreferences().entrySet()) {
                props.setProperty("str." + preference.getKey(), preference.getValue().toString());
            }
            for (Entry<String, Integer> preference : preferences.getIntegerPreferences().entrySet()) {
                props.setProperty("int." + preference.getKey(), preference.getValue().toString());
            }
            for (Entry<String, Integer> preference : preferences.getScalableIntegerPreferences().entrySet()) {
                props.setProperty("sit." + preference.getKey(), preference.getValue().toString());
            }
            for (Entry<String, Float> preference : preferences.getFloatPreferences().entrySet()) {
                props.setProperty("flt." + preference.getKey(), preference.getValue().toString());
            }
            for (Entry<String, Float> preference : preferences.getScalableFloatPreferences().entrySet()) {
                props.setProperty("sft." + preference.getKey(), preference.getValue().toString());
            }

            File f = getPreferencesFile();
            OutputStream out = new FileOutputStream(f);
            props.store(out, null);

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *   Gets the preferences file from the clickfic user folder,
     *   building out the path and file if it doesnt already exist
     *
     * @return
     * @throws IOException
     */
    private File getPreferencesFile() throws IOException {
        // load prefs from user.dir, if it exists
        String userDir = System.getProperty("user.home");

        // does .ifebuilder directory exist?
        File clickficDir = new File(userDir + System.getProperty("file.separator") + PreferencesHandler.CLICKFIC_DIR);
        if (!clickficDir.exists()) {
            logger.debug("preferences directory not found.  creating.");
            clickficDir.mkdir();
        }

        File prefsFile = new File(clickficDir.getAbsolutePath() + System.getProperty("file.separator") + READER_PROPERTIES_FILE_NAME);
        if (!prefsFile.exists()) {
            logger.debug("preferences file not found.  creating.");
            prefsFile.createNewFile();
        }

        logger.debug("prefs file found at location " + prefsFile.getAbsolutePath());

        return prefsFile;
    }
}
