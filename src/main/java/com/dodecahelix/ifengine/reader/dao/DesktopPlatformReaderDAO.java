/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.reader.dao;

import java.util.List;

import com.dodecahelix.clickfic.PlatformDAO;
import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.clickfic.model.book.StoryQuery;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.choice.ChoiceRecord;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.UserPreferences;

public class DesktopPlatformReaderDAO implements PlatformDAO {

    private ReaderEnvironmentHandler environmentHandler = new ReaderEnvironmentHandler();
    private SaveProgressHandler saveProgressHandler = new JsonSaveProgressHandler();
    private LibraryHandler libraryHandler = new FileBasedLibraryHandler();
    private ReaderPreferencesHandler userPreferencesHandler = new ReaderPreferencesHandler();

    @Override
    public boolean isMobile() {
        return false;
    }

    @Override
    public StoryBundle loadStoryFile(String storyName) throws PlatformException {
        return environmentHandler.loadStoryFile(storyName);
    }

    @Override
    public EnvironmentSave restoreSavedProgress(String name) throws PlatformException {
        return saveProgressHandler.restoreSavedProgress(name);
    }

    @Override
    public void saveProgress(EnvironmentSave environmentSave) throws PlatformException {
        saveProgressHandler.saveProgress(environmentSave);
    }

    @Override
    public UserPreferences loadUserPreferences() {
        return userPreferencesHandler.loadReaderPreferences();
    }

    @Override
    public void saveUserPreferences(UserPreferences preferences) {
        userPreferencesHandler.saveUserPreferences(preferences);
    }

    @Override
    public List<LibraryCard> retrieveAdditionalStories(StoryQuery query) {
        return libraryHandler.queryForAdditonalStories(query);
    }

    @Override
    public void saveChoiceRecord(List<ChoiceRecord> record) throws PlatformException  {
        saveProgressHandler.saveTurnRecord(record);
    }

    @Override
    public List<ChoiceRecord> getChoiceRecord() throws PlatformException {
        return saveProgressHandler.retrieveStoredTurnRecord();
    }

    @Override
    public void initializeProperties(Properties properties) throws PlatformException {
        properties.addBooleanProperty(PropertyName.DEBUG_MODE.name(), true);
    }


}
