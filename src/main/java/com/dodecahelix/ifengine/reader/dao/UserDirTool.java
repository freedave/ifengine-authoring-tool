/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.reader.dao;

import java.io.File;

public class UserDirTool {

    private static final String CLICKFIC_USER_DIR_PATH = System.getProperty("user.home")
    + System.getProperty("file.separator")
    + ".clickfic";

    public static final void checkForPrivateUserDir() {
        File file = new File(CLICKFIC_USER_DIR_PATH);
        if (!file.exists()) {
            file.mkdir();
        }
    }
}
