/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.reader.dao;

import java.util.List;

import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.choice.ChoiceRecord;

public interface SaveProgressHandler {

    /**
     *   Retrieves the save game from the filesystem
     *
     * @return
     * @throws PlatformException
     */
    public EnvironmentSave restoreSavedProgress(String saveFilename) throws PlatformException;

    /**
     *  Persists the save game to the filesystem
     *
     * @param environmentSave
     * @throws PlatformException
     */
    public void saveProgress(EnvironmentSave environmentSave) throws PlatformException;

    /**
     *   Persists the turn record to a file
     *
     * @param record
     * @throws PlatformException
     */
    public void saveTurnRecord(List<ChoiceRecord> record) throws PlatformException;

    public List<ChoiceRecord> retrieveStoredTurnRecord() throws PlatformException;

}
