/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.reader.dao;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.save.json.JsonEnvironmentSave;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.ifengine.editor.events.ErrorEvent;
import com.dodecahelix.ifengine.editor.events.LoadPreferencesEvent;
import com.dodecahelix.ifengine.editor.events.UpdatePreferenceEvent;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.main.PreferenceKey;
import com.dodecahelix.ifengine.choice.ChoiceRecord;
import com.dodecahelix.ifengine.util.IoUtils;
import com.dodecahelix.ifengine.util.StringUtils;
import com.google.common.eventbus.Subscribe;

public class JsonSaveProgressHandler implements SaveProgressHandler {

    static final Logger logger = LoggerFactory.getLogger(JsonSaveProgressHandler.class.getName());

    private String workingDir;

    public static final String SAVES_SUB_DIR = "saves";
    public static final String TURN_RECORD_FILENAME = "turns.log";

    public JsonSaveProgressHandler() {
        Events.getInstance().getBus().register(this);
    }

    /**
     *  Retrieves the EnvironmentSave from the filesystem
     */
    @Override
    public EnvironmentSave restoreSavedProgress(String saveFilename) throws PlatformException {
        try {
            File file = getSaveFile(saveFilename);
            String saveGame = IoUtils.readFileAsString(file);

            return new JsonEnvironmentSave(saveFilename, saveGame);
        } catch (IOException e) {
            e.printStackTrace();
            Events.getInstance().getBus().post(new ErrorEvent("Error restoring save game : " + e.getMessage()));
            return null;
        }
    }

    @Override
    public void saveProgress(EnvironmentSave environmentSave) throws PlatformException {
        PrintWriter out = null;
        try {
            File file = getSaveFile(environmentSave.getName());
            out = new PrintWriter(file);

            JsonEnvironmentSave jsonSaveGame = (JsonEnvironmentSave) environmentSave;
            out.println(jsonSaveGame.getContent());
        } catch (Exception e) {
            e.printStackTrace();
            Events.getInstance().getBus().post(new ErrorEvent("Error writing save file : " + e.getMessage()));
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    private File getSaveFile(String name) throws IOException {
        // load stories from working directory
        if (workingDir == null) {
            throw new IllegalStateException("cannot locate working directory for save files");
        }

        File savesDir = new File(workingDir
        + System.getProperty("file.separator") + SAVES_SUB_DIR);
        if (!savesDir.exists()) {
            savesDir.mkdir();
        }

        File saveGameFile = new File(savesDir.getAbsolutePath() + System.getProperty("file.separator") + name);
        if (!saveGameFile.exists()) {
            logger.debug("save game file not found.  creating.");
            saveGameFile.createNewFile();
        }

        logger.debug("save game file found at location " + saveGameFile.getAbsolutePath());
        return saveGameFile;
    }

    @Subscribe
    public void loadStoryDirectoryPreference(LoadPreferencesEvent event) {
        Properties prefs = event.getPreferences();
        workingDir = prefs.getProperty(PreferenceKey.FILE_DIRECTORY.getPropertyKey());
    }

    @Subscribe
    public void updateStoryDirectoryPreference(UpdatePreferenceEvent event) {
        if (StringUtils.equalsIgnoreCase(PreferenceKey.FILE_DIRECTORY.name(), event.getPreferenceKey())) {
            workingDir = event.getPreferenceValue();
        }
    }

    @Override
    public void saveTurnRecord(List<ChoiceRecord> record) throws PlatformException {

        PrintWriter out = null;
        try {
            File file = getSaveFile(TURN_RECORD_FILENAME);
            out = new PrintWriter(file);

            for (ChoiceRecord turn : record) {
                String turnRecord = null;
                if (turn.isDialog()) {
                    turnRecord = String.format("%s~%s", turn.getReferenceId(), turn.getPersonId());
                } else {
                    turnRecord = String.format("%s~%s~%s", turn.getReferenceId(), turn.getSubjectId(), turn.getTargetId());
                }
                out.println(turnRecord);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Events.getInstance().getBus().post(new ErrorEvent("Error writing turn record file : " + e.getMessage()));
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    @Override
    public List<ChoiceRecord> retrieveStoredTurnRecord() throws PlatformException {
        try {
            File file = getSaveFile(TURN_RECORD_FILENAME);
            if (file != null) {
                List<ChoiceRecord> turns = new ArrayList<ChoiceRecord>();
                List<String> stringTurnRecord = IoUtils.readFileAsStringList(file);

                for (String stringTurn : stringTurnRecord) {
                    String[] parts = stringTurn.split("~");
                    ChoiceRecord record = null;
                    if (parts.length == 2) {
                        // this is how we know it is a dialog - dialog id and person id only
                        record = new ChoiceRecord(parts[0], parts[1]);
                    } else {
                        // this is a command
                        record = new ChoiceRecord(parts[0], parts[1], parts[2]);
                    }
                    turns.add(record);
                }

                return turns;
            } else {
                return null;
            }

        } catch (IOException e) {
            e.printStackTrace();
            Events.getInstance().getBus().post(new ErrorEvent("Error restoring save game : " + e.getMessage()));
            return null;
        }
    }

}
