/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.reader.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.dodecahelix.ifengine.data.story.LibraryCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.clickfic.model.book.StoryQuery;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.LoadPreferencesEvent;
import com.dodecahelix.ifengine.editor.events.UpdatePreferenceEvent;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.main.PreferenceKey;
import com.dodecahelix.ifengine.util.IoUtils;
import com.dodecahelix.ifengine.util.StringUtils;
import com.google.common.eventbus.Subscribe;
import com.google.gson.Gson;

/**
 *   Reads the additonal stories from a local directory containing story files
 *
 */
public class FileBasedLibraryHandler implements LibraryHandler {

    private static final Logger logger = LoggerFactory.getLogger(FileBasedLibraryHandler.class.getName());

    private Gson gson;

    private String workingDir = null;

    public FileBasedLibraryHandler() {
        gson = new Gson();

        Events.getInstance().getBus().register(this);
    }

    @Override
    public List<LibraryCard> queryForAdditonalStories(StoryQuery query) {

        List<LibraryCard> stories = new ArrayList<LibraryCard>();

        // find all .JSON files in the clickfic story dir

        // does .ifebuilder directory exist?
        File clickficStoryDir = new File(workingDir);
        if (clickficStoryDir.exists()) {
            logger.debug("clickfic story directory found at location " + clickficStoryDir.getAbsolutePath());
            for (File file : clickficStoryDir.listFiles()) {
                LibraryCard libraryCard = getStoryMetadataFromFile(file);
                if (libraryCard != null) {
                    stories.add(libraryCard);
                }
            }
        }

        return stories;
    }

    private LibraryCard getStoryMetadataFromFile(File file) {
        LibraryCard libraryCard = null;
        try {
            String fileName = file.getName();
            if (!file.isDirectory() && fileName.endsWith(".json")) {
                String environmentString = IoUtils.readFileAsString(file);
                Environment environment = gson.fromJson(environmentString, Environment.class);
                libraryCard = environment.getLibraryCard();

                // dont add libraryCard if invalid
                if (libraryCard.getTitle() == null || libraryCard.getAuthor() == null
                || libraryCard.getVersion() == null || libraryCard.getSynopsis() == null) {
                    libraryCard = null;
                }
            }
        } catch (Exception e) {
            // if it fails, just dont add this to the library
            e.printStackTrace();
        }

        return libraryCard;
    }

    @Subscribe
    public void loadStoryDirectoryPreference(LoadPreferencesEvent event) {
        Properties prefs = event.getPreferences();
        workingDir = prefs.getProperty(PreferenceKey.FILE_DIRECTORY.getPropertyKey());
    }

    @Subscribe
    public void updateStoryDirectoryPreference(UpdatePreferenceEvent event) {
        if (StringUtils.equalsIgnoreCase(PreferenceKey.FILE_DIRECTORY.name(), event.getPreferenceKey())) {
            workingDir = event.getPreferenceValue();
        }
    }

}
