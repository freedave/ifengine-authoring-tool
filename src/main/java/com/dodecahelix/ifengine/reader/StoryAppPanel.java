/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.panel.WebPanel;
import com.alee.managers.popup.PopupManager;
import com.badlogic.gdx.backends.lwjgl.LwjglAWTCanvas;
import com.dodecahelix.clickfic.ClickFic;
import com.dodecahelix.clickfic.PlatformDAO;
import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.ifengine.editor.events.ErrorEvent;
import com.dodecahelix.ifengine.editor.events.SwapMainViewEvent;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.main.MainFrame;
import com.dodecahelix.ifengine.reader.dao.DesktopPlatformReaderDAO;
import com.google.common.eventbus.Subscribe;

public class StoryAppPanel extends WebPanel {

    private static final Logger log = LoggerFactory.getLogger(StoryAppPanel.class);

    private static final long serialVersionUID = -9023544842806201910L;
    private PlatformDAO platformDAO;

    private LwjglAWTCanvas canvas;

    public StoryAppPanel() {
        platformDAO = new DesktopPlatformReaderDAO();

        Events.getInstance().getBus().register(this);
    }

    @Subscribe
    public void swapViews(SwapMainViewEvent swapViewEvent) {
        if (MainFrame.READER_VIEW.equalsIgnoreCase(swapViewEvent.getView())) {
            updateStory();

            // (re)initialize the GDX canvas
            initReader();
        }
    }

    private void updateStory() {
        try {
            platformDAO.loadStoryFile(null);
        } catch (PlatformException e) {
            log.error("cannot load story", e);
            Events.getInstance().getBus().post(new ErrorEvent(e.getMessage()));
        }
    }

    private void initReader() {
        if (canvas != null) {
            log.info("exiting previous canvas");
            canvas.exit();
        }

        log.info("initializing ClickFic reader mode");
        this.removeAll();
        canvas = new LwjglAWTCanvas(new ClickFic(platformDAO));

        this.add(canvas.getCanvas());

        // This fixes a bug where the Toaster Notification hides the canvas
        // after a save notification popup
        PopupManager.hideAllPopups(this);
    }

    public void exit() {
        // need to stop() the canvas or exiting the app will keep it open
        try {
            if (canvas != null) {
                canvas.exit();
                canvas.stop();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
