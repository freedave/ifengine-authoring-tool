/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main;

import java.awt.CardLayout;

import com.alee.laf.panel.WebPanel;
import com.dodecahelix.ifengine.editor.events.SwapMainViewEvent;
import com.google.common.eventbus.Subscribe;

public class SwapViewHandler {

    private WebPanel basePanel;
    private CardLayout viewLayout;

    public SwapViewHandler(WebPanel basePanel, CardLayout viewLayout) {
        this.basePanel = basePanel;
        this.viewLayout = viewLayout;
    }

    @Subscribe
    public void swapViews(SwapMainViewEvent swapViewEvent) {
        viewLayout.show(basePanel, swapViewEvent.getView());
    }

}
