/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;

// packages not supported by Android, so pass on this one
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.ErrorEvent;
import com.dodecahelix.ifengine.editor.events.ToasterNotificationEvent;
import com.dodecahelix.ifengine.main.Events;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SaveHandler {

    static final Logger logger = LoggerFactory.getLogger(SaveHandler.class.getName());

    /**
     *   Saves a serialized version of the Environment in JSON format.  This is yet to be tested in
     *
     * @param environment
     * @param file
     */
    public static void saveEnvironmentToFile(Environment environment, File file, boolean pretty) {

        logger.debug("Saving story " + environment.getLibraryCard().getTitle() + " to file " + file);

        try {
            PrintWriter out = new PrintWriter(file);

            Gson gson = new Gson();
            if (pretty) {
                gson = new GsonBuilder().setPrettyPrinting().create();
            }
            String jsonEnv = gson.toJson(environment);
            out.println(jsonEnv);
            out.close();

            Events.getInstance().getBus().post(new ToasterNotificationEvent("Story has been saved."));
        } catch (Exception e) {
            e.printStackTrace();
            Events.getInstance().getBus().post(new ErrorEvent("Error saving file : " + e.getMessage()));
        }
    }

    /**
     *   Save a serialized version of the Environment in XML.  Requires XML annotations added to the model objects.
     *   (NOTE:  The javax.xml.bind packages are not currently supported in Android.)
     *
     * @param environment
     * @param file
     * @throws JAXBException
     * @throws IOException
     */
    @SuppressWarnings("unused")
    private void serializeUsingJaxb(Environment environment, File file) throws JAXBException, IOException {

        FileOutputStream fos = new FileOutputStream(file);

        JAXBContext context = JAXBContext.newInstance(Environment.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(environment, fos);

        fos.close();

    }

    /**
     *  Save a serialized version of the Environment in binary format.
     *
     *  <br>Story writers won't be able to edit this file by hand
     *  <br>Also, a little risky since saved files wont be easy to troubleshoot.
     *
     * @param environment
     * @param file
     * @throws JAXBException
     * @throws IOException
     */
    @SuppressWarnings("unused")
    private void serializeBasic(Environment environment, File file) throws JAXBException, IOException {

        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(environment);

        fos.close();

    }

}
