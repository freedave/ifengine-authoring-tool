/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.editor.events.LoadPreferencesEvent;
import com.dodecahelix.ifengine.editor.events.UpdatePreferenceEvent;
import com.google.common.eventbus.Subscribe;

public class PreferencesHandler {

    static final Logger logger = LoggerFactory.getLogger(PreferencesHandler.class.getName());

    public static final String CLICKFIC_DIR = ".clickfic";
    private static final String BUILDER_PROPERTIES_FILE_NAME = "builder.properties";

    public PreferencesHandler() {
        Events.getInstance().getBus().register(this);
    }

    public void load() {
        try {
            File prefsFile = getPreferencesFile();

            Properties prefs = new Properties();
            FileReader reader = new FileReader(prefsFile);
            prefs.load(reader);
            reader.close();

            logger.debug("loaded preferences");

            Events.getInstance().getBus().post(new LoadPreferencesEvent(prefs));
        } catch (IOException e) {
            // not fatal, but print a stack trace
            e.printStackTrace();
        }
    }

    @Subscribe
    public void updatePreference(UpdatePreferenceEvent updatePreferenceEvent) {
        String prefKey = updatePreferenceEvent.getPreferenceKey();
        String prefValue = updatePreferenceEvent.getPreferenceValue();

        logger.debug(String.format("updating preference %s : %s", prefKey, prefValue));

        try {
            File prefsFile = getPreferencesFile();

            Properties prefs = new Properties();
            FileReader reader = new FileReader(prefsFile);
            prefs.load(reader);
            reader.close();

            prefs.setProperty(prefKey, prefValue);

            FileOutputStream fis = new FileOutputStream(prefsFile);
            prefs.store(fis, "clickfic builder preferences");
            fis.close();

            Events.getInstance().getBus().post(new LoadPreferencesEvent(prefs));
        } catch (IOException e) {
            // not fatal, but print out stack trace
            e.printStackTrace();
        }
    }


    /**
     *   Gets the preferences file from the clickfic user folder,
     *   building out the path and file if it doesnt already exist
     *
     * @return
     * @throws IOException
     */
    private File getPreferencesFile() throws IOException {
        // load prefs from user.dir, if it exists
        String userDir = System.getProperty("user.home");

        // does .ifebuilder directory exist?
        File clickficDir = new File(userDir + System.getProperty("file.separator") + CLICKFIC_DIR);
        if (!clickficDir.exists()) {
            logger.debug("preferences directory not found.  creating.");
            clickficDir.mkdir();
        }

        File prefsFile = new File(clickficDir.getAbsolutePath() + System.getProperty("file.separator") + BUILDER_PROPERTIES_FILE_NAME);
        if (!prefsFile.exists()) {
            logger.debug("preferences file not found.  creating.");
            prefsFile.createNewFile();
        }

        logger.debug("prefs file found at location " + prefsFile.getAbsolutePath());

        return prefsFile;
    }

}
