/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main;

import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotificationPopup;
import com.alee.managers.popup.PopupManager;
import com.dodecahelix.ifengine.editor.events.ErrorEvent;
import com.dodecahelix.ifengine.editor.events.NotificationEvent;
import com.dodecahelix.ifengine.editor.events.ToasterNotificationEvent;
import com.google.common.eventbus.Subscribe;

public class MessageHandler {

    private JComponent frame;

    private ImageIcon toasterIcon;

    public MessageHandler(JComponent builderFrame) {
        this.frame = builderFrame;

        URL imgUrl = MessageHandler.class.getClassLoader().getResource("icons/page_save.png");
        toasterIcon = new ImageIcon(imgUrl);
    }

    @Subscribe
    public void handleErrors(ErrorEvent event) {
        String title = event.getTitle();
        if (title == null) {
            title = "Error";
        }
        WebOptionPane.showMessageDialog(frame, event.getErrorDisplay(), title, WebOptionPane.ERROR_MESSAGE);
    }

    @Subscribe
    public void handleNotifications(NotificationEvent event) {
        WebOptionPane.showMessageDialog(frame, event.getNotificationMessage());
    }

    @Subscribe
    public void handleToasterNotification(ToasterNotificationEvent event) {
        final WebNotificationPopup notificationPopup = new WebNotificationPopup();
        notificationPopup.setIcon(toasterIcon);
        notificationPopup.setDisplayTime(2000);

        WebLabel label = new WebLabel(event.getMessage());
        notificationPopup.setContent(new GroupPanel(label));

        // this fixes a bug - the StoryAppPanel has to hide the popups before it can be seen
        PopupManager.getPopupLayer(frame).setVisible(true);

        NotificationManager.showNotification(frame, notificationPopup);
    }

}
