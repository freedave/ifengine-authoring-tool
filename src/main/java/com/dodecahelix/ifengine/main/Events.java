/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main;

import com.google.common.eventbus.EventBus;

public class Events {

    private static Events instance = null;
    private EventBus guavaEventBus;

    protected Events() {
        // Exists only to defeat instantiation.
        guavaEventBus = new EventBus();
    }

    public static Events getInstance() {
        if (instance == null) {
            instance = new Events();
        }
        return instance;
    }

    public EventBus getBus() {
        return guavaEventBus;
    }

}
