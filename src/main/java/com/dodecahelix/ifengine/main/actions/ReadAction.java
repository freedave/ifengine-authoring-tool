/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.editor.events.SwapMainViewEvent;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.main.MainFrame;

public class ReadAction extends AbstractAction {

    static final Logger logger = LoggerFactory.getLogger(ReadAction.class.getName());

    private static final long serialVersionUID = 2345454741684439490L;

    public ReadAction() {
        super("Reader View");
    }

    public ReadAction(String actionText) {
        super(actionText);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        logger.debug("Switching to reader view");

        Events.getInstance().getBus().post(new SwapMainViewEvent(MainFrame.READER_VIEW));
    }

}
