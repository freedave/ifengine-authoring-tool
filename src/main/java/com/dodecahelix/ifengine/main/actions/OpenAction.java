/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Properties;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.filechooser.WebFileChooser;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.FixEnvironmentEvent;
import com.dodecahelix.ifengine.editor.events.LoadPreferencesEvent;
import com.dodecahelix.ifengine.editor.events.UpdatePreferenceEvent;
import com.dodecahelix.ifengine.editor.system.EnvironmentFileBuilder;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.main.PreferenceKey;
import com.google.common.eventbus.Subscribe;

public class OpenAction extends AbstractAction {

    static final Logger logger = LoggerFactory.getLogger(OpenAction.class.getName());

    private static final long serialVersionUID = 4182661695058950713L;

    private Component sourceComponent;

    private String workingDir;

    public OpenAction(Component sourceComponent) {
        super("Load Story...");
        this.sourceComponent = sourceComponent;

        Events.getInstance().getBus().register(this);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        logger.debug("Opening File...");

        //Create a file chooser
        final WebFileChooser fc = new WebFileChooser(workingDir);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setDialogTitle("Choose file to open");

        //In response to a button click:
        int returnVal = fc.showOpenDialog(sourceComponent);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            String filePath = file.getAbsolutePath();
            logger.debug("Opening : " + file.getName() + ".");

            Environment env = EnvironmentFileBuilder.buildEnvironmentFromFile(file);

            //Events.getInstance().getBus().post(new LoadEnvironmentEvent(env, filePath));
            // run environment fixes before loading into UI..
            Events.getInstance().getBus().post(new FixEnvironmentEvent(env, filePath));

            // update working dir preference
            File workingDir = file.getParentFile();
            if (workingDir.isDirectory()) {
                logger.debug("updating working directory preference");
                Events.getInstance().getBus().post(new UpdatePreferenceEvent(
                PreferenceKey.FILE_DIRECTORY.getPropertyKey(),
                workingDir.getAbsolutePath()));
            }
        } else {
            logger.debug("'Open File' command cancelled by user.");
        }
    }


    @Subscribe
    public void loadLastDirectoryPreference(LoadPreferencesEvent event) {
        Properties prefs = event.getPreferences();
        if (workingDir == null) {
            // no save location set
            workingDir = prefs.getProperty(PreferenceKey.FILE_DIRECTORY.getPropertyKey());
        }
    }

}
