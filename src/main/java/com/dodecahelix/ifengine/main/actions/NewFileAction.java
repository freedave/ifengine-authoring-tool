/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.editor.events.ClearEnvironmentEvent;
import com.dodecahelix.ifengine.main.Events;

public class NewFileAction extends AbstractAction {

    static final Logger logger = LoggerFactory.getLogger(NewFileAction.class.getName());

    private static final long serialVersionUID = 4182661695058950713L;

    public NewFileAction() {
        super("New");
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        logger.debug("Creating new file...");

        Events.getInstance().getBus().post(new ClearEnvironmentEvent());
    }

}
