/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.editor.events.AboutEvent;
import com.dodecahelix.ifengine.main.Events;

public class AboutAction extends AbstractAction {

    private static final long serialVersionUID = -1687362719130061088L;

    static final Logger logger = LoggerFactory.getLogger(AboutAction.class.getName());

    public AboutAction() {
        super("About..");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        logger.debug("Bringing up about dialog..");

        Events.getInstance().getBus().post(new AboutEvent());
    }
}
