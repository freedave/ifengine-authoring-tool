package com.dodecahelix.ifengine.main.actions;

import com.alee.laf.filechooser.WebFileChooser;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.*;
import com.dodecahelix.ifengine.editor.utils.DefaultEnvironmentBuilder;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.main.PreferenceKey;
import com.dodecahelix.ifengine.main.ix.ImportHandler;
import com.google.common.eventbus.Subscribe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Properties;

/**
 * Created on 10/10/2016.
 */
public class ImportEntitiesAction extends AbstractAction {

    static final Logger logger = LoggerFactory.getLogger(ImportEntitiesAction.class.getName());

    private Component sourceComponent;

    /**
     *  Maintain a reference to the working dir.
     *
     *  When importing a file, the file chooser will start here.
     */
    private String workingDir;

    /**
     *  Maintain a reference to the loaded environment, to update with the imported entities.
     */
    private Environment environment;

    public ImportEntitiesAction(Component sourceContainer) {
        super("Import Entities");
        this.sourceComponent = sourceContainer;

        Events.getInstance().getBus().register(this);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        logger.debug("Importing entities...");
        //Create a file chooser
        final WebFileChooser fc = new WebFileChooser(workingDir);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setDialogTitle("Choose file to import");

        //In response to a button click:
        int returnVal = fc.showOpenDialog(sourceComponent);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            String filePath = file.getAbsolutePath();
            logger.debug("Importing from : {}", file.getName());

            ImportHandler.importEnvironmentEntitiesFromFile(environment, file);

            Events.getInstance().getBus().post(new TreeReloadEvent());

            // TODO - should we save after this??
            //Events.getInstance().getBus().post(new SaveStoryEvent());
        } else {
            logger.debug("'Import Entities' command cancelled by user.");
        }
    }

    @Subscribe
    public void loadEnvironment(LoadEnvironmentEvent event) {
        this.environment = event.getEnvironment();
    }

    @Subscribe
    public void clearEnvironment(ClearEnvironmentEvent event) {
        this.environment = DefaultEnvironmentBuilder.build();
    }

    @Subscribe
    public void loadLastDirectoryPreference(LoadPreferencesEvent event) {
        Properties prefs = event.getPreferences();
        if (workingDir == null) {
            // no save location set
            workingDir = prefs.getProperty(PreferenceKey.FILE_DIRECTORY.getPropertyKey());
        }
    }
}
