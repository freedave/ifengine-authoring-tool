/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExitAction extends AbstractAction {

    static final Logger logger = LoggerFactory.getLogger(ExitAction.class.getName());

    private static final long serialVersionUID = 4182661695058950713L;

    private Component sourceComponent;

    public ExitAction(Component sourceContainer) {
        super("Exit");

        this.sourceComponent = sourceContainer;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        logger.debug("Exiting App...");

        while (!(sourceComponent instanceof JFrame)) {
            sourceComponent = sourceComponent.getParent();
        }
        ((JFrame) sourceComponent).dispose();
    }
}
