/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;

import java.io.File;
import java.util.Properties;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import com.dodecahelix.ifengine.main.utils.SaveHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.filechooser.WebFileChooser;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.ClearEnvironmentEvent;
import com.dodecahelix.ifengine.editor.events.LoadEnvironmentEvent;
import com.dodecahelix.ifengine.editor.events.LoadPreferencesEvent;
import com.dodecahelix.ifengine.editor.events.SaveStoryEvent;
import com.dodecahelix.ifengine.editor.events.UpdateCurrentFilePathEvent;
import com.dodecahelix.ifengine.editor.events.UpdatePreferenceEvent;
import com.dodecahelix.ifengine.editor.utils.DefaultEnvironmentBuilder;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.main.PreferenceKey;
import com.google.common.eventbus.Subscribe;

public class SaveAction extends AbstractAction {

    private static final long serialVersionUID = 4182661695058950713L;

    static final Logger logger = LoggerFactory.getLogger(SaveAction.class.getName());

    private Component sourceComponent;

    /**
     *   Maintain a reference to the environment, in order to save on request
     */
    private Environment environment;

    /**
     *   Maintain location where file was loaded in order to save it where it came from
     */
    private String saveLocation;

    private String workingDir;

    public SaveAction(Component sourceComponent) {
        super("Save");
        this.sourceComponent = sourceComponent;

        Events.getInstance().getBus().register(this);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        File file = null;
        if (saveLocation == null) {
            logger.debug("Saving File (no path, must execute a Save As)...");

            final WebFileChooser fc = new WebFileChooser(workingDir);
            fc.setDialogTitle("Choose path to save this file");

            //In response to a button click:
            int returnVal = fc.showSaveDialog(sourceComponent);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                file = fc.getSelectedFile();
                file = appendJsonExtension(file);
                logger.debug("saving new file to path " + file.getAbsolutePath());
            } else {
                logger.debug("'Save' command cancelled by user.");
            }
        } else {
            logger.debug("(re)saving file to path : " + saveLocation);
            file = new File(saveLocation);
        }

        if (file != null) {
            SaveHandler.saveEnvironmentToFile(environment, file, true);

            saveLocation = file.getAbsolutePath();

            workingDir = file.getParentFile().getAbsolutePath();
            Events.getInstance().getBus().post(new UpdatePreferenceEvent(
            PreferenceKey.FILE_DIRECTORY.getPropertyKey(),
            workingDir));
        }
    }

    /**
     *   Appends .json to the filename, if it is not already there
     *
     * @param file
     * @return
     */
    private File appendJsonExtension(File file) {
        String fileName = file.getName();

        if (!fileName.endsWith(".json")) {
            file = new File(file.getAbsolutePath() + ".json");
            logger.debug("renaming file to " + file.getName());
        }

        return file;
    }

    @Subscribe
    public void loadEnvironment(LoadEnvironmentEvent event) {
        this.environment = event.getEnvironment();

        this.saveLocation = event.getLoadLocation();
    }

    @Subscribe
    public void clearEnvironment(ClearEnvironmentEvent event) {
        this.environment = DefaultEnvironmentBuilder.build();

        // no save location, so we have to do a Save As..
        this.saveLocation = null;
    }

    @Subscribe
    public void updateSaveLocationPath(UpdateCurrentFilePathEvent event) {
        this.saveLocation = event.getNewPath();
    }

    @Subscribe
    public void saveStory(SaveStoryEvent saveEvent) {
        // external event to save the story
        this.actionPerformed(null);
    }

    @Subscribe
    public void loadLastDirectoryPreference(LoadPreferencesEvent event) {
        Properties prefs = event.getPreferences();
        if (workingDir == null) {
            // no save location set
            workingDir = prefs.getProperty(PreferenceKey.FILE_DIRECTORY.getPropertyKey());
        }
    }

}
