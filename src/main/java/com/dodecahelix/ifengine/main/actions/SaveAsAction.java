/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Properties;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;

import com.dodecahelix.ifengine.main.utils.SaveHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.filechooser.WebFileChooser;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.ClearEnvironmentEvent;
import com.dodecahelix.ifengine.editor.events.LoadEnvironmentEvent;
import com.dodecahelix.ifengine.editor.events.LoadPreferencesEvent;
import com.dodecahelix.ifengine.editor.events.UpdateCurrentFilePathEvent;
import com.dodecahelix.ifengine.editor.events.UpdatePreferenceEvent;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.main.PreferenceKey;
import com.google.common.eventbus.Subscribe;

public class SaveAsAction extends AbstractAction {

    private static final long serialVersionUID = 4182661695058950713L;

    static final Logger logger = LoggerFactory.getLogger(SaveAsAction.class.getName());

    private Component sourceComponent;
    private Environment environment;

    private String workingDir;

    public SaveAsAction(Component sourceComponent) {
        super("Save As...");

        this.sourceComponent = sourceComponent;
        Events.getInstance().getBus().register(this);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        logger.debug("Saving File (As)...");

        // Create a file chooser
        final WebFileChooser fc = new WebFileChooser(workingDir);
        fc.setDialogTitle("Choose path to save this file");

        // prepopulate default value - TODO : not working!
        //String defaultFileName = StoryFileNameUtil.getStoryFileName(environment.getStoryMetadata());
        //defaultFileName += ".json";
        //logger.debug("prepopulating file name: " + defaultFileName);
        //fc.setSelectedFile(defaultFileName);

        //In response to a button click:
        int returnVal = fc.showSaveDialog(sourceComponent);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            file = appendJsonExtension(file);
            logger.debug("saving new file to path " + file.getAbsolutePath());
            if (file != null) {
                // TODO - make pretty format configurable
                SaveHandler.saveEnvironmentToFile(environment, file, true);
            }

            // update working dir preference
            File workingDirFile = file.getParentFile();
            if (workingDirFile.isDirectory()) {
                logger.debug("updating working directory preference");

                workingDir = workingDirFile.getAbsolutePath();
                Events.getInstance().getBus().post(new UpdatePreferenceEvent(
                    PreferenceKey.FILE_DIRECTORY.getPropertyKey(),
                    workingDir));

                Events.getInstance().getBus().post(new UpdateCurrentFilePathEvent(file.getAbsolutePath()));
            }
        } else {
            logger.debug("'Save As' command cancelled by user.");
        }
    }

    private File appendJsonExtension(File file) {
        String fileName = file.getName();

        if (!fileName.endsWith("json")) {
            file = new File(file.getAbsolutePath() + ".json");
            logger.debug("renaming file to " + file.getName());
        }

        return file;
    }


    @Subscribe
    public void loadEnvironment(LoadEnvironmentEvent event) {
        this.environment = event.getEnvironment();
    }

    @Subscribe
    public void clearEnvironment(ClearEnvironmentEvent event) {
        this.environment = new Environment();
    }

    @Subscribe
    public void loadLastDirectoryPreference(LoadPreferencesEvent event) {
        Properties prefs = event.getPreferences();
        if (workingDir == null) {
            // no save location set
            workingDir = prefs.getProperty(PreferenceKey.FILE_DIRECTORY.getPropertyKey());
        }
    }

}
