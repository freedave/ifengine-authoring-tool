package com.dodecahelix.ifengine.main.actions;

import javax.swing.*;
import java.awt.*;

/**
 * Created on 6/14/2016.
 */
public class ActionMap {

    private Action buildAction;
    private Action readAction;
    private Action saveAction;
    private Action exitAction;
    private Action saveAsAction;
    private Action openAction;
    private Action newFileAction;
    private Action aboutAction;
    private Action importEntitiesAction;
    private Action exportEntitiesAction;

    public ActionMap(Component component) {
        buildAction = new BuildAction();
        readAction = new ReadAction();
        saveAction = new SaveAction(component);
        exitAction = new ExitAction(component);
        saveAsAction = new SaveAsAction(component);
        openAction = new OpenAction(component);
        newFileAction = new NewFileAction();
        aboutAction = new AboutAction();
        importEntitiesAction = new ImportEntitiesAction(component);
        exportEntitiesAction = new ExportEntitiesAction(component);
    }

    public Action getBuildAction() {
        return buildAction;
    }

    public Action getReadAction() {
        return readAction;
    }

    public Action getSaveAction() {
        return saveAction;
    }

    public Action getExitAction() {
        return exitAction;
    }

    public Action getSaveAsAction() {
        return saveAsAction;
    }

    public Action getOpenAction() {
        return openAction;
    }

    public Action getNewFileAction() {
        return newFileAction;
    }

    public Action getAboutAction() {
        return aboutAction;
    }

    public Action getImportEntitiesAction() {
        return importEntitiesAction;
    }

    public Action getExportEntitiesAction() {
        return exportEntitiesAction;
    }
}
