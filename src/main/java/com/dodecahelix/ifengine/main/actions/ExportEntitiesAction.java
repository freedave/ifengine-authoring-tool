package com.dodecahelix.ifengine.main.actions;

import com.alee.laf.filechooser.WebFileChooser;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.ClearEnvironmentEvent;
import com.dodecahelix.ifengine.editor.events.LoadEnvironmentEvent;
import com.dodecahelix.ifengine.editor.events.LoadPreferencesEvent;
import com.dodecahelix.ifengine.editor.utils.DefaultEnvironmentBuilder;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.main.PreferenceKey;
import com.dodecahelix.ifengine.main.ix.ExportHandler;
import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Properties;

/**
 *  Created on 10/10/2016.
 */
public class ExportEntitiesAction extends AbstractAction {

    static final Logger logger = LoggerFactory.getLogger(ExportEntitiesAction.class.getName());

    private Component sourceComponent;

    /**
     *  Maintain a reference to the loaded environment, to generate the export file.
     */
    private Environment environment;

    /**
     *  Maintain a reference to the working dir.
     *
     *  When exporting a file, the file chooser will start here.
     */
    private String workingDir;

    public ExportEntitiesAction(Component sourceContainer) {
        super("Export Entities");
        this.sourceComponent = sourceContainer;

        Events.getInstance().getBus().register(this);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        logger.debug("Exporting entities...");

        File file = null;
        logger.debug("Exporting entities to file...");

        final WebFileChooser fc = new WebFileChooser(workingDir);
        fc.setDialogTitle("Choose path to save the export file");

        //In response to a button click:
        int returnVal = fc.showSaveDialog(sourceComponent);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            file = fc.getSelectedFile();
            logger.debug("exporting entities to path " + file.getAbsolutePath());
        } else {
            logger.debug("'Export Entities' command cancelled by user.");
        }

        if (file != null) {
            ExportHandler.exportEnvironmentToFile(environment, file);
        }
    }

    @Subscribe
    public void loadEnvironment(LoadEnvironmentEvent event) {
        this.environment = event.getEnvironment();
    }

    @Subscribe
    public void clearEnvironment(ClearEnvironmentEvent event) {
        this.environment = DefaultEnvironmentBuilder.build();
    }

    @Subscribe
    public void loadLastDirectoryPreference(LoadPreferencesEvent event) {
        Properties prefs = event.getPreferences();
        if (workingDir == null) {
            // no save location set
            workingDir = prefs.getProperty(PreferenceKey.FILE_DIRECTORY.getPropertyKey());
        }
    }

}
