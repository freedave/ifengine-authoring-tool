/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main;

import javax.swing.SwingUtilities;

import com.alee.laf.WebLookAndFeel;
import com.dodecahelix.ifengine.editor.system.EntityIdChangeHandler;
import com.dodecahelix.ifengine.editor.system.EnvironmentFixer;

public class IfStoryBuildingApp {

    public static void main(String[] args) {
        // You should work with UI (including installing L&F) inside Event
        // Dispatch Thread (EDT)
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                // Install WebLaF as application L&F
                WebLookAndFeel.install();

                // Setting this kills the ReaderView
                //WebLookAndFeel.setDecorateAllWindows(true);

                // fire up any service layer objects that arent tied to the interface
                new EntityIdChangeHandler();
                new EnvironmentFixer();

                // create and show the interface
                MainFrame frame = new MainFrame();
                frame.setVisible(true);

                PreferencesHandler prefsHandler = new PreferencesHandler();
                prefsHandler.load();
            }
        });
    }
}
