/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.main;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.net.URL;

import javax.swing.ImageIcon;

import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.BuilderPanel;
import com.dodecahelix.ifengine.editor.about.AboutHandler;
import com.dodecahelix.ifengine.editor.events.LoadEnvironmentEvent;
import com.dodecahelix.ifengine.editor.menu.BuilderMenuBar;
import com.dodecahelix.ifengine.editor.menu.BuilderToolBar;
import com.dodecahelix.ifengine.editor.utils.DefaultEnvironmentBuilder;
import com.dodecahelix.ifengine.main.actions.ActionMap;
import com.dodecahelix.ifengine.reader.ReaderPanel;

public class MainFrame extends WebFrame {

    private static final long serialVersionUID = -6312842949750848020L;

    public static final String BUILDER_VIEW = "builder";
    public static final String READER_VIEW = "reader";

    private AboutHandler aboutHandler;
    private MessageHandler errorHandler;
    private SwapViewHandler swapViewHandler;
    private CardLayout viewLayout;
    private WebPanel appPanel;

    private ReaderPanel readerPanel;
    private BuilderPanel builderPanel;

    public MainFrame() {

        setTitle("ClickFic Authoring Tool");
        setSize(800, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        URL imgUrl = MainFrame.class.getClassLoader().getResource("icons/logo_32.png");
        this.setIconImage(new ImageIcon(imgUrl).getImage());

        ActionMap actionMap = new ActionMap(this);
        BuilderMenuBar menuBar = new BuilderMenuBar(actionMap);
        this.setJMenuBar(menuBar);

        BuilderToolBar toolBar = new BuilderToolBar(actionMap);
        this.getContentPane().add(toolBar, BorderLayout.NORTH);

        viewLayout = new CardLayout();
        appPanel = new WebPanel(viewLayout);
        readerPanel = new ReaderPanel();
        builderPanel = new BuilderPanel();

        appPanel.add(builderPanel, BUILDER_VIEW);
        appPanel.add(readerPanel, READER_VIEW);

        //this.setContentPane(appPanel);
        this.getContentPane().add(appPanel, BorderLayout.CENTER);

        registerBaseEvents();
        loadInitialEnvironment();
    }

    private void registerBaseEvents() {
        errorHandler = new MessageHandler(builderPanel);
        Events.getInstance().getBus().register(errorHandler);

        swapViewHandler = new SwapViewHandler(appPanel, viewLayout);
        Events.getInstance().getBus().register(swapViewHandler);

        aboutHandler = new AboutHandler(this);
        Events.getInstance().getBus().register(aboutHandler);
    }


    private void loadInitialEnvironment() {
        Environment env = DefaultEnvironmentBuilder.build();
        Events.getInstance().getBus().post(new LoadEnvironmentEvent(env, null));
    }

    @Override
    public void dispose() {
        // need to properly dispose GDX panel
        readerPanel.exit();

        super.dispose();
    }


}
