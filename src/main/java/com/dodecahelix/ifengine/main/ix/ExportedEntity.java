package com.dodecahelix.ifengine.main.ix;

import com.dodecahelix.ifengine.data.EntityType;

import java.util.ArrayList;
import java.util.List;

/**
 *  This represents the base attributes of an entity - ID, title, description.
 */
public class ExportedEntity {

    private EntityType type;
    private String entityId;
    private String entityTitle;
    private List<String> descriptionLines;

    // indicates that this entity belongs to another entity (i.e; an item owner, or the location for a mass)
    private String ownerEntityType;
    private String ownerEntityId;

    public ExportedEntity() {
        descriptionLines = new ArrayList<>();
    }

    public ExportedEntity(EntityType type, String entityId, String entityTitle) {
        this.type = type;
        this.entityId = entityId;
        this.entityTitle = entityTitle;

        descriptionLines = new ArrayList<>();
    }

    public EntityType getType() {
        return type;
    }

    public void setType(EntityType type) {
        this.type = type;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityTitle() {
        return entityTitle;
    }

    public void setEntityTitle(String entityTitle) {
        this.entityTitle = entityTitle;
    }

    public List<String> getDescriptionLines() {
        return descriptionLines;
    }

    public void setOwnerEntityType(String ownerEntityType) {
        this.ownerEntityType = ownerEntityType;
    }

    public void setOwnerEntityId(String forEntityId) {
        this.ownerEntityType = ownerEntityType;
    }

    public String getOwnerEntityType() {
        return ownerEntityType;
    }

    public String getOwnerEntityId() {
        return ownerEntityId;
    }

    public void setDescriptionLines(List<String> descriptionLines) {
        this.descriptionLines = descriptionLines;
    }

    public void addDescriptionLine(String descriptionLine) {
        descriptionLines.add(descriptionLine);
    }

    public String getDescriptionString() {
        StringBuffer description = new StringBuffer();
        for (String line: descriptionLines) {
            description.append(line);
            description.append("  ");
        }
        return description.toString().trim();
    }
}
