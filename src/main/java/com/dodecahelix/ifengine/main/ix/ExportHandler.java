package com.dodecahelix.ifengine.main.ix;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.ErrorEvent;
import com.dodecahelix.ifengine.editor.events.ToasterNotificationEvent;
import com.dodecahelix.ifengine.main.Events;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created on 10/10/2016.
 */
public class ExportHandler {

    static final Logger logger = LoggerFactory.getLogger(ExportHandler.class.getName());

    public static void exportEnvironmentToFile(Environment environment, File file) {
        logger.debug("Saving story " + environment.getLibraryCard().getTitle() + " to file " + file);

        try {
            PrintWriter out = new PrintWriter(file);
            List<String> exportedEntities = getExportedEntities(environment);
            for (String line : exportedEntities) {
                out.println(line);
            }
            out.close();

            Events.getInstance().getBus().post(new ToasterNotificationEvent("entities have been exported."));
        } catch (Exception e) {
            e.printStackTrace();
            Events.getInstance().getBus().post(new ErrorEvent("error exporting entities : " + e.getMessage()));
        }
    }

    private static List<String> getExportedEntities(Environment environment) {
        EntityExportTraversal traversal = new EntityExportTraversal();
        traversal.traverseEnvironment(environment);

        return traversal.getLines();
    }

}
