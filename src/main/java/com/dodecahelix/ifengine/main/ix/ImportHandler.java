package com.dodecahelix.ifengine.main.ix;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.*;
import com.dodecahelix.ifengine.editor.events.ErrorEvent;
import com.dodecahelix.ifengine.editor.events.ToasterNotificationEvent;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.util.IoUtils;
import com.dodecahelix.ifengine.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

/**
 * Created on 10/11/2016.
 */
public class ImportHandler {

    static final Logger logger = LoggerFactory.getLogger(ImportHandler.class.getName());

    /**
     * From an entity file, add new entities and update exisitng entities on the current Environment.
     *
     * If there is an Entity with the given ID, update it with the given description and title,
     * otherwise, create a new Entity of that type with the given description and title.
     *
     * @param environment
     * @param file
     * @return
     */
    public static Environment importEnvironmentEntitiesFromFile(Environment environment, File file) {
        try {
            List<String> entityLines = IoUtils.readFileAsStringList(file);

            // track the current entity -- it will be defined on multiple lines.
            int lineNum = 1;
            ExportedEntity currentEntity = null;
            for (String entityLine: entityLines) {
                if (!entityLine.trim().isEmpty()) {
                    logger.trace("parsing entity line {} : {}", lineNum, entityLine);
                    currentEntity = parseEntityFileLine(lineNum, entityLine, currentEntity, environment);
                }

                lineNum++;
            }

            Events.getInstance().getBus().post(new ToasterNotificationEvent("entities have been imported."));
        } catch (Exception e) {
            e.printStackTrace();
            Events.getInstance().getBus().post(new ErrorEvent("error importing entities : " + e.getMessage()));
        }

        return environment;
    }

    /**
     * This parses out and interprets a line in the exported entity file.
     *
     * @param lineNum
     * @param entityLine
     * @param currentEntity
     * @param environment
     * @return
     */
    private static ExportedEntity parseEntityFileLine(int lineNum, String entityLine, ExportedEntity currentEntity, Environment environment) {

        // whether or not this line is a new entity
        boolean newEntity = false;
        for (EntityType type: EntityType.values()) {
            if (StringUtils.startsWithIgnoreCase(type.name(), entityLine)) {
                newEntity = true;
                logger.debug("importing entity: {}", entityLine);

                // Save the old entity to the environment and start a new base.
                if (currentEntity!=null) {
                    saveEntityToEnvironment(currentEntity, environment);
                }
                currentEntity = parseNewEntityFromLine(entityLine);
            }
        }

        if (!newEntity) {
            if (currentEntity==null) {
                throw new IllegalArgumentException("invalid entity file - first line in an entity file should be of format type:id:title");
            }

            if (entityLine.trim().startsWith("owner")) {
                parseOwnerLine(entityLine, currentEntity);
            } else {
                // this is a description line.  Add to the current entity.
                currentEntity.addDescriptionLine(entityLine);
            }
        }

        return currentEntity;
    }

    /**
     * Parse out the ownership line.
     *
     * @param entityLine
     * @param currentEntity
     */
    private static void parseOwnerLine(String entityLine, ExportedEntity currentEntity) {
        String locationForLine = entityLine.trim().substring(6);
        logger.debug(".. owned by {}", locationForLine);

        String[] ownerTypeAndId = locationForLine.split(":");
        currentEntity.setOwnerEntityType(ownerTypeAndId[0]);
        currentEntity.setOwnerEntityId(ownerTypeAndId[1]);
    }

    /**
     * Parse out the ExportedEntity from the new entity line.
     *
     * @param entityLine
     * @return
     */
    private static ExportedEntity parseNewEntityFromLine(String entityLine) {

        String[] entityParts = entityLine.split(":");
        if (entityParts.length!=3) {
            throw new IllegalArgumentException("");
        }
        String typePart = entityParts[0];
        String idPart = entityParts[1];
        String titlePart = entityParts[2];
        logger.debug("imported entity is type {}, with an id of {} and title {}", typePart, idPart, titlePart);

        EntityType type = EntityType.valueOf(typePart.toUpperCase());
        return new ExportedEntity(type, idPart, titlePart);
    }

    private static void saveEntityToEnvironment(ExportedEntity newEntity, Environment environment) {
        logger.debug("saving imported entity to environment");

        EnvironmentDAO dao = new SimpleEnvironmentDAO(environment, false);
        Entity entity = dao.getEntityById(newEntity.getType(), newEntity.getEntityId());
        if (entity!=null) {
            logger.debug("found existing entity with ID.  updating title and description.");
            // this is an existing entity.  Just set its title and description
            entity.setTitle(newEntity.getEntityTitle());
            entity.setDescription(newEntity.getDescriptionString());
        } else {
            logger.debug("creating new entity of type {}", newEntity.getType());

            // this is a new entity.  Add it to the Environment and add ownership information (if any)
            switch (newEntity.getType()) {
                //case ITEM: environment.addItem(new Item(newEntity.getEntityId(), ));
                case LOCATION: environment.addLocation(new Location(newEntity.getEntityId(), newEntity.getEntityTitle(), newEntity.getDescriptionString())); break;
                case ITEM: environment.addItem(new Item(newEntity.getEntityId(), newEntity.getEntityTitle(), newEntity.getDescriptionString())); break;
                case MASS: environment.addMass(new Mass(newEntity.getEntityId(), newEntity.getEntityTitle(), newEntity.getDescriptionString())); break;
                default: throw new IllegalArgumentException("cannot import an entity type of " + newEntity.getType());
            }

            // add ownership information (add a mass to a location, or an item as a content item)
            if (newEntity.getOwnerEntityType()!=null) {
                logger.debug("assigning ownership to {}:{}", newEntity.getOwnerEntityType(), newEntity.getOwnerEntityId());

                // add an ownership
                EntityType ownerType = EntityType.valueOf(newEntity.getOwnerEntityType().toUpperCase());
                Entity owner = dao.getEntityById(ownerType, newEntity.getOwnerEntityId());

                if (EntityType.MASS == newEntity.getType() && EntityType.LOCATION == ownerType) {
                    logger.debug("adding mass to location");
                    ((Location)owner).addMass(newEntity.getEntityId());
                }

                if (EntityType.ITEM == newEntity.getType()) {
                    logger.debug("adding content item");
                    owner.addContentItem(newEntity.getEntityId());
                }
            }

        }
    }
}
