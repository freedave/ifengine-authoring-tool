package com.dodecahelix.ifengine.main.ix;

import com.dodecahelix.ifengine.dao.traversal.DefaultEnvironmentTraversal;
import com.dodecahelix.ifengine.data.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 10/10/2016.
 */
public class EntityExportTraversal extends DefaultEnvironmentTraversal {

    private List<String> lines = new ArrayList<>();

    @Override
    public void processLocation(Location location) {
        super.processLocation(location);

        StringBuffer title = new StringBuffer(EntityType.LOCATION.name().toLowerCase());
        title.append(":");
        title.append(location.getId());
        title.append(":");
        title.append(location.getTitle());
        lines.add(title.toString());
        lines.add(location.getDescription());
        lines.add("");
    }

    @Override
    public void processItem(Item item) {
        super.processItem(item);

        // TODO - find the owner of this item and print a "for:" line

        StringBuffer title = new StringBuffer(EntityType.ITEM.name().toLowerCase());
        title.append(":");
        title.append(item.getId());
        title.append(":");
        title.append(item.getTitle());
        lines.add(title.toString());
        lines.add(item.getDescription());
        lines.add("");
    }

    @Override
    public void processPerson(Person person) {
        super.processPerson(person);

        StringBuffer title = new StringBuffer(EntityType.PERSON.name().toLowerCase());
        title.append(":");
        title.append(person.getId());
        title.append(":");
        title.append(person.getTitle());
        lines.add(title.toString());
        lines.add(person.getDescription());
        lines.add("");
    }

    @Override
    public void processNarrator(Reader narrator) {
        super.processNarrator(narrator);

        StringBuffer title = new StringBuffer(EntityType.NARRATOR.name().toLowerCase());
        title.append(":");
        title.append(narrator.getId());
        title.append(":");
        title.append(narrator.getTitle());
        lines.add(title.toString());
        lines.add(narrator.getDescription());
        lines.add("");
    }

    @Override
    public void processMass(Mass mass) {
        super.processMass(mass);

        // TODO - find the owner of this mass, and print a "for:" line

        StringBuffer title = new StringBuffer(EntityType.MASS.name().toLowerCase());
        title.append(":");
        title.append(mass.getId());
        title.append(":");
        title.append(mass.getTitle());
        lines.add(title.toString());
        lines.add(mass.getDescription());
        lines.add("");
    }

    public List<String> getLines() {
        return lines;
    }
}
