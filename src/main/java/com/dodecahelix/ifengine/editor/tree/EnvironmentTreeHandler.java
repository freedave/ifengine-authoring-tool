/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import com.dodecahelix.ifengine.editor.events.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.menu.WebPopupMenu;
import com.alee.laf.tree.WebTree;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.ModelType;
import com.dodecahelix.ifengine.editor.tree.build.TreeLoader;
import com.dodecahelix.ifengine.editor.tree.context.ContextMenuBuilder;
import com.dodecahelix.ifengine.editor.tree.context.FolderContextMenu;
import com.dodecahelix.ifengine.editor.tree.context.FolderContextMenuBuilder;
import com.dodecahelix.ifengine.editor.tree.context.ModelObjectContextMenu;
import com.dodecahelix.ifengine.editor.utils.DefaultEnvironmentBuilder;
import com.dodecahelix.ifengine.main.Events;
import com.google.common.eventbus.Subscribe;

public class EnvironmentTreeHandler implements TreeSelectionListener, MouseListener {

    static final Logger logger = LoggerFactory.getLogger(EnvironmentTreeHandler.class.getName());

    private WebTree<DefaultMutableTreeNode> tree;
    private DefaultTreeModel model;
    private Environment environment;
    private TreeLoader treeBuilder;

    private Map<ModelType, ModelObjectContextMenu> contextMenuMap;
    private Map<ModelType, FolderContextMenu> folderContextMenuMap;

    public EnvironmentTreeHandler(WebTree<DefaultMutableTreeNode> tree, DefaultTreeModel treeModel) {
        this.tree = tree;
        this.model = treeModel;

        this.treeBuilder = new TreeLoader(tree);

        // build the right-click context menu's for all the different tree nodes
        contextMenuMap = new HashMap<ModelType, ModelObjectContextMenu>();
        folderContextMenuMap = new HashMap<ModelType, FolderContextMenu>();
        for (ModelType modelType : ModelType.values()) {
            ModelObjectContextMenu contextMenu = new ModelObjectContextMenu();
            ContextMenuBuilder.addMenuForModelType(contextMenu, modelType);
            contextMenuMap.put(modelType, contextMenu);

            FolderContextMenu folderContextMenu = new FolderContextMenu();
            FolderContextMenuBuilder.addMenuForFolder(folderContextMenu, modelType);
            folderContextMenuMap.put(modelType, folderContextMenu);
        }

        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.addTreeSelectionListener(this);
        tree.addMouseListener(this);

        Events.getInstance().getBus().register(this);
    }

    private void buildNewEnvironment() {
        environment = DefaultEnvironmentBuilder.build();

        // will this forward to itself?
        Events.getInstance().getBus().post(new LoadEnvironmentEvent(environment, null));
    }

    @Subscribe
    public void clearEnvironment(ClearEnvironmentEvent event) {
        StoryElementNode environmentNode = (StoryElementNode) tree.getRootNode();
        environmentNode.removeAllChildren();

        buildNewEnvironment();
    }

    @Subscribe
    public void loadEnvironment(LoadEnvironmentEvent event) {
        environment = event.getEnvironment();
        treeBuilder.loadEnvironment(environment);

        model.reload();
    }

    @Subscribe
    public void reloadTree(TreeReloadEvent event) {
        treeBuilder.loadEnvironment(environment);
        model.reload();
    }

    @Subscribe
    public void refreshTree(TreeRefreshEvent event) {
        // TODO - should event include the parent node to refresh?
        if (event.getNodeToRefresh() == null) {
            StoryElementNode node = (StoryElementNode) tree.getLastSelectedPathComponent();
            model.reload(node);
        } else {
            // expand the node you just refreshed
            if (event.getNodeToRefresh() instanceof DefaultMutableTreeNode) {
                tree.expandNode((DefaultMutableTreeNode) event.getNodeToRefresh());
            }

            model.reload(event.getNodeToRefresh());
        }
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        Object node = tree.getLastSelectedPathComponent();
        if (node != null && (node instanceof EnvironmentNode)) {
            logger.debug("selected environment node " + node.toString());

            Events.getInstance().getBus().post(new SelectEnvironmentNodeEvent((EnvironmentNode) node, environment));
        }
    }

    /**
     *   Customize the right-click context menu
     */
    @Override
    public void mouseClicked(MouseEvent e) {

        if (SwingUtilities.isRightMouseButton(e)) {
            int row = tree.getClosestRowForLocation(e.getX(), e.getY());
            tree.setSelectionRow(row);

            DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
            if (node != null && node.getUserObject() != null) {
                Object modelObject = node.getUserObject();

                WebPopupMenu contextMenu;
                if (node instanceof FolderNode) {
                    ModelType filterType = ((FolderNode) node).getContextFilter();
                    logger.debug("loading context for folder type " + filterType);
                    contextMenu = folderContextMenuMap.get(filterType);
                } else {
                    ModelType modelType = ModelType.forClass(modelObject.getClass());
                    logger.info("loading context for model type " + modelType);
                    contextMenu = contextMenuMap.get(modelType);
                }

                if (contextMenu != null) {
                    contextMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent event) {
    }

    @Override
    public void mouseExited(MouseEvent event) {
    }

    @Override
    public void mousePressed(MouseEvent event) {
    }

    @Override
    public void mouseReleased(MouseEvent event) {
    }
}
