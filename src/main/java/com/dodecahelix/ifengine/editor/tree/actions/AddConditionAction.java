/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;

public class AddConditionAction extends TreeContextAction {

    private static final long serialVersionUID = -5814339297131124284L;

    public AddConditionAction() {
        super("Add Condition", ModelIconBuilder.getIconForModelClass(Condition.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        Condition newCondition = new Condition(ConditionType.FLAG);

        if (parentModelObject instanceof Command) {
            ((Command) parentModelObject).addValidation(newCondition);
        }
        if (parentModelObject instanceof Action) {
            ((Action) parentModelObject).addValidation(newCondition);
        }
        if (parentModelObject instanceof Dialog) {
            ((Dialog) parentModelObject).addValidation(newCondition);
        }
        if (parentModelObject instanceof Exit) {
            ((Exit) parentModelObject).getValidations().getConditions().add(newCondition);
        }
        if (parentModelObject instanceof ConditionSet) {
            ((ConditionSet) parentModelObject).getConditions().add(newCondition);
        }

        TreeNodeAppender.appendConditionNode(newCondition, this.currentlySelectedTreeNode);
    }


}
