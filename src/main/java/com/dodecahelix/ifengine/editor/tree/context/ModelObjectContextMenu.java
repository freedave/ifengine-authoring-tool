/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.menu.WebPopupMenu;

/**
 *   Do we need this, or just use WebPopupMenu?
 *
 */
public class ModelObjectContextMenu extends WebPopupMenu {

    private static final long serialVersionUID = 7030181054679108721L;

    static final Logger logger = LoggerFactory.getLogger(ModelObjectContextMenu.class.getName());

    public ModelObjectContextMenu() {
    }

}
