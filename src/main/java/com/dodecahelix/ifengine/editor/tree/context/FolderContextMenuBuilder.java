/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.context;

import com.alee.laf.menu.WebMenuItem;
import com.dodecahelix.ifengine.data.ModelType;
import com.dodecahelix.ifengine.editor.tree.actions.AddEntityCommandAction;
import com.dodecahelix.ifengine.editor.tree.actions.AddSharedConditionAction;
import com.dodecahelix.ifengine.editor.tree.actions.AddSharedExecutionAction;
import com.dodecahelix.ifengine.editor.tree.actions.TreeContextAction;

public class FolderContextMenuBuilder extends ContextMenuBuilder {

    public static void addMenuForFolder(FolderContextMenu menu, ModelType modelType) {

        switch (modelType) {
            case COMMAND:
                buildCommandMenu(menu);
                break;
            case CONDITION:
                buildConditionMenu(menu);
                break;
            case ACTION:
                buildConsequenceMenu(menu);
                break;
            case DIALOG:
                buildDialogMenu(menu);
                break;
            case EXECUTION:
                buildExecutionMenu(menu);
                break;
            case EXIT:
                buildExitMenu(menu);
                break;
            case ITEM:
                buildItemMenu(menu);
                break;
            case LOCATION:
                buildLocationMenu(menu);
                break;
            case MASS:
                buildMassMenu(menu);
                break;
            case PERSON:
                buildPersonMenu(menu);
                break;
            case PROPERTY:
                buildPropertyMenu(menu);
                break;
            case READER:
                buildNarratorMenu(menu);
                break;
            case SCHEDULE:
                buildScheduleMenu(menu);
                break;
            case STATISTIC:
                buildStatisticMenu(menu);
                break;
            case RANGE_DISPLAY:
                buildRangeDisplayMenu(menu);
                break;
            case BOOLEAN_DISPLAY:
                buildBooleanDisplayMenu(menu);
                break;
            case SHARED_CONDITIONS:
                buildSharedConditionsMenu(menu);
                break;
            case SHARED_EXECUTIONS:
                buildSharedExecutionsMenu(menu);
                break;
            case ENTITY_COMMAND_ITEMS:
                buildCommandConfigurationMenu(menu, modelType);
                break;
            case ENTITY_COMMAND_LOCATIONS:
                buildCommandConfigurationMenu(menu, modelType);
                break;
            case ENTITY_COMMAND_MASSES:
                buildCommandConfigurationMenu(menu, modelType);
                break;
            case ENTITY_COMMAND_PEOPLE:
                buildCommandConfigurationMenu(menu, modelType);
                break;
            case ENTITY_COMMAND_NARRATORS:
                buildCommandConfigurationMenu(menu, modelType);
                break;

            default:
                break;
        }

    }

    private static void buildBooleanDisplayMenu(FolderContextMenu menu) {
        menu.add(buildAddBooleanDisplayMenuItem());
    }

    private static void buildRangeDisplayMenu(FolderContextMenu menu) {
        menu.add(buildAddRangeDisplayMenuItem());
    }

    private static void buildConditionMenu(FolderContextMenu menu) {
        menu.add(buildAddConditionMenuItem());
    }

    private static void buildScheduleMenu(FolderContextMenu menu) {
        menu.add(buildAddScheduleMenuItem());
    }

    private static void buildStatisticMenu(FolderContextMenu menu) {
        //menu.add( buildAddStatisticMenuItem() );
    }

    private static void buildNarratorMenu(FolderContextMenu menu) {
        menu.add(buildAddNarratorMenuItem());
    }

    private static void buildPropertyMenu(FolderContextMenu menu) {
        menu.add(buildAddPropertyMenuItem());
    }

    private static void buildExitMenu(FolderContextMenu menu) {
        menu.add(buildAddExitMenuItem());
    }

    private static void buildExecutionMenu(FolderContextMenu menu) {
        menu.add(buildAddExecutionMenuItem());
    }

    private static void buildCommandMenu(FolderContextMenu menu) {
        menu.add(buildAddCommandMenuItem());
    }

    private static void buildConsequenceMenu(FolderContextMenu menu) {
        menu.add(buildAddConsequenceMenuItem());
    }

    private static void buildDialogMenu(FolderContextMenu menu) {
        menu.add(buildAddDialogMenuItem());
    }

    private static void buildItemMenu(FolderContextMenu menu) {
        menu.add(buildAddItemMenuItem());
    }

    private static void buildLocationMenu(FolderContextMenu menu) {
        menu.add(buildAddLocationMenuItem());
    }

    private static void buildMassMenu(FolderContextMenu menu) {
        menu.add(buildAddMassMenuItem());
    }

    private static void buildPersonMenu(FolderContextMenu menu) {
        menu.add(buildAddPersonMenuItem());
    }


    private static void buildSharedExecutionsMenu(FolderContextMenu menu) {
        TreeContextAction addSharedExecutionAction = new AddSharedExecutionAction();
        WebMenuItem menuItem = new WebMenuItem(addSharedExecutionAction);
        menu.add(menuItem);
    }

    private static void buildSharedConditionsMenu(FolderContextMenu menu) {
        TreeContextAction addSharedConditionAction = new AddSharedConditionAction();
        WebMenuItem menuItem = new WebMenuItem(addSharedConditionAction);
        menu.add(menuItem);
    }

    private static void buildCommandConfigurationMenu(FolderContextMenu menu, ModelType modelType) {
        TreeContextAction addEntityCommandAction = new AddEntityCommandAction(modelType);
        WebMenuItem menuItem = new WebMenuItem(addEntityCommandAction);
        menu.add(menuItem);
    }

}
