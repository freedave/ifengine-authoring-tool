/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

public class AddLocationAction extends TreeContextAction {

    private static final long serialVersionUID = -7423235599157070148L;

    public AddLocationAction() {
        super("Add Location", ModelIconBuilder.getIconForModelClass(Location.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        // locations can only be added to the environment
        if (parentModelObject instanceof Environment) {
            String id = "location-" + Randomizer.getRandomString(7);
            Location location = new Location(id, "name", "description");
            ((Environment) parentModelObject).addLocation(location);

            TreeNodeAppender.appendLocationNode(location, this.currentlySelectedTreeNode, true);
        }
    }

}
