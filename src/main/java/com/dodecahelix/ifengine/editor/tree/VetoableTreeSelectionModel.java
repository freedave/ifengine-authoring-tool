/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree;

import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreePath;

import com.dodecahelix.ifengine.editor.events.FormDirtyStatusEvent;
import com.dodecahelix.ifengine.editor.events.NotificationEvent;
import com.dodecahelix.ifengine.main.Events;
import com.google.common.eventbus.Subscribe;

public class VetoableTreeSelectionModel extends DefaultTreeSelectionModel {

    private static final long serialVersionUID = -5478813656473400581L;

    private boolean allowSelectionChange = true;

    public VetoableTreeSelectionModel() {
        Events.getInstance().getBus().register(this);
    }

    @Override
    public void setSelectionPaths(TreePath[] paths) {
        if (allowSelectionChange) {
            super.setSelectionPaths(paths);
        } else {
            Events.getInstance().getBus().post(new NotificationEvent("<html><body width='300px'><p>The current form has changes.  Please save or reset those changes before switching to another form."));
        }
    }

    @Subscribe
    public void updateFormStatus(FormDirtyStatusEvent statusEvent) {
        allowSelectionChange = !statusEvent.isDirty();
    }

}

