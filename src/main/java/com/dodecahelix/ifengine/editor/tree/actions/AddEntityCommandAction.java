/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.ModelType;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;

public class AddEntityCommandAction extends TreeContextAction {

    private static final long serialVersionUID = 6426236505927033954L;

    private ModelType modelType;

    public AddEntityCommandAction(ModelType modelType) {
        super("Add Command", ModelIconBuilder.getIconForModelClass(Command.class));
        this.modelType = modelType;
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {

        CommandConfiguration commandConfiguration = environment.getConfiguration().getCommandConfiguration();
        EntityType entityType = null;
        switch (modelType) {
            case ENTITY_COMMAND_ITEMS:
                entityType = EntityType.ITEM;
                break;
            case ENTITY_COMMAND_LOCATIONS:
                entityType = EntityType.LOCATION;
                break;
            case ENTITY_COMMAND_PEOPLE:
                entityType = EntityType.PERSON;
                break;
            case ENTITY_COMMAND_MASSES:
                entityType = EntityType.MASS;
                break;
            case ENTITY_COMMAND_NARRATORS:
                entityType = EntityType.NARRATOR;
                break;
            default:
                throw new IllegalArgumentException("invalid model type " + modelType);
        }

        Command command = new Command("Do Something", entityType);
        command.setDefaultExecutionMessage("Nothing happens.");

        switch (modelType) {
            case ENTITY_COMMAND_ITEMS:
                commandConfiguration.getItemCommands().add(command);
                break;
            case ENTITY_COMMAND_LOCATIONS:
                commandConfiguration.getLocationCommands().add(command);
                break;
            case ENTITY_COMMAND_PEOPLE:
                commandConfiguration.getPeopleCommands().add(command);
                break;
            case ENTITY_COMMAND_MASSES:
                commandConfiguration.getMassCommands().add(command);
                break;
            case ENTITY_COMMAND_NARRATORS:
                commandConfiguration.getNarratorCommands().add(command);
                break;
            default:
                throw new IllegalArgumentException("invalid model type " + modelType);
        }

        TreeNodeAppender.appendCommandNode(command, currentlySelectedTreeNode);
    }

}
