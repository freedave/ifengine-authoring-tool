/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

public class AddPropertyAction extends TreeContextAction {

    private static final long serialVersionUID = -1291066433349434934L;

    public AddPropertyAction() {
        super("Add Property", ModelIconBuilder.getIconForModelClass(Property.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        if (parentModelObject instanceof Entity) {
            // property id should be unique for this object (although this is no guarantee)
            String propId = "prop-" + Randomizer.getRandomString(7);

            Property newProperty = ((Entity) parentModelObject).setProperty(propId, "temp-prop-value");

            TreeNodeAppender.appendPropertyNode(newProperty, this.currentlySelectedTreeNode);
        }
    }

}
