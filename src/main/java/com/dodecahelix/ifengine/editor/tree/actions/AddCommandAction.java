/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

public class AddCommandAction extends TreeContextAction {

    private static final long serialVersionUID = 8688097108163151820L;

    public AddCommandAction() {
        super("Add Command", ModelIconBuilder.getIconForModelClass(Command.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {

        if (parentModelObject instanceof Entity) {
            Command command = new Command("Do Something", ((Entity) parentModelObject).getEntityType());
            command.setDefaultExecutionMessage("Nothing happens.");
            String randomId = "command-" + Randomizer.getRandomString(10);
            command.setReferenceId(randomId);

            ((Entity) parentModelObject).addCommand(command);

            TreeNodeAppender.appendCommandNode(command, currentlySelectedTreeNode);
        }

    }

}
