/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

public class AddSharedConditionAction extends TreeContextAction {

    private static final long serialVersionUID = -8363553330021229803L;

    public AddSharedConditionAction() {
        super("Add Shared Condition", ModelIconBuilder.getIconForModelClass(Condition.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        ConditionSet conditionSet = new ConditionSet();
        String refId = "cset-" + Randomizer.getRandomString(7);
        conditionSet.setReferenceId(refId);

        this.environment.getSharedConditions().add(conditionSet);

        TreeNodeAppender.nestConditionSetNode(conditionSet, this.currentlySelectedTreeNode, "Shared Condition Set : " + refId);
    }

}
