/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree;

import java.awt.Color;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.tree.WebTree;
import com.dodecahelix.ifengine.data.Environment;

public class EnvironmentTreePanel extends WebPanel {

    private static final long serialVersionUID = 984818434805721215L;

    static final Logger logger = LoggerFactory.getLogger(EnvironmentTreePanel.class.getName());

    public EnvironmentTreePanel() {

        this.setBackground(Color.WHITE);

        StoryElementNode top = new StoryElementNode(new Environment());
        DefaultTreeModel treeModel = new DefaultTreeModel(top);
        WebTree<DefaultMutableTreeNode> tree = new WebTree<DefaultMutableTreeNode>(treeModel);
        tree.setSelectionModel(new VetoableTreeSelectionModel());

        new EnvironmentTreeHandler(tree, treeModel);

        tree.setCellRenderer(new TreeNodeRenderer());

        WebScrollPane scrollPane = new WebScrollPane(tree);
        this.add(scrollPane);
    }

}
