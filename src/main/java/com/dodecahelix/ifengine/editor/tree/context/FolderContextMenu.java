/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.context;

import com.alee.laf.menu.WebPopupMenu;

public class FolderContextMenu extends WebPopupMenu {

    private static final long serialVersionUID = -7197851841885854713L;

}
