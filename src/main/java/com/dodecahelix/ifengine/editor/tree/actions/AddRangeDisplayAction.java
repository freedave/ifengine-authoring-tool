/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.RangeDisplay;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;

public class AddRangeDisplayAction extends TreeContextAction {

    private static final long serialVersionUID = -1560297965695201979L;

    public AddRangeDisplayAction() {
        super("Add Range Display", ModelIconBuilder.getIconForModelClass(RangeDisplay.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {

        if (parentModelObject instanceof Property) {
            RangeDisplay rangeDisplay = new RangeDisplay("Low", 0, 10);
            ((Property) parentModelObject).getRangeDisplays().add(rangeDisplay);

            TreeNodeAppender.appendRangeDisplayNode(rangeDisplay, currentlySelectedTreeNode);
        }
    }

}
