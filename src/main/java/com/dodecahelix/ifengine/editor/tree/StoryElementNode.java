/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree;

import javax.swing.Icon;

import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.BooleanDisplay;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.RangeDisplay;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.Schedule;
import com.dodecahelix.ifengine.data.configuration.AnatomyConfiguration;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.data.configuration.DialogConfiguration;
import com.dodecahelix.ifengine.data.configuration.TimeConfiguration;
import com.dodecahelix.ifengine.data.story.Conclusion;
import com.dodecahelix.ifengine.data.story.Passage;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *   Tree Node which represents an object in the Environment model
 *
 * @author dpeters
 *
 */
public class StoryElementNode extends EnvironmentNode {

    private static final long serialVersionUID = 7641289861184602108L;
    private String baseDisplay;

    public StoryElementNode(Object storyElementObject) {
        super(storyElementObject);
    }

    public StoryElementNode(Object storyElementObject, String label) {
        super(storyElementObject);

        baseDisplay = label;
    }

    private String updateDisplay() {
        Object userObject = this.getUserObject();
        String display = baseDisplay;

        if (display == null) {
            display = userObject.getClass().getSimpleName();
        }

        if ((userObject instanceof Entity)
        && !(userObject instanceof Environment)
        && !(userObject instanceof Reader)) {
            display = userObject.getClass().getSimpleName() + ": " + ((Entity) userObject).getTitle();
        }

        if (userObject instanceof Reader) {
            display = String.format("Narrator: %s", ((Reader) userObject).getTitle());
        }
        if (userObject instanceof Command) {
            display = String.format("Command: %s", ((Command) userObject).getCommandDisplay());
        }
        if (userObject instanceof Execution) {
            display = String.format("Execution: %s", ((Execution) userObject).getExecutionType());
        }
        if (userObject instanceof Condition) {
            display = String.format("Condition: %s", ((Condition) userObject).getType());
        }
        if (userObject instanceof Property) {
            display = String.format("Property: %s", ((Property) userObject).getPropertyId());
        }
        if (userObject instanceof Exit) {
            display = String.format("Exit: %s", ((Exit) userObject).getExitName());
        }
        if (userObject instanceof Dialog) {
            display = String.format("Dialog: %s", ((Dialog) userObject).getOptionDisplay());
        }
        if (userObject instanceof Passage) {
            display = String.format("Passage: %s", ((Passage) userObject).getPassageId());
        }
        if (userObject instanceof Conclusion) {
            display = String.format("Conclusion: %s", ((Conclusion) userObject).getConclusionId());
        }
        if (userObject instanceof Action) {
            String message = ((Action) userObject).getExecutionMessage();
            if (!StringUtils.isEmpty(message)) {
                if (message.length() > 20) {
                    message = message.substring(0, 20) + "...";
                }
                display = String.format("Action: %s", message);
            }
        }
        if (userObject instanceof TimeConfiguration) {
            display = "Time Configuration";
        }
        if (userObject instanceof AnatomyConfiguration) {
            display = "Anatomy";
        }
        if (userObject instanceof DialogConfiguration) {
            display = "Dialog Configuration";
        }
        if (userObject instanceof CommandConfiguration) {
            display = "Commands";
        }
        if (userObject instanceof StoryContent) {
            display = "Story Content";
        }
        if (userObject instanceof LibraryCard) {
            display = "Library Card";
        }
        if (userObject instanceof RangeDisplay) {
            display = String.format("Display: %s", ((RangeDisplay) userObject).getDisplay());
        }
        if (userObject instanceof BooleanDisplay) {
            display = String.format("Boolean Display");
        }
        if (userObject instanceof Schedule) {
            Schedule schedule = (Schedule) userObject;
            if (schedule.getLocationId() != null) {
                display = String.format("Schedule: %s", schedule.getLocationId());
            } else {
                display = "Schedule";
            }
        }
        if (userObject instanceof ConditionSet) {
            String refId = ((ConditionSet) userObject).getReferenceId();
            if (!StringUtils.isEmpty(refId)) {
                display = String.format("Condition Set: %s", refId);
            } else {
                display = "Condition Set";
            }
        }
        if (userObject instanceof ExecutionSet) {
            String refId = ((ExecutionSet) userObject).getReferenceId();
            if (!StringUtils.isEmpty(refId)) {
                display = String.format("Execution Set: %s", refId);
            } else {
                display = "Execution Set";
            }
        }

        return display;
    }

    @Override
    public String toString() {
        // display may have changed when user object is edited
        return updateDisplay();
    }

    public Icon getIcon() {
        Object userObject = this.getUserObject();
        if (userObject != null) {
            return ModelIconBuilder.getIconForModelClass(userObject.getClass());
        }
        return null;
    }

}
