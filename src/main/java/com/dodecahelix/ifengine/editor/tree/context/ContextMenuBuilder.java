/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.context;

import javax.swing.JMenuItem;

import com.alee.laf.menu.WebMenuItem;
import com.dodecahelix.ifengine.data.ModelType;
import com.dodecahelix.ifengine.editor.tree.actions.*;

public class ContextMenuBuilder {

    public static void addMenuForModelType(ModelObjectContextMenu menu, ModelType modelType) {

        switch (modelType) {
            case COMMAND:
                buildCommandMenu(menu);
                break;
            case CONDITION:
                buildDeleteOnlyMenu(menu);
                break;
            case ACTION:
                buildConsequenceMenu(menu);
                break;
            case DIALOG:
                buildDialogMenu(menu);
                break;
            case ENVIRONMENT:
                buildEnvironmentMenu(menu);
                break;
            case EXECUTION:
                buildDeleteOnlyMenu(menu);
                break;
            case EXIT:
                buildDeleteOnlyMenu(menu);
                break;
            case ITEM:
                buildItemMenu(menu);
                break;
            case LOCATION:
                buildLocationMenu(menu);
                break;
            case MASS:
                buildMassMenu(menu);
                break;
            case PERSON:
                buildPersonMenu(menu);
                break;
            case PROPERTY:
                buildDeleteOnlyMenu(menu);
                break;
            case READER:
                buildReaderMenu(menu);
                break;
            case SCHEDULE:
                buildDeleteOnlyMenu(menu);
                break;
            case STATISTIC:
                buildDeleteOnlyMenu(menu);
                break;
            case TIME:
                buildDeleteOnlyMenu(menu);
                break;
            case CONDITION_SET:
                buildConditionSetMenu(menu);
                break;
            case EXECUTION_SET:
                buildExecutionSetMenu(menu);
                break;
            case STORY_CONTENT:
                buildStoryContentMenu(menu);
                break;
            case STORY_PASSAGE:
                buildDeleteOnlyMenu(menu);
                break;
            case STORY_CONCLUSION:
                buildDeleteOnlyMenu(menu);
                break;
            default:
                break;
        }
    }

    private static void buildStoryContentMenu(ModelObjectContextMenu menu) {
        TreeContextAction addStoryPassageAction = new AddStoryPassageAction();
        TreeContextAction addStoryConclusionAction = new AddStoryConclusionAction();
        WebMenuItem menuItem = new WebMenuItem(addStoryPassageAction);
        WebMenuItem scMenuItem = new WebMenuItem(addStoryConclusionAction);
        menu.add(menuItem);
        menu.add(scMenuItem);
    }


    protected static JMenuItem buildAddPropertyMenuItem() {
        TreeContextAction addPropertyAction = new AddPropertyAction();
        WebMenuItem menuItem = new WebMenuItem(addPropertyAction);
        return menuItem;
    }

    protected static JMenuItem buildAddItemMenuItem() {
        TreeContextAction addContentItemAction = new AddContentItemAction();
        WebMenuItem menuItem = new WebMenuItem(addContentItemAction);
        return menuItem;
    }

    protected static JMenuItem buildAddNarratorMenuItem() {
        TreeContextAction addNarratorAction = new AddNarratorAction();
        WebMenuItem menuItem = new WebMenuItem(addNarratorAction);
        return menuItem;
    }

    protected static JMenuItem buildAddLocationMenuItem() {
        TreeContextAction addLocationAction = new AddLocationAction();
        WebMenuItem menuItem = new WebMenuItem(addLocationAction);
        return menuItem;
    }

    protected static JMenuItem buildDeleteMenuItem() {
        TreeContextAction deleteAction = new DeleteAction();
        WebMenuItem menuItem = new WebMenuItem(deleteAction);
        return menuItem;
    }

    protected static JMenuItem buildAddConsequenceMenuItem() {
        TreeContextAction csqAction = new AddConsequenceAction();
        WebMenuItem menuItem = new WebMenuItem(csqAction);
        return menuItem;
    }

    protected static JMenuItem buildAddConditionMenuItem() {
        TreeContextAction conditionAction = new AddConditionAction();
        WebMenuItem menuItem = new WebMenuItem(conditionAction);
        return menuItem;
    }

    protected static JMenuItem buildAddExecutionMenuItem() {
        TreeContextAction executionAction = new AddExecutionAction();
        WebMenuItem menuItem = new WebMenuItem(executionAction);
        return menuItem;
    }

    protected static JMenuItem buildAddPersonMenuItem() {
        TreeContextAction addPersonAction = new AddPersonAction();
        WebMenuItem menuItem = new WebMenuItem(addPersonAction);
        return menuItem;
    }

    protected static JMenuItem buildAddMassMenuItem() {
        TreeContextAction addMassAction = new AddMassAction();
        WebMenuItem menuItem = new WebMenuItem(addMassAction);
        return menuItem;
    }

    protected static JMenuItem buildAddBooleanDisplayMenuItem() {
        TreeContextAction addBooleanDisplayAction = new AddBooleanDisplayAction();
        WebMenuItem menuItem = new WebMenuItem(addBooleanDisplayAction);
        return menuItem;
    }

    protected static JMenuItem buildAddRangeDisplayMenuItem() {
        TreeContextAction addRangeAction = new AddRangeDisplayAction();
        WebMenuItem menuItem = new WebMenuItem(addRangeAction);
        return menuItem;
    }

    protected static JMenuItem buildAddCommandMenuItem() {
        TreeContextAction addCommandAction = new AddCommandAction();
        WebMenuItem menuItem = new WebMenuItem(addCommandAction);
        return menuItem;
    }

    protected static JMenuItem buildAddExitMenuItem() {
        TreeContextAction addExitAction = new AddExitAction();
        WebMenuItem menuItem = new WebMenuItem(addExitAction);
        return menuItem;
    }

    protected static JMenuItem buildAddScheduleMenuItem() {
        TreeContextAction addScheduleAction = new AddScheduleAction();
        WebMenuItem menuItem = new WebMenuItem(addScheduleAction);
        return menuItem;
    }

    protected static JMenuItem buildAddDialogMenuItem() {
        TreeContextAction addDialogAction = new AddDialogAction();
        WebMenuItem menuItem = new WebMenuItem(addDialogAction);
        return menuItem;
    }

    private static JMenuItem buildAddConditionSetMenuItem() {
        TreeContextAction addConditionSetAction = new AddConditionSetAction();
        WebMenuItem menuItem = new WebMenuItem(addConditionSetAction);
        return menuItem;
    }


    private static void buildDeleteOnlyMenu(ModelObjectContextMenu menu) {
        menu.add(buildDeleteMenuItem());
    }

    private static void buildCommandMenu(ModelObjectContextMenu menu) {
        menu.add(buildDeleteMenuItem());
        //menu.add( buildAddConditionMenuItem() );
        //menu.add( buildAddConsequenceMenuItem() );

    }

    private static void buildConsequenceMenu(ModelObjectContextMenu menu) {
        menu.add(buildDeleteMenuItem());
        //menu.add( buildAddConditionMenuItem() );
        //menu.add( buildAddExecutionMenuItem() );
    }

    private static void buildDialogMenu(ModelObjectContextMenu menu) {
        menu.add(buildDeleteMenuItem());
        //menu.add( buildAddConditionMenuItem() );
        //menu.add( buildAddExecutionMenuItem() );
    }

    private static void buildEnvironmentMenu(ModelObjectContextMenu menu) {
        //menu.add ( buildAddItemMenuItem () );
        //menu.add ( buildAddPropertyMenuItem() );
        //menu.add ( buildAddLocationMenuItem() );
        //menu.add ( buildAddMassMenuItem() );
        //menu.add ( buildAddPersonMenuItem() );
        //menu.add ( buildAddConsequenceMenuItem() );
        //menu.add ( buildAddConfigurationMenuItem() );
    }

    private static void buildItemMenu(ModelObjectContextMenu menu) {
        //menu.add ( buildAddPropertyMenuItem() );
        //menu.add ( buildAddCommandMenuItem() );
        menu.add(buildDeleteMenuItem());
        //menu.add ( buildAddConsequenceMenuItem() );
    }

    private static void buildLocationMenu(ModelObjectContextMenu menu) {
        //menu.add ( buildAddItemMenuItem () );
        //menu.add ( buildAddPropertyMenuItem() );
        //menu.add ( buildAddExitMenuItem() );
        //menu.add ( buildAddCommandMenuItem() );
        //menu.add ( buildAddMassMenuItem() );
        //menu.add ( buildAddConsequenceMenuItem() );
        menu.add(buildDeleteMenuItem());
    }

    private static void buildMassMenu(ModelObjectContextMenu menu) {
        //menu.add ( buildAddItemMenuItem () );
        //menu.add ( buildAddPropertyMenuItem() );
        //menu.add ( buildAddCommandMenuItem() );
        menu.add(buildDeleteMenuItem());
        //menu.add ( buildAddConsequenceMenuItem() );
    }

    private static void buildPersonMenu(ModelObjectContextMenu menu) {
        //menu.add ( buildAddItemMenuItem () );
        //menu.add ( buildAddPropertyMenuItem() );
        //menu.add ( buildAddDialogMenuItem() );
        //menu.add ( buildAddScheduleMenuItem() );
        //menu.add ( buildAddCommandMenuItem() );
        menu.add(buildDeleteMenuItem());
        //menu.add ( buildAddConsequenceMenuItem() );
    }

    private static void buildReaderMenu(ModelObjectContextMenu menu) {
        //menu.add ( buildAddItemMenuItem () );
        //menu.add ( buildAddPropertyMenuItem() );
        //menu.add ( buildAddConsequenceMenuItem() );
    }


    private static void buildExecutionSetMenu(ModelObjectContextMenu menu) {
        menu.add(buildAddExecutionMenuItem());
    }

    private static void buildConditionSetMenu(ModelObjectContextMenu menu) {
        menu.add(buildAddConditionMenuItem());
        menu.add(buildAddConditionSetMenuItem());
    }

}
