/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

public class AddPersonAction extends TreeContextAction {

    private static final long serialVersionUID = 1148975067933133951L;

    public AddPersonAction() {
        super("Add Person", ModelIconBuilder.getIconForModelClass(Person.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        String id = "person-" + Randomizer.getRandomString(7);
        Person person = new Person(id, "name", "description");

        if (parentModelObject instanceof Environment) {
            environment.addPerson(person);
            TreeNodeAppender.appendPersonNode(person, this.currentlySelectedTreeNode, true);
        }
    }

}
