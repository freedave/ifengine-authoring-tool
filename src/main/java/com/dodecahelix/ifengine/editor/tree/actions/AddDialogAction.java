/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

public class AddDialogAction extends TreeContextAction {

    private static final long serialVersionUID = 6684835300661563147L;

    public AddDialogAction() {
        super("Add Dialog", ModelIconBuilder.getIconForModelClass(Dialog.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        // dialogs can only be added to people
        if (parentModelObject instanceof Person) {
            Dialog dialog = new Dialog("Say Hello", "Hello right back at ya.", null);
            String randomId = "dialog-" + Randomizer.getRandomString(10);
            dialog.setReferenceId(randomId);
            ((Person) parentModelObject).addDialog(dialog);

            TreeNodeAppender.appendDialogNode(dialog, currentlySelectedTreeNode);
        }

        if (parentModelObject instanceof Dialog) {
            Dialog dialog = new Dialog("Uh huh.", "Yep.", null);
            String randomId = "dialog-" + Randomizer.getRandomString(10);
            dialog.setReferenceId(randomId);
            ((Dialog) parentModelObject).addDialog(dialog);

            TreeNodeAppender.appendDialogNode(dialog, currentlySelectedTreeNode);
        }
    }

}
