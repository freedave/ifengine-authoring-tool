/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree;

import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.dodecahelix.ifengine.data.ModelType;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;

/**
 *  Tree Node which groups a set of environment model objects
 *
 * @author dpeters
 *
 */
public class FolderNode extends EnvironmentNode {

    private static final long serialVersionUID = -8539010333259176630L;

    private String folderName;
    private ModelType contextFilter;

    /**
     *
     * @param storyElementObject - the parent model object (i.e; the Location)
     * @param contextFilter - what type of model objects belong in this folder (i.e; Exits)
     * @param folderName - display name for the folder
     */
    public FolderNode(Object storyElementObject, ModelType contextFilter, String folderName) {
        super(storyElementObject);

        this.folderName = folderName;
        this.contextFilter = contextFilter;
    }

    @Override
    public String toString() {
        return folderName;
    }

    public Icon getIcon() {
        URL imgUrl = ModelIconBuilder.class.getClassLoader().getResource("icons/folder.png");
        ImageIcon icon = new ImageIcon(imgUrl);
        return icon;
    }

    public ModelType getContextFilter() {
        return contextFilter;
    }

}
