/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Schedule;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;

public class AddScheduleAction extends TreeContextAction {

    private static final long serialVersionUID = 1317535303664370664L;

    public AddScheduleAction() {
        super("Add Schedule", ModelIconBuilder.getIconForModelClass(Schedule.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        // schedules can only be added to people
        if (parentModelObject instanceof Person) {
            Schedule schedule = new Schedule();
            // should never be empty, but check anyway
            if (!environment.getLocations().isEmpty()) {
                schedule.setLocationId(environment.getLocations().iterator().next().getId());
            }
            ((Person) parentModelObject).addSchedule(schedule);

            TreeNodeAppender.appendScheduleNode(schedule, this.currentlySelectedTreeNode);
        }
    }

}
