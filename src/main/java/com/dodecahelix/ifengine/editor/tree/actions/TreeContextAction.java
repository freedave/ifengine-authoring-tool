/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.tree.DefaultMutableTreeNode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.LoadEnvironmentEvent;
import com.dodecahelix.ifengine.editor.events.SelectEnvironmentNodeEvent;
import com.dodecahelix.ifengine.editor.events.TreeRefreshEvent;
import com.dodecahelix.ifengine.editor.tree.EnvironmentNode;
import com.dodecahelix.ifengine.editor.tree.StoryElementNode;
import com.dodecahelix.ifengine.main.Events;
import com.google.common.eventbus.Subscribe;

/**
 *   Abstract action supporting a selected model object
 * @author dpeters
 *
 */
public abstract class TreeContextAction extends AbstractAction {

    private static final long serialVersionUID = 3926373479814733880L;

    static final Logger logger = LoggerFactory.getLogger(TreeContextAction.class.getName());

    /**
     *   the currently selected node in the tree view
     */
    protected EnvironmentNode currentlySelectedTreeNode;

    /**
     *   keep a reference to the environment.  we might have to add an object to it in the action
     */
    protected Environment environment;

    public TreeContextAction(String string, Icon iconForModelClass) {
        super(string, iconForModelClass);

        // listen for tree selection events and update currently selected node
        Events.getInstance().getBus().register(this);
    }

    @Deprecated
    protected StoryElementNode addNewStoryElementNode(Object userObject) {
        StoryElementNode newNode = new StoryElementNode(userObject);
        currentlySelectedTreeNode.insert(newNode, currentlySelectedTreeNode.getChildCount());

        // TODO - what about nested objects and folders?  create on the fly?

        // fire off a tree refresh event
        Events.getInstance().getBus().post(new TreeRefreshEvent(currentlySelectedTreeNode));
        return newNode;
    }

    @Subscribe
    public void updateSelectedNode(SelectEnvironmentNodeEvent selectEnvironmentNodeEvent) {
        this.currentlySelectedTreeNode = selectEnvironmentNodeEvent.getSelectedTreeNode();
    }

    @Subscribe
    public void updateEnvironment(LoadEnvironmentEvent loadEnvironmentEvent) {
        this.environment = loadEnvironmentEvent.getEnvironment();
    }

    public DefaultMutableTreeNode getTreeNode() {
        return currentlySelectedTreeNode;
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        if (logger.isDebugEnabled()) {
            logger.debug("adding child node to currently selected node " + currentlySelectedTreeNode);
        }

        if (currentlySelectedTreeNode != null && currentlySelectedTreeNode.getUserObject() != null) {
            Object entity = this.currentlySelectedTreeNode.getUserObject();

            addNewEnvironmentObject(entity);

            Events.getInstance().getBus().post(new TreeRefreshEvent(currentlySelectedTreeNode));
        }
    }

    protected abstract void addNewEnvironmentObject(Object parentModelObject);

}
