/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

public class AddSharedExecutionAction extends TreeContextAction {

    private static final long serialVersionUID = 1935989647706758133L;

    public AddSharedExecutionAction() {
        super("Add Shared Execution", ModelIconBuilder.getIconForModelClass(Execution.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        ExecutionSet execSet = new ExecutionSet();
        String refId = "xset-" + Randomizer.getRandomString(7);
        execSet.setReferenceId(refId);

        this.environment.getSharedExecutions().add(execSet);

        TreeNodeAppender.nestExecutionSetNode(execSet, this.currentlySelectedTreeNode, "Shared Execution Set : " + refId);
    }

}
