/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.build;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.tree.DefaultMutableTreeNode;

import com.dodecahelix.ifengine.data.story.Conclusion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.tree.WebTree;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.ModelType;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.configuration.Configuration;
import com.dodecahelix.ifengine.data.story.Passage;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.editor.tree.FolderNode;
import com.dodecahelix.ifengine.editor.tree.StoryElementNode;

public class TreeLoader {

    static final Logger logger = LoggerFactory.getLogger(TreeLoader.class.getName());

    private WebTree<DefaultMutableTreeNode> tree;

    private Map<String, Item> itemMap = new HashMap<String, Item>();
    private Map<String, Mass> massMap = new HashMap<String, Mass>();
    private Map<String, Person> peopleMap = new HashMap<String, Person>();
    private Map<String, Reader> narratorMap = new HashMap<String, Reader>();

    public TreeLoader(WebTree<DefaultMutableTreeNode> tree) {
        this.tree = tree;
    }

    public void loadEnvironment(Environment environment) {
        logger.debug("loading environment into tree");

        itemMap.clear();
        peopleMap.clear();
        massMap.clear();
        narratorMap.clear();

        for (Item item : environment.getItems()) {
            itemMap.put(item.getId(), item);
        }
        for (Person person : environment.getPeople()) {
            peopleMap.put(person.getId(), person);
        }
        for (Mass mass : environment.getMasses()) {
            massMap.put(mass.getId(), mass);
        }
        for (Reader reader : environment.getNarrators()) {
            narratorMap.put(reader.getId(), reader);
        }

        StoryElementNode environmentNode = (StoryElementNode) tree.getRootNode();
        environmentNode.setUserObject(environment);
        environmentNode.removeAllChildren();

        // one and only one story node
        LibraryCard storyMeta = environment.getLibraryCard();
        StoryElementNode storyMetaNode = new StoryElementNode(storyMeta);
        environmentNode.insert(storyMetaNode, environmentNode.getChildCount());

        // one story element node
        StoryContent content = environment.getStoryContent();
        StoryElementNode storyContentNode = new StoryElementNode(content);
        environmentNode.insert(storyContentNode, environmentNode.getChildCount());
        for (Passage passage : content.getPassages()) {
            TreeNodeAppender.appendPassageNode(passage, storyContentNode);
        }
        for (Conclusion conclusion : content.getConclusions()) {
            TreeNodeAppender.appendConclusionNode(conclusion, storyContentNode);
        }

        // folder that stores locations
        FolderNode locationsFolder = new FolderNode(environment, ModelType.LOCATION, "Locations");
        environmentNode.insert(locationsFolder, environmentNode.getChildCount());

        for (Location location : environment.getLocations()) {
            logger.debug("adding location " + location.getTitle());

            StoryElementNode locationNode = TreeNodeAppender.appendLocationNode(location, locationsFolder, false);
            nestMassNodes(location.getMasses(), locationNode);
            nestItemNodes(location.getContents(), locationNode);
        }

        nestPeopleNodes(environment.getPeople(), environmentNode);

        nestItemNodes(environment.getContents(), environmentNode);
        TreeNodeAppender.nestCommandNodes(environment.getCommands(), environmentNode);
        TreeNodeAppender.nestActionNodes(environment.getActions(), environmentNode, "Turn Actions");

        nestNarratorNodes(environment.getNarrators(), environmentNode);

        TreeNodeAppender.nestPropertyNodes(environment.getProperties(), environmentNode);

        nestSharedConditionNodes(environment.getSharedConditions(), environmentNode, environment);
        nestSharedExecutionNodes(environment.getSharedExecutions(), environmentNode, environment);
        nestConfigurationNode(environment.getConfiguration(), environmentNode, environment);

        logger.info("finished loading environment into tree.");
    }

    private void nestConfigurationNode(Configuration configuration, StoryElementNode environmentNode, Environment environment) {
        StoryElementNode configurationNode = new StoryElementNode(configuration);
        environmentNode.insert(configurationNode, environmentNode.getChildCount());

        StoryElementNode anatomyConfigNode = new StoryElementNode(configuration.getAnatomyConfiguration());
        configurationNode.insert(anatomyConfigNode, configurationNode.getChildCount());

        StoryElementNode commandConfigurationNode = new StoryElementNode(configuration.getCommandConfiguration());
        configurationNode.insert(commandConfigurationNode, configurationNode.getChildCount());

        TreeNodeAppender.appendCommandNode(configuration.getCommandConfiguration().getIntroCommand(), commandConfigurationNode);

        TreeNodeAppender.nestCommandNodes(configuration.getCommandConfiguration().getItemCommands(), commandConfigurationNode, ModelType.ENTITY_COMMAND_ITEMS, "Item Commands");
        TreeNodeAppender.nestCommandNodes(configuration.getCommandConfiguration().getLocationCommands(), commandConfigurationNode, ModelType.ENTITY_COMMAND_LOCATIONS, "Location Commands");
        TreeNodeAppender.nestCommandNodes(configuration.getCommandConfiguration().getMassCommands(), commandConfigurationNode, ModelType.ENTITY_COMMAND_MASSES, "Mass Commands");
        TreeNodeAppender.nestCommandNodes(configuration.getCommandConfiguration().getNarratorCommands(), commandConfigurationNode, ModelType.ENTITY_COMMAND_NARRATORS, "Narrator Commands");
        TreeNodeAppender.nestCommandNodes(configuration.getCommandConfiguration().getPeopleCommands(), commandConfigurationNode, ModelType.ENTITY_COMMAND_PEOPLE, "Person Commands");

        StoryElementNode dialogConfigurationNode = new StoryElementNode(configuration.getDialogConfiguration());
        configurationNode.insert(dialogConfigurationNode, configurationNode.getChildCount());

        StoryElementNode messageConfigurationNode = new StoryElementNode(configuration.getMessageConfiguration());
        configurationNode.insert(messageConfigurationNode, configurationNode.getChildCount());

        StoryElementNode timeConfigurationNode = new StoryElementNode(configuration.getTimeConfiguration());
        configurationNode.insert(timeConfigurationNode, configurationNode.getChildCount());
    }

    private void nestSharedExecutionNodes(Set<ExecutionSet> sharedExecutions, StoryElementNode environmentNode, Environment environment) {

        FolderNode shareFolderNode = new FolderNode(sharedExecutions,
        ModelType.SHARED_EXECUTIONS,
        "Shared Executions");
        environmentNode.insert(shareFolderNode, environmentNode.getChildCount());

        for (ExecutionSet executionSet : environment.getSharedExecutions()) {
            String display = "Shared Execution Set";
            String refId = executionSet.getReferenceId();
            if (refId != null) {
                display = String.format("Shared Execution Set : %s", refId);
            }
            TreeNodeAppender.nestExecutionSetNode(executionSet, shareFolderNode, display);
        }
    }

    private void nestSharedConditionNodes(Set<ConditionSet> sharedConditions, StoryElementNode environmentNode, Environment environment) {

        FolderNode shareFolderNode = new FolderNode(sharedConditions,
        ModelType.SHARED_CONDITIONS,
        "Shared Conditions");
        environmentNode.insert(shareFolderNode, environmentNode.getChildCount());

        for (ConditionSet conditionSet : environment.getSharedConditions()) {
            String display = "Shared Condition Set";
            String refId = conditionSet.getReferenceId();
            if (refId != null) {
                display = String.format("Shared Condition Set : %s", refId);
            }
            TreeNodeAppender.nestConditionSetNode(conditionSet, shareFolderNode, display);
        }
    }

    private void nestNarratorNodes(Set<Reader> narrators, StoryElementNode environmentNode) {

        FolderNode narratorsFolder = new FolderNode(environmentNode.getUserObject(),
        ModelType.READER,
        "Narrators");
        environmentNode.insert(narratorsFolder, environmentNode.getChildCount());

        for (Reader reader : narrators) {
            StoryElementNode readerNode = new StoryElementNode(reader);

            narratorsFolder.insert(readerNode, narratorsFolder.getChildCount());

            TreeNodeAppender.nestPropertyNodes(reader.getProperties(), readerNode);
            nestItemNodes(reader.getContents(), readerNode);
            TreeNodeAppender.nestCommandNodes(reader.getCommands(), readerNode);
            TreeNodeAppender.nestActionNodes(reader.getActions(), readerNode, "Turn Actions");
        }
    }

    private void nestMassNodes(Set<String> masses, StoryElementNode parentNode) {

        FolderNode massFolder = new FolderNode(parentNode.getUserObject(),
        ModelType.MASS,
        "Masses");
        parentNode.insert(massFolder, parentNode.getChildCount());

        for (String massId : masses) {
            Mass mass = massMap.get(massId);
            StoryElementNode massNode = TreeNodeAppender.appendMassNode(mass, massFolder, false);
            nestItemNodes(mass.getContents(), massNode);
        }

    }

    private void nestItemNodes(Set<String> contents, StoryElementNode parentNode) {

        FolderNode itemFolder = new FolderNode(parentNode.getUserObject(),
        ModelType.ITEM,
        "Items");
        parentNode.insert(itemFolder, parentNode.getChildCount());

        for (String itemId : contents) {
            Item item = itemMap.get(itemId);
            TreeNodeAppender.appendItemNode(item, itemFolder);
        }
    }

    private void nestPeopleNodes(Set<Person> people, StoryElementNode environmentNode) {

        FolderNode peopleFolder = new FolderNode(environmentNode.getUserObject(),
        ModelType.PERSON,
        "People");
        environmentNode.insert(peopleFolder, environmentNode.getChildCount());

        for (Person person : people) {
            StoryElementNode personNode = TreeNodeAppender.appendPersonNode(person, peopleFolder, false);
            nestItemNodes(person.getContents(), personNode);
        }

    }

}
