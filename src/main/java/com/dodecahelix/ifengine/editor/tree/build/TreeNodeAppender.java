/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.build;

import java.util.List;
import java.util.Set;

import javax.swing.tree.DefaultMutableTreeNode;

import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.BooleanDisplay;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.ModelType;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.RangeDisplay;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.Schedule;
import com.dodecahelix.ifengine.data.story.Conclusion;
import com.dodecahelix.ifengine.data.story.Passage;
import com.dodecahelix.ifengine.editor.tree.EnvironmentNode;
import com.dodecahelix.ifengine.editor.tree.FolderNode;
import com.dodecahelix.ifengine.editor.tree.StoryElementNode;

public class TreeNodeAppender {

    public static void nestPropertyNodes(Set<Property> properties, StoryElementNode parentEntityNode) {

        // create a folder node for properties and put these under there
        FolderNode propertiesFolder = new FolderNode(parentEntityNode.getUserObject(),
        ModelType.PROPERTY, "Properties");
        parentEntityNode.insert(propertiesFolder, parentEntityNode.getChildCount());

        for (Property property : properties) {
            appendPropertyNode(property, propertiesFolder);
        }
    }

    public static void nestDialogNodes(List<Dialog> dialogs, StoryElementNode personNode) {

        // create a folder node for dialogs and put these under there
        FolderNode dialogsFolder = new FolderNode(personNode.getUserObject(),
        ModelType.DIALOG,
        "Dialogs");
        personNode.insert(dialogsFolder, personNode.getChildCount());

        for (Dialog dialog : dialogs) {
            appendDialogNode(dialog, dialogsFolder);
        }
    }

    public static void nestScheduleNodes(List<Schedule> schedules, StoryElementNode personNode) {

        FolderNode schedulesFolder = new FolderNode(personNode.getUserObject(),
        ModelType.SCHEDULE,
        "Schedules");
        personNode.insert(schedulesFolder, personNode.getChildCount());

        for (Schedule schedule : schedules) {
            appendScheduleNode(schedule, schedulesFolder);
        }

    }

    public static void nestActionNodes(List<Action> consequences, StoryElementNode parentNode, String label) {

        FolderNode conditionsFolder = new FolderNode(parentNode.getUserObject(),
        ModelType.ACTION,
        label);
        parentNode.insert(conditionsFolder, parentNode.getChildCount());

        for (Action consequence : consequences) {
            appendActionNode(consequence, conditionsFolder);
        }
    }

    private static void nestSubDialogNodes(List<Dialog> subDialogs, StoryElementNode dialogNode, String label) {
        FolderNode subDialogsFolder = new FolderNode(dialogNode.getUserObject(),
        ModelType.DIALOG,
        label);
        dialogNode.insert(subDialogsFolder, dialogNode.getChildCount());

        for (Dialog dialog : subDialogs) {
            appendDialogNode(dialog, subDialogsFolder);
        }
    }

    public static void nestConditionSetNode(ConditionSet conditionSet, DefaultMutableTreeNode parentNode, String label) {

        StoryElementNode conditionSetNode = new StoryElementNode(conditionSet, label);
        parentNode.insert(conditionSetNode, parentNode.getChildCount());

        for (Condition condition : conditionSet.getConditions()) {
            appendConditionNode(condition, conditionSetNode);
        }

        for (ConditionSet conditionSubset : conditionSet.getSubsets()) {
            nestConditionSetNode(conditionSubset, conditionSetNode, "subset");
        }
    }

    public static void nestExecutionSetNode(ExecutionSet executionSet, DefaultMutableTreeNode parentNode, String display) {
        StoryElementNode executionSetNode = new StoryElementNode(executionSet, display);
        parentNode.insert(executionSetNode, parentNode.getChildCount());

        for (Execution execution : executionSet.getExecutions()) {
            StoryElementNode executionNode = new StoryElementNode(execution);
            executionSetNode.insert(executionNode, executionSetNode.getChildCount());
        }
    }


    public static void nestCommandNodes(List<Command> commands, StoryElementNode parentNode) {
        nestCommandNodes(commands, parentNode, ModelType.COMMAND, "Commands");
    }

    public static void nestCommandNodes(List<Command> commands, StoryElementNode parentNode, ModelType modelType, String display) {

        FolderNode commandsFolder = new FolderNode(parentNode.getUserObject(),
        modelType,
        display);

        parentNode.insert(commandsFolder, parentNode.getChildCount());

        for (Command command : commands) {
            appendCommandNode(command, commandsFolder);
        }
    }

    public static void nestExitNodes(Set<Exit> exits, StoryElementNode locationNode) {

        FolderNode exitsFolder = new FolderNode(locationNode.getUserObject(),
        ModelType.EXIT,
        "Exits");

        locationNode.insert(exitsFolder, locationNode.getChildCount());

        for (Exit exit : exits) {
            appendExitNode(exit, exitsFolder);
        }
    }

    private static void nestRangeDisplayNodes(List<RangeDisplay> displays, StoryElementNode propertyNode) {

        FolderNode rangeDisplaysFolder = new FolderNode(propertyNode.getUserObject(),
        ModelType.RANGE_DISPLAY,
        "Range Displays");

        propertyNode.insert(rangeDisplaysFolder, propertyNode.getChildCount());

        for (RangeDisplay display : displays) {
            appendRangeDisplayNode(display, rangeDisplaysFolder);
        }
    }

    private static void nestBooleanDisplayNodes(List<BooleanDisplay> displays, StoryElementNode propertyNode) {

        FolderNode booleanDisplayFolder = new FolderNode(propertyNode.getUserObject(),
        ModelType.BOOLEAN_DISPLAY,
        "Boolean Display");

        propertyNode.insert(booleanDisplayFolder, propertyNode.getChildCount());

        for (BooleanDisplay display : displays) {
            appendBooleanDisplayNode((BooleanDisplay) display, booleanDisplayFolder);
        }
    }

    public static void appendBooleanDisplayNode(BooleanDisplay booleanDisplay, EnvironmentNode booleanDisplayFolder) {
        StoryElementNode booleanDisplayNode = new StoryElementNode(booleanDisplay);
        booleanDisplayFolder.insert(booleanDisplayNode, booleanDisplayFolder.getChildCount());
    }

    public static void appendRangeDisplayNode(RangeDisplay rangeDisplay, EnvironmentNode rangeDisplaysFolder) {
        StoryElementNode rangeDisplayNode = new StoryElementNode(rangeDisplay);
        rangeDisplaysFolder.insert(rangeDisplayNode, rangeDisplaysFolder.getChildCount());
    }

    public static void appendExitNode(Exit exit, EnvironmentNode parentNode) {
        StoryElementNode exitNode = new StoryElementNode(exit);
        parentNode.insert(exitNode, parentNode.getChildCount());

        nestConditionSetNode(exit.getValidations(), exitNode, "Validations");
    }

    public static void appendCommandNode(Command command, EnvironmentNode parentNode) {
        StoryElementNode commandNode = new StoryElementNode(command);
        parentNode.insert(commandNode, parentNode.getChildCount());

        nestConditionSetNode(command.getValidations(), commandNode, "Validations");
        nestExecutionSetNode(command.getDefaultResults(), commandNode, "Default Executions");
        nestActionNodes(command.getOutcomes(), commandNode, "Alternative Outcomes");
    }

    public static void appendConditionNode(Condition condition, EnvironmentNode parentNode) {
        StoryElementNode conditionNode = new StoryElementNode(condition);
        parentNode.insert(conditionNode, parentNode.getChildCount());
    }

    public static void appendActionNode(Action action, EnvironmentNode parentNode) {
        StoryElementNode actionNode = new StoryElementNode(action);
        parentNode.insert(actionNode, parentNode.getChildCount());

        nestConditionSetNode(action.getValidations(), actionNode, "Validations");
        nestExecutionSetNode(action.getResults(), actionNode, "Executions");
    }

    public static void appendDialogNode(Dialog dialog, EnvironmentNode parentNode) {
        StoryElementNode dialogNode = new StoryElementNode(dialog);
        parentNode.insert(dialogNode, parentNode.getChildCount());

        nestConditionSetNode(dialog.getValidations(), dialogNode, "Validations");
        nestExecutionSetNode(dialog.getResults(), dialogNode, "Executions");
        nestSubDialogNodes(dialog.getSubDialogs(), dialogNode, "Sub Dialogs");
    }

    public static void appendExecutionNode(Execution execution, EnvironmentNode parentNode) {
        StoryElementNode executionNode = new StoryElementNode(execution);
        parentNode.insert(executionNode, parentNode.getChildCount());
    }

    public static void appendPropertyNode(Property property, EnvironmentNode parentNode) {
        StoryElementNode propertyNode = new StoryElementNode(property);
        parentNode.insert(propertyNode, parentNode.getChildCount());

        nestActionNodes(property.getChangeActions(), propertyNode, "Change Actions");
        nestRangeDisplayNodes(property.getRangeDisplays(), propertyNode);
        nestBooleanDisplayNodes(property.getBooleanDisplays(), propertyNode);
    }

    public static void appendScheduleNode(Schedule schedule, EnvironmentNode parentNode) {
        StoryElementNode scheduleNode = new StoryElementNode(schedule);
        parentNode.insert(scheduleNode, parentNode.getChildCount());
    }

    public static void appendItemNode(Item item, EnvironmentNode parentNode) {
        StoryElementNode itemNode = new StoryElementNode(item);

        parentNode.insert(itemNode, parentNode.getChildCount());

        nestPropertyNodes(item.getProperties(), itemNode);
        nestCommandNodes(item.getCommands(), itemNode);
        nestActionNodes(item.getActions(), itemNode, "Turn Actions");
    }

    public static StoryElementNode appendMassNode(Mass mass,
                                                  EnvironmentNode parentNode,
                                                  boolean empty) {

        StoryElementNode massNode = new StoryElementNode(mass);

        parentNode.insert(massNode, parentNode.getChildCount());

        TreeNodeAppender.nestPropertyNodes(mass.getProperties(), massNode);
        TreeNodeAppender.nestCommandNodes(mass.getCommands(), massNode);
        TreeNodeAppender.nestActionNodes(mass.getActions(), massNode, "Turn Actions");

        if (empty) {
            TreeNodeAppender.appendEmptyContentNode(massNode);
        }

        return massNode;
    }

    private static void appendEmptyContentNode(StoryElementNode entityNode) {
        FolderNode itemFolder = new FolderNode(entityNode.getUserObject(),
        ModelType.ITEM,
        "Items");
        entityNode.insert(itemFolder, entityNode.getChildCount());
    }


    private static void appendEmptyMassFolderNode(StoryElementNode locationNode) {
        FolderNode massesFolder = new FolderNode(locationNode.getUserObject(),
        ModelType.MASS,
        "Masses");
        locationNode.insert(massesFolder, locationNode.getChildCount());
    }

    public static StoryElementNode appendLocationNode(Location location,
                                                      EnvironmentNode parentNode,
                                                      boolean empty) {

        StoryElementNode locationNode = new StoryElementNode(location);
        parentNode.insert(locationNode, parentNode.getChildCount());

        // nest commands, property nodes, in this node
        nestPropertyNodes(location.getProperties(), locationNode);
        nestCommandNodes(location.getCommands(), locationNode);
        nestExitNodes(location.getExits(), locationNode);
        nestActionNodes(location.getActions(), locationNode, "Turn Actions");

        if (empty) {
            TreeNodeAppender.appendEmptyMassFolderNode(locationNode);
            TreeNodeAppender.appendEmptyContentNode(locationNode);
        }

        return locationNode;
    }

    public static StoryElementNode appendPersonNode(Person person,
                                                    EnvironmentNode parentNode,
                                                    boolean empty) {
        StoryElementNode personNode = new StoryElementNode(person);

        parentNode.insert(personNode, parentNode.getChildCount());

        nestPropertyNodes(person.getProperties(), personNode);
        nestCommandNodes(person.getCommands(), personNode);
        nestDialogNodes(person.getDialogs(), personNode);
        nestScheduleNodes(person.getSchedules(), personNode);
        nestActionNodes(person.getActions(), personNode, "Turn Actions");

        if (empty) {
            TreeNodeAppender.appendEmptyContentNode(personNode);
        }

        return personNode;
    }

    public static void appendPassageNode(Passage passage, EnvironmentNode parentNode) {
        StoryElementNode passageNode = new StoryElementNode(passage);
        parentNode.insert(passageNode, parentNode.getChildCount());
    }

    public static void appendConclusionNode(Conclusion conclusion, EnvironmentNode parentNode) {
        StoryElementNode conclusionNode = new StoryElementNode(conclusion);
        parentNode.insert(conclusionNode, parentNode.getChildCount());
    }

    public static void appendConditionSetNode(ConditionSet conditionSet, EnvironmentNode parentNode) {
        StoryElementNode conditionSetNode = new StoryElementNode(conditionSet);
        parentNode.insert(conditionSetNode, parentNode.getChildCount());
    }

    public static void appendNarratorNode(Reader narrator, EnvironmentNode parentNode) {
        StoryElementNode narratorNode = new StoryElementNode(narrator);
        parentNode.insert(narratorNode, parentNode.getChildCount());

        TreeNodeAppender.nestPropertyNodes(narrator.getProperties(), narratorNode);
        TreeNodeAppender.appendEmptyContentNode(narratorNode);
        TreeNodeAppender.nestCommandNodes(narrator.getCommands(), narratorNode);
        TreeNodeAppender.nestActionNodes(narrator.getActions(), narratorNode, "Turn Actions");
    }

}
