/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.tree.TreeNode;

import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.ModelType;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Schedule;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.data.story.Passage;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.editor.events.TreeRefreshEvent;
import com.dodecahelix.ifengine.editor.tree.EnvironmentNode;
import com.dodecahelix.ifengine.editor.tree.FolderNode;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.util.StringUtils;

public class DeleteAction extends TreeContextAction {

    private static final long serialVersionUID = -7707327939957101114L;

    public DeleteAction() {
        super("Delete", new ImageIcon(DeleteAction.class.getClassLoader().getResource("icons/delete.png")));
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        // need to delete this object from its parent - in both the model and the tree
        TreeNode parentNode = currentlySelectedTreeNode.getParent();
        if (parentNode != null && (parentNode instanceof EnvironmentNode)) {
            Object parentObject = ((EnvironmentNode) parentNode).getUserObject();
            Object childObject = currentlySelectedTreeNode.getUserObject();

            if ((parentObject instanceof Entity) && (childObject instanceof Command)) {
                removeCommand((Command) childObject, (Entity) parentObject);
            }

            if ((parentObject instanceof Entity) && (childObject instanceof Item)) {
                removeItem((Item) childObject, (Entity) parentObject);
            }

            if ((parentObject instanceof Entity) && (childObject instanceof Action)) {
                removeTurnAction((Action) childObject, (Entity) parentObject);
            }

            if ((parentObject instanceof Environment) && (childObject instanceof Location)) {
                removeLocation((Location) childObject, (Environment) parentObject);
            }

            if ((parentObject instanceof Entity) && (childObject instanceof Mass)) {
                removeMass((Mass) childObject, (Entity) parentObject);
            }

            if ((parentObject instanceof Entity) && (childObject instanceof Person)) {
                removePerson((Person) childObject, (Entity) parentObject);
            }

            // remove execution from consequence
            if ((parentObject instanceof Command) && (childObject instanceof Action)) {
                removeConsequence((Action) childObject, (Command) parentObject);
            }

            if ((parentObject instanceof Person) && (childObject instanceof Schedule)) {
                removeSchedule((Schedule) childObject, (Person) parentObject);
            }

            if ((parentObject instanceof Command) && (childObject instanceof Condition)) {
                removeCommandValidation((Condition) childObject, (Command) parentObject);
            }

            if ((parentObject instanceof ConditionSet) && (childObject instanceof Condition)) {
                removeConditionFromSet((Condition) childObject, (ConditionSet) parentObject);
            }

            if ((parentObject instanceof ExecutionSet) && (childObject instanceof Execution)) {
                removeExecutionFromSet((Execution) childObject, (ExecutionSet) parentObject);
            }

            if ((parentObject instanceof Person) && (childObject instanceof Dialog)) {
                removeDialog((Dialog) childObject, (Person) parentObject);
            }

            if ((parentObject instanceof Location) && (childObject instanceof Exit)) {
                removeExit((Exit) childObject, (Location) parentObject);
            }

            if ((parentObject instanceof StoryContent) && (childObject instanceof Passage)) {
                removeStoryPassage((Passage) childObject, (StoryContent) parentObject);
            }

            if (parentNode instanceof FolderNode) {
                ModelType context = ((FolderNode) parentNode).getContextFilter();

                if (parentObject instanceof CommandConfiguration) {
                    removeItemTypeCommand((Command) childObject, (CommandConfiguration) parentObject, context);
                }
            }

            ((EnvironmentNode) parentNode).remove(currentlySelectedTreeNode);

            // refresh the tree
            Events.getInstance().getBus().post(new TreeRefreshEvent(parentNode));
        } else {
            logger.warn("Invalid parent node " + parentNode);
        }

    }


    private void removeItemTypeCommand(Command command, CommandConfiguration commandConfiguration, ModelType context) {
        switch (context) {
            case ENTITY_COMMAND_ITEMS:
                removeEntityCommandFromList(command, commandConfiguration.getItemCommands());
                break;
            case ENTITY_COMMAND_MASSES:
                removeEntityCommandFromList(command, commandConfiguration.getMassCommands());
                break;
            case ENTITY_COMMAND_LOCATIONS:
                removeEntityCommandFromList(command, commandConfiguration.getLocationCommands());
                break;
            case ENTITY_COMMAND_NARRATORS:
                removeEntityCommandFromList(command, commandConfiguration.getNarratorCommands());
                break;
            case ENTITY_COMMAND_PEOPLE:
                removeEntityCommandFromList(command, commandConfiguration.getPeopleCommands());
                break;
            default:
                throw new IllegalArgumentException("Cannot remove a " + context.name() + " from a command configuration.");
        }
    }

    private void removeEntityCommandFromList(Command commandToRemove, List<Command> commandList) {
        Iterator<Command> commandIt = commandList.iterator();
        while (commandIt.hasNext()) {
            Command nextCommand = commandIt.next();
            if (nextCommand.getCommandDisplay().equalsIgnoreCase(commandToRemove.getCommandDisplay())) {
                commandIt.remove();
            }
        }
    }

    private void removeDialog(Dialog dialog, Person person) {
        Iterator<Dialog> dialogs = person.getDialogs().iterator();
        while (dialogs.hasNext()) {
            Dialog personDialog = dialogs.next();

            if (dialog.getOptionDisplay().equalsIgnoreCase(personDialog.getOptionDisplay())) {
                dialogs.remove();
            }
        }

    }

    private void removeExecutionFromSet(Execution execution, ExecutionSet parentObject) {

        Iterator<Execution> executions = ((ExecutionSet) parentObject).getExecutions().iterator();
        while (executions.hasNext()) {
            Execution loopExecution = executions.next();

            // TODO - override equals in Execution rather than use toString
            if (loopExecution.toString().equalsIgnoreCase(execution.toString())) {
                executions.remove();
            }
        }
    }

    private void removeConditionFromSet(Condition validation, ConditionSet parentObject) {
        Iterator<Condition> conditions = ((ConditionSet) parentObject).getNestedConditions().iterator();
        while (conditions.hasNext()) {
            Condition loopCondition = conditions.next();

            // TODO - override equals in Condition rather than use toString as a check
            if (loopCondition.toString().equalsIgnoreCase(validation.toString())) {
                conditions.remove();
            }
        }
    }

    private void removeCommandValidation(Condition validation, Command command) {
        Iterator<Condition> validations = ((Command) command).getValidations().getNestedConditions().iterator();
        while (validations.hasNext()) {
            Condition loopCondition = validations.next();
            // TODO - override equals in Condition
            if (loopCondition.toString().equalsIgnoreCase(validation.toString())) {
                validations.remove();
            }
        }
    }

    private void removeSchedule(Schedule schedule, Person person) {
        Iterator<Schedule> schedules = ((Person) person).getSchedules().iterator();
        while (schedules.hasNext()) {
            Schedule loopSchedule = schedules.next();
            if (loopSchedule.getActionMessage().equalsIgnoreCase(schedule.getActionMessage())) {
                schedules.remove();
            }
        }
    }

    private void removeConsequence(Action consequence, Command parentCommand) {
        Iterator<Action> consequences = ((Command) parentCommand).getOutcomes().iterator();
        while (consequences.hasNext()) {
            Action loopConsequence = consequences.next();
            // TODO - find a better field to compare
            if (loopConsequence.getExecutionMessage().equalsIgnoreCase(consequence.getExecutionMessage())) {
                consequences.remove();
            }
        }
    }


    private void removeTurnAction(Action action, Entity parentObject) {
        Iterator<Action> actions = ((Entity) parentObject).getActions().iterator();
        while (actions.hasNext()) {
            Action loopAction = actions.next();
            // TODO - find a better field to compare
            if (loopAction.getExecutionMessage().equalsIgnoreCase(action.getExecutionMessage())) {
                actions.remove();
            }
        }
    }

    private void removePerson(Person person, Entity parentObject) {
        Iterator<Person> people = this.environment.getPeople().iterator();
        while (people.hasNext()) {
            Person loopPerson = people.next();
            if (loopPerson.getId().equalsIgnoreCase(person.getId())) {
                people.remove();
            }
        }
    }

    private void removeMass(Mass mass, Entity parentObject) {
        Iterator<Mass> masses = this.environment.getMasses().iterator();
        while (masses.hasNext()) {
            Mass loopMass = masses.next();
            if (loopMass.getId().equalsIgnoreCase(mass.getId())) {
                masses.remove();
            }
        }

        if (parentObject instanceof Location) {
            Iterator<String> massIds = ((Location) parentObject).getMasses().iterator();
            while (massIds.hasNext()) {
                String loopMassId = massIds.next();
                if (loopMassId.equalsIgnoreCase(mass.getId())) {
                    massIds.remove();
                }
            }
        }
    }

    private void removeLocation(Location location, Environment environment) {
        Iterator<Location> locations = environment.getLocations().iterator();
        while (locations.hasNext()) {
            Location loopLocation = locations.next();
            if (loopLocation.getId().equalsIgnoreCase(location.getId())) {
                locations.remove();
            }
        }
    }

    private void removeItem(Item item, Entity entity) {
        Iterator<Item> items = this.environment.getItems().iterator();
        while (items.hasNext()) {
            Item loopItem = items.next();
            if (loopItem.getId().equalsIgnoreCase(item.getId())) {
                items.remove();
            }
        }

        // now, remove the content item
        Iterator<String> itemIds = entity.getContents().iterator();
        while (itemIds.hasNext()) {
            String loopItemId = itemIds.next();
            if (loopItemId.equalsIgnoreCase(item.getId())) {
                itemIds.remove();
            }
        }

    }

    private void removeCommand(Command command, Entity entity) {
        Iterator<Command> commands = entity.getCommands().iterator();
        while (commands.hasNext()) {
            Command loopCommand = commands.next();
            if (loopCommand.getCommandDisplay().equalsIgnoreCase(command.getCommandDisplay())) {
                commands.remove();
            }
        }
    }

    private void removeExit(Exit exit, Location location) {
        Iterator<Exit> exits = location.getExits().iterator();
        while (exits.hasNext()) {
            Exit loopExit = exits.next();
            if (loopExit.getExitName().equalsIgnoreCase(exit.getExitName())) {
                exits.remove();
            }
        }
    }

    private void removeStoryPassage(Passage passage, StoryContent content) {
        Iterator<Passage> passages = content.getPassages().iterator();
        while (passages.hasNext()) {
            Passage loopPassage = passages.next();
            if (StringUtils.equalsIgnoreCase(loopPassage.getPassageId(), passage.getPassageId())) {
                passages.remove();
            }
        }

    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        // not used
    }

}
