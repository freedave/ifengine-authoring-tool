/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;

public class AddConsequenceAction extends TreeContextAction {

    private static final long serialVersionUID = -9062884697341898466L;

    public AddConsequenceAction() {
        super("Add Action", ModelIconBuilder.getIconForModelClass(Action.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        Action consequence = new Action();
        consequence.setExecutionMessage("Stuff happens.");

        // consequences can only be added to commands or actions
        if (parentModelObject instanceof Command) {
            ((Command) parentModelObject).addOutcome(consequence);
        }
        if (parentModelObject instanceof Entity) {
            ((Entity) parentModelObject).addAction(consequence);
        }

        TreeNodeAppender.appendActionNode(consequence, currentlySelectedTreeNode);
    }

}
