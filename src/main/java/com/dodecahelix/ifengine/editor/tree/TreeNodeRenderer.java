/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

public class TreeNodeRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = -5412675553876258122L;

    @Override
    public Component getTreeCellRendererComponent(JTree tree,
                                                  Object value,
                                                  boolean sel,
                                                  boolean expanded,
                                                  boolean leaf,
                                                  int row,
                                                  boolean hasFocus) {

        super.getTreeCellRendererComponent(tree,
        value,
        sel,
        expanded,
        leaf,
        row, hasFocus);

        Icon icon = null;
        if (value instanceof StoryElementNode) {
            StoryElementNode node = (StoryElementNode) value;
            icon = node.getIcon();
        }
        if (value instanceof FolderNode) {
            FolderNode node = (FolderNode) value;
            icon = node.getIcon();
        }

        if (icon != null) {
            this.setIcon(icon);
        }

        return this;
    }

}
