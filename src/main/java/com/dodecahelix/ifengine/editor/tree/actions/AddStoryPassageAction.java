/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.story.Passage;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

public class AddStoryPassageAction extends TreeContextAction {

    private static final long serialVersionUID = 240417176081604147L;

    public AddStoryPassageAction() {
        super("Add Story Passage", ModelIconBuilder.getIconForModelClass(Passage.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentObject) {
        if (parentObject instanceof StoryContent) {

            String passageId = "passage-" + Randomizer.getRandomString(7);

            Passage passage = new Passage(passageId, "Some passage text");
            ((StoryContent) parentObject).getPassages().add(passage);

            TreeNodeAppender.appendPassageNode(passage, this.currentlySelectedTreeNode);
        }
    }

}
