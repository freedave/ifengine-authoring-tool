/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import java.util.List;

import com.dodecahelix.ifengine.data.BooleanDisplay;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.editor.events.ErrorEvent;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.main.Events;

public class AddBooleanDisplayAction extends TreeContextAction {

    private static final long serialVersionUID = -6217919431300773432L;

    public AddBooleanDisplayAction() {
        super("Add Boolean Display", ModelIconBuilder.getIconForModelClass(BooleanDisplay.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {

        if (parentModelObject instanceof Property) {
            List<BooleanDisplay> displays = ((Property) parentModelObject).getBooleanDisplays();
            if (displays.size() == 0) {
                BooleanDisplay booleanDisplay = new BooleanDisplay("true", "false");
                displays.add(booleanDisplay);
                TreeNodeAppender.appendBooleanDisplayNode(booleanDisplay, currentlySelectedTreeNode);
            } else {
                Events.getInstance().getBus().post(new ErrorEvent("Cannot add more than one boolean display!"));
            }
        }
    }

}
