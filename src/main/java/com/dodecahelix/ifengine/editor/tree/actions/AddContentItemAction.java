/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

public class AddContentItemAction extends TreeContextAction {

    private static final long serialVersionUID = -1856372727871761604L;

    public AddContentItemAction() {
        super("Add Content Item", ModelIconBuilder.getIconForModelClass(Item.class));

    }


    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        // Question - should the action be done here, or in the EnvironmentTreeHandler, which has access to the environment and the tree
        if (parentModelObject instanceof Entity) {
            // property id should be unique for this object (although this is no guarantee)
            String id = "item-" + Randomizer.getRandomString(7);

            Item newItem = new Item(id, "name", "description");

            // the entity itself is added to the environment object in the model
            ((Entity) parentModelObject).addContentItem(id);
            environment.addItem(newItem);

            TreeNodeAppender.appendItemNode(newItem, this.currentlySelectedTreeNode);
        }
    }

}
