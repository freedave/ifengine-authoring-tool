/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;


public class AddConditionSetAction extends TreeContextAction {

    private static final long serialVersionUID = -6160515011356674212L;

    public AddConditionSetAction() {
        super("Add Subset", ModelIconBuilder.getIconForModelClass(ConditionSet.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {

        if (parentModelObject instanceof ConditionSet) {
            ConditionSet conditionSet = new ConditionSet();

            ((ConditionSet) parentModelObject).getSubsets().add(conditionSet);

            TreeNodeAppender.appendConditionSetNode(conditionSet, currentlySelectedTreeNode);
        }
    }

}
