/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

public class AddMassAction extends TreeContextAction {

    private static final long serialVersionUID = -599227961133154210L;

    public AddMassAction() {
        super("Add Mass", ModelIconBuilder.getIconForModelClass(Mass.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        String id = "mass-" + Randomizer.getRandomString(7);
        Mass mass = new Mass(id, "name", "description");

        // can only be added to locations and the environment?

        if (parentModelObject instanceof Location) {
            // the entity itself is added to the environment object in the model
            ((Location) parentModelObject).addMass(id);
            environment.addMass(mass);
        }

        if (parentModelObject instanceof Environment) {
            environment.addMass(mass);
        }

        TreeNodeAppender.appendMassNode(mass, this.currentlySelectedTreeNode, true);
    }

}
