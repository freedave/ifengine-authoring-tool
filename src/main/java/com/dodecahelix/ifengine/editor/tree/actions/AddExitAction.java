/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;

public class AddExitAction extends TreeContextAction {

    private static final long serialVersionUID = -8823329319989348392L;

    public AddExitAction() {
        super("Add Exit", ModelIconBuilder.getIconForModelClass(Exit.class));
    }


    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        // exits can only be added to locations
        if (parentModelObject instanceof Location) {
            Exit exit = new Exit();
            exit.setExitName("North");
            ((Location) parentModelObject).addExit(exit);

            TreeNodeAppender.appendExitNode(exit, currentlySelectedTreeNode);
        }
    }

}
