package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.story.Conclusion;
import com.dodecahelix.ifengine.data.story.Passage;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

/**
 * Created on 8/1/2016.
 */
public class AddStoryConclusionAction extends TreeContextAction {

    private static final long serialVersionUID = 240417176081604147L;

    public AddStoryConclusionAction() {
        super("Add Story Conclusion", ModelIconBuilder.getIconForModelClass(Conclusion.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentObject) {
        if (parentObject instanceof StoryContent) {

            String conclusionId = "conclusion-" + Randomizer.getRandomString(7);

            Conclusion conclusion = new Conclusion(conclusionId, "This is the end.");
            ((StoryContent) parentObject).getConclusions().add(conclusion);

            TreeNodeAppender.appendConclusionNode(conclusion, this.currentlySelectedTreeNode);
        }
    }
}
