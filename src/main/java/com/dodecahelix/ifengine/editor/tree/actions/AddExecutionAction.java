/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;

public class AddExecutionAction extends TreeContextAction {

    private static final long serialVersionUID = 8113270200897965210L;

    public AddExecutionAction() {
        super("Add Execution", ModelIconBuilder.getIconForModelClass(Execution.class));
    }


    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        Execution newExecution = new Execution(ExecutionType.INC_TIME_INTERVAL);
        newExecution.getEntityHolder().setSubjectType(EntityType.ENVIRONMENT);
        newExecution.setOperator(ExecutionOperator.SET);

        if (parentModelObject instanceof Action) {
            ((Action) parentModelObject).addResult(newExecution);
        }
        if (parentModelObject instanceof Dialog) {
            ((Dialog) parentModelObject).addResult(newExecution);
        }
        if (parentModelObject instanceof ExecutionSet) {
            ((ExecutionSet) parentModelObject).getExecutions().add(newExecution);
        }

        TreeNodeAppender.appendExecutionNode(newExecution, currentlySelectedTreeNode);
    }

}
