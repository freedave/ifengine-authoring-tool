/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.tree.actions;

import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.editor.tree.build.TreeNodeAppender;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.util.Randomizer;

public class AddNarratorAction extends TreeContextAction {

    private static final long serialVersionUID = -599227961133154210L;

    public AddNarratorAction() {
        super("Add Narrator", ModelIconBuilder.getIconForModelClass(Reader.class));
    }

    @Override
    protected void addNewEnvironmentObject(Object parentModelObject) {
        String id = "narrator-" + Randomizer.getRandomString(7);
        Reader reader = new Reader(id);
        reader.setTitle("Some Body");
        reader.setDescription("You are alive.");

        environment.getNarrators().add(reader);

        TreeNodeAppender.appendNarratorNode(reader, this.currentlySelectedTreeNode);
    }


}
