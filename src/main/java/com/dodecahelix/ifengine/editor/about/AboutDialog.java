/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.about;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.ScrollPaneConstants;

import net.miginfocom.swing.MigLayout;

import com.alee.extended.image.WebImage;
import com.alee.extended.label.WebLinkLabel;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.dodecahelix.ifengine.editor.forms.layout.LayoutHelper;

public class AboutDialog extends WebFrame {

    private static final long serialVersionUID = 9058645306949062738L;

    public AboutDialog() {
        setTitle("ClickFic Authoring Tool");
        setSize(400, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setShowMinimizeButton(false);
        setShowMaximizeButton(false);
        URL imgUrl = AboutDialog.class.getClassLoader().getResource("icons/logo_32.png");
        this.setIconImage(new ImageIcon(imgUrl).getImage());

        WebPanel contentPanel = buildContentPanel();
        WebScrollPane scrollPane = new WebScrollPane(contentPanel);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        this.add(scrollPane);
    }

    private WebPanel buildContentPanel() {
        WebPanel aboutPanel = new WebPanel(LayoutHelper.getDialogLayout());
        aboutPanel.setBackground(LayoutHelper.BG_COLOR);
        aboutPanel.setMargin(10);

        URL imgUrl = AboutDialog.class.getClassLoader().getResource("images/logo.png");
        WebImage logo = new WebImage(imgUrl);
        logo.setSize(100, 100);
        aboutPanel.add(logo);

        WebPanel group = new WebPanel(new MigLayout("wrap 1"));
        group.setBackground(LayoutHelper.BG_COLOR);
        group.add(new WebLabel("<html><b>ClickFic Authoring Tool</b></html>"));
        group.add(new WebLabel("version 1.0"));
        WebLinkLabel cfSiteLabel = new WebLinkLabel();
        cfSiteLabel.setLink("http://www.clickfic.com/");
        cfSiteLabel.setText("ClickFic Website");
        group.add(cfSiteLabel);
        aboutPanel.add(group);

        // add in a gap
        aboutPanel.add(new WebLabel(""), "span 2, gapbottom 10");

        // credits
        aboutPanel.add(new WebLabel("<html><b>Credits:</b></html>"), "span 2");
        aboutPanel.add(buildCreditsPanel(), "span 2");

        return aboutPanel;
    }

    /**
     * Reads from credits.txt in src/main/resources
     * @return
     */
    private WebPanel buildCreditsPanel() {

        WebPanel creditsPanel = new WebPanel(new MigLayout("wrap 1"));
        creditsPanel.setBackground(LayoutHelper.BG_COLOR);

        InputStream in = AboutDialog.class.getClassLoader().getResourceAsStream("credits.txt");
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = br.readLine()) != null) {
                appendCreditsLine(line, creditsPanel);
            }
        } catch (IOException ioe) {
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                ;
            }
        }

        return creditsPanel;
    }

    private void appendCreditsLine(String line, WebPanel creditsPanel) {
        if ("".equals(line.trim())) {
            // blank line
            creditsPanel.add(new WebLabel(""), "gapbottom 15");
        } else {
            if (line.startsWith("#")) {
                line = "<html><b>" + line.substring(1) + "</b></html>";
            }
            if (line.contains("http") && line.contains("URL")) {
                // special URL line
                int i = line.indexOf(":");
                String text = line.substring(0, i).trim();
                String url = line.substring(i + 1).trim();
                WebLinkLabel urlLabel = new WebLinkLabel();
                urlLabel.setLink(url);
                urlLabel.setText(text);
                creditsPanel.add(urlLabel);
            } else {
                creditsPanel.add(new WebLabel(line));
            }
        }

    }

}
