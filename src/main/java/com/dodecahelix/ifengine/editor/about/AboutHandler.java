/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.about;

import javax.swing.JFrame;

import com.dodecahelix.ifengine.editor.events.AboutEvent;
import com.google.common.eventbus.Subscribe;

public class AboutHandler {

    private AboutDialog dialog;

    public AboutHandler(JFrame mainFrame) {
        buildAboutDialog(mainFrame);
    }

    public void buildAboutDialog(JFrame owner) {
        dialog = new AboutDialog();
        //dialog.pack();
        dialog.setLocationRelativeTo(owner);
        dialog.setVisible(false);
    }

    @Subscribe
    public void handleAbout(AboutEvent event) {
        dialog.setVisible(true);
    }


}
