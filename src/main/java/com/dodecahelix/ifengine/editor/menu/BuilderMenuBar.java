/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.menu;

import java.net.URL;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import com.alee.laf.menu.WebMenu;
import com.alee.laf.menu.WebMenuBar;
import com.alee.laf.menu.WebMenuItem;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.main.actions.*;

public class BuilderMenuBar extends WebMenuBar {

    private static final long serialVersionUID = -6611345315798158305L;

    private ActionMap actionMap;

    public BuilderMenuBar(ActionMap actionMap) {
        this.actionMap = actionMap;

        this.add(buildFileMenu());
        this.add(buildAuthorMenu());
        this.add(buildHelpMenu());
    }

    private JMenu buildAuthorMenu() {
        WebMenu authorMenu = new WebMenu("View");

        authorMenu.add(buildFileMenuItem(actionMap.getBuildAction(), "ff/application_side_tree"));
        authorMenu.add(buildFileMenuItem(actionMap.getReadAction(), "ff/book_open"));

        return authorMenu;
    }

    private JMenu buildFileMenu() {
        WebMenu fileMenu = new WebMenu("File");

        fileMenu.add(buildFileMenuItem(actionMap.getOpenAction(), "ff/open"));
        fileMenu.add(buildFileMenuItem(actionMap.getSaveAction(), "ff/disk"));
        fileMenu.add(buildFileMenuItem(actionMap.getSaveAsAction()));
        fileMenu.add(buildFileMenuItem(actionMap.getNewFileAction(), "book_add"));
        fileMenu.addSeparator();
        fileMenu.add(buildFileMenuItem(actionMap.getImportEntitiesAction(), "ff/script_add"));
        fileMenu.add(buildFileMenuItem(actionMap.getExportEntitiesAction(), "ff/script_go"));
        fileMenu.addSeparator();
        fileMenu.add(buildFileMenuItem(actionMap.getExitAction(), "ff/door_out"));

        return fileMenu;
    }

    private JMenu buildHelpMenu() {
        WebMenu aboutMenu = new WebMenu("Help");

        aboutMenu.add(buildFileMenuItem(actionMap.getAboutAction(), "logo_16"));
        return aboutMenu;
    }

    public JMenuItem buildFileMenuItem(Action action) {
        return buildFileMenuItem(action, null);
    }

    public JMenuItem buildFileMenuItem(Action action, String icon) {
        WebMenuItem menuItem = new WebMenuItem(action);

        if (icon != null) {
            URL imgUrl = ModelIconBuilder.class.getClassLoader().getResource("icons/" + icon + ".png");
            ImageIcon imageIcon = new ImageIcon(imgUrl);
            menuItem.setIcon(imageIcon);
        }

        return menuItem;
    }

}
