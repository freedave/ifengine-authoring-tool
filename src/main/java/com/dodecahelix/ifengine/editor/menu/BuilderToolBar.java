/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.Action;
import javax.swing.ImageIcon;

import com.alee.global.StyleConstants;
import com.alee.laf.button.WebButton;
import com.alee.laf.toolbar.ToolbarStyle;
import com.alee.laf.toolbar.WebToolBar;
import com.alee.managers.language.data.TooltipWay;
import com.alee.managers.tooltip.TooltipManager;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;
import com.dodecahelix.ifengine.main.actions.*;

public class BuilderToolBar extends WebToolBar {

    private static final long serialVersionUID = 5329839941825838992L;

    public BuilderToolBar(ActionMap actionMap) {
        this.setToolbarStyle(ToolbarStyle.attached);

        this.add(buildButton(actionMap.getNewFileAction(), "New..", "book_add"));
        this.add(buildButton(actionMap.getOpenAction(), "Open..", "ff/open"));
        this.add(buildButton(actionMap.getSaveAction(), "Save..", "ff/disk"));
        this.addSeparator();
        this.add(buildButton(actionMap.getReadAction(), "Reader View", "ff/book_open"));
        this.add(buildButton(actionMap.getBuildAction(), "Editor View", "ff/application_side_tree"));
    }

    public WebButton buildButton(final Action action, String tooltip, String icon) {
        URL imgUrl = ModelIconBuilder.class.getClassLoader().getResource("icons/" + icon + ".png");
        ImageIcon imageIcon = new ImageIcon(imgUrl);
        WebButton button = WebButton.createIconWebButton(imageIcon, StyleConstants.smallRound, true);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                action.actionPerformed(event);
            }
        });

        TooltipManager.addTooltip(button, tooltip, TooltipWay.down, 0);
        return button;
    }

}
