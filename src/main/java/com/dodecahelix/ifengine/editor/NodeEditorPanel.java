/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor;

import java.awt.CardLayout;

import javax.swing.JPanel;

import com.dodecahelix.ifengine.data.story.Conclusion;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.editor.forms.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.panel.WebPanel;
import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.BooleanDisplay;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.ModelType;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.RangeDisplay;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.Schedule;
import com.dodecahelix.ifengine.data.Statistic;
import com.dodecahelix.ifengine.data.configuration.AnatomyConfiguration;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.data.configuration.DialogConfiguration;
import com.dodecahelix.ifengine.data.configuration.MessageConfiguration;
import com.dodecahelix.ifengine.data.configuration.TimeConfiguration;
import com.dodecahelix.ifengine.data.story.Passage;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.editor.events.FormDirtyStatusEvent;
import com.dodecahelix.ifengine.editor.events.LoadEnvironmentEvent;
import com.dodecahelix.ifengine.editor.events.SelectEnvironmentNodeEvent;
import com.dodecahelix.ifengine.editor.forms.config.AnatomyConfigurationForm;
import com.dodecahelix.ifengine.editor.forms.config.CommandConfigurationForm;
import com.dodecahelix.ifengine.editor.forms.config.DialogConfigurationForm;
import com.dodecahelix.ifengine.editor.forms.config.MessageConfigurationForm;
import com.dodecahelix.ifengine.editor.forms.config.TimeConfigurationForm;
import com.dodecahelix.ifengine.editor.tree.EnvironmentNode;
import com.dodecahelix.ifengine.editor.tree.FolderNode;
import com.dodecahelix.ifengine.main.Events;
import com.google.common.eventbus.Subscribe;

public class NodeEditorPanel extends WebPanel {

    private static final long serialVersionUID = 4295723336091560988L;

    static final Logger logger = LoggerFactory.getLogger(NodeEditorPanel.class.getName());

    private EnvironmentForm environmentForm;
    private LocationForm locationForm;
    private CommandForm commandForm;
    private ConditionForm conditionForm;
    private ConsequenceForm consequenceForm;
    private DialogForm dialogForm;
    private ExecutionForm executionForm;
    private ItemForm itemForm;
    private MassForm massForm;
    private PersonForm personForm;
    private PropertyForm propertyForm;
    private ReaderForm readerForm;
    private StatisticForm statisticForm;
    private ExitForm exitForm;
    private ScheduleForm scheduleForm;
    private StoryForm storyForm;
    private StoryContentForm storyContentForm;
    private StoryPassageForm storyPassageForm;
    private StoryConclusionForm storyConclusionForm;
    private ConditionSetForm conditionSetForm;
    private ExecutionSetForm executionSetForm;
    private RangeDisplayForm rangeDisplayForm;
    private BooleanDisplayForm booleanDisplayForm;

    private AnatomyConfigurationForm anatomyConfigForm;
    private CommandConfigurationForm commandConfigForm;
    private DialogConfigurationForm dialogConfigForm;
    private MessageConfigurationForm messageConfigForm;
    private TimeConfigurationForm timeConfigForm;

    private static CardLayout cards = new CardLayout();

    public NodeEditorPanel() {
        super(cards);

        buildAddForms();
        cards.show(this, "blank");

        Events.getInstance().getBus().register(this);
    }

    private void buildAddForms() {

        environmentForm = new EnvironmentForm("Environment");
        storyForm = new StoryForm("Story Info");
        locationForm = new LocationForm("Location");
        propertyForm = new PropertyForm("Property");
        commandForm = new CommandForm("Command");
        conditionForm = new ConditionForm("Condition");
        consequenceForm = new ConsequenceForm("Action");
        dialogForm = new DialogForm("Dialog");
        executionForm = new ExecutionForm("Execution");
        itemForm = new ItemForm("Item");
        massForm = new MassForm("Mass");
        personForm = new PersonForm("Person");
        readerForm = new ReaderForm("Reader");
        scheduleForm = new ScheduleForm("Schedule");
        exitForm = new ExitForm("Exit");
        statisticForm = new StatisticForm("Statistic");
        storyContentForm = new StoryContentForm("Story Content");
        storyPassageForm = new StoryPassageForm("Story Passage");
        storyConclusionForm = new StoryConclusionForm("Story Conclusion");
        executionSetForm = new ExecutionSetForm("Execution Set");
        conditionSetForm = new ConditionSetForm("Condition Set");
        rangeDisplayForm = new RangeDisplayForm("Range Display");
        booleanDisplayForm = new BooleanDisplayForm("Boolean Display");

        anatomyConfigForm = new AnatomyConfigurationForm("Anatomy Configuration");
        commandConfigForm = new CommandConfigurationForm("Command Configuration");
        dialogConfigForm = new DialogConfigurationForm("Dialog Configuration");
        messageConfigForm = new MessageConfigurationForm("Message Configuration");
        timeConfigForm = new TimeConfigurationForm("Time Configuration");

        this.add(environmentForm, ModelType.ENVIRONMENT.name());
        this.add(storyForm, ModelType.STORY_METADATA.name());
        this.add(locationForm, ModelType.LOCATION.name());
        this.add(propertyForm, ModelType.PROPERTY.name());
        this.add(commandForm, ModelType.COMMAND.name());
        this.add(conditionForm, ModelType.CONDITION.name());
        this.add(consequenceForm, ModelType.ACTION.name());
        this.add(dialogForm, ModelType.DIALOG.name());
        this.add(executionForm, ModelType.EXECUTION.name());
        this.add(itemForm, ModelType.ITEM.name());
        this.add(massForm, ModelType.MASS.name());
        this.add(personForm, ModelType.PERSON.name());
        this.add(readerForm, ModelType.READER.name());
        this.add(scheduleForm, ModelType.SCHEDULE.name());
        this.add(exitForm, ModelType.EXIT.name());
        this.add(statisticForm, ModelType.STATISTIC.name());
        this.add(storyContentForm, ModelType.STORY_CONTENT.name());
        this.add(storyPassageForm, ModelType.STORY_PASSAGE.name());
        this.add(storyConclusionForm, ModelType.STORY_CONCLUSION.name());
        this.add(conditionSetForm, ModelType.CONDITION_SET.name());
        this.add(executionSetForm, ModelType.EXECUTION_SET.name());
        this.add(rangeDisplayForm, ModelType.RANGE_DISPLAY.name());
        this.add(booleanDisplayForm, ModelType.BOOLEAN_DISPLAY.name());

        this.add(anatomyConfigForm, ModelType.ANATOMY_CONFIGURATION.name());
        this.add(commandConfigForm, ModelType.COMMAND_CONFIGURATION.name());
        this.add(dialogConfigForm, ModelType.DIALOG_CONFIGURATION.name());
        this.add(messageConfigForm, ModelType.MESSAGE_CONFIGURATION.name());
        this.add(timeConfigForm, ModelType.TIME_CONFIGURATION.name());

        this.add(new JPanel(), "blank");
    }

    @Subscribe
    public void showForm(SelectEnvironmentNodeEvent selectStoryElementEvent) {

        cards.show(this, "blank");

        Object storyElement = selectStoryElementEvent.getStoryElement();
        Environment environment = selectStoryElementEvent.getEnvironment();
        EnvironmentNode node = selectStoryElementEvent.getSelectedTreeNode();

        if (node instanceof FolderNode) {
            showFolderForm((FolderNode) node);
        } else {

            logger.debug("Showing form for object : " + selectStoryElementEvent.getStoryElement());

            if (storyElement instanceof Environment) {
                environmentForm.load((Environment) storyElement);
                cards.show(this, ModelType.ENVIRONMENT.name());
            }

            if (storyElement instanceof LibraryCard) {
                storyForm.load((LibraryCard) storyElement);
                cards.show(this, ModelType.STORY_METADATA.name());
            }

            if (storyElement instanceof StoryContent) {
                storyContentForm.load((StoryContent) storyElement);
                cards.show(this, ModelType.STORY_CONTENT.name());
            }

            if (storyElement instanceof Passage) {
                storyPassageForm.load((Passage) storyElement);
                cards.show(this, ModelType.STORY_PASSAGE.name());
            }

            if (storyElement instanceof Conclusion) {
                storyConclusionForm.load((Conclusion) storyElement);
                cards.show(this, ModelType.STORY_CONCLUSION.name());
            }

            if (storyElement instanceof Location) {
                locationForm.load((Location) storyElement, environment);
                cards.show(this, ModelType.LOCATION.name());
            }

            if (storyElement instanceof Command) {
                commandForm.load((Command) storyElement, environment);
                cards.show(this, ModelType.COMMAND.name());
            }

            if (storyElement instanceof Condition) {
                conditionForm.load((Condition) storyElement, environment);
                cards.show(this, ModelType.CONDITION.name());
            }

            if (storyElement instanceof Action) {
                consequenceForm.load((Action) storyElement);
                cards.show(this, ModelType.ACTION.name());
            }

            if (storyElement instanceof Dialog) {
                dialogForm.load((Dialog) storyElement, environment);
                cards.show(this, ModelType.DIALOG.name());
            }

            if (storyElement instanceof Execution) {
                executionForm.load((Execution) storyElement, environment);
                cards.show(this, ModelType.EXECUTION.name());
            }

            if (storyElement instanceof Exit) {
                exitForm.load((Exit) storyElement, environment);
                cards.show(this, ModelType.EXIT.name());
            }

            if (storyElement instanceof Item) {
                itemForm.load((Item) storyElement, environment);
                cards.show(this, ModelType.ITEM.name());
            }

            if (storyElement instanceof Mass) {
                massForm.load((Mass) storyElement, environment);
                cards.show(this, ModelType.MASS.name());
            }

            if (storyElement instanceof Person) {
                personForm.load((Person) storyElement, environment);
                cards.show(this, ModelType.PERSON.name());
            }

            if (storyElement instanceof Property) {
                propertyForm.load((Property) storyElement);
                cards.show(this, ModelType.PROPERTY.name());
            }

            if (storyElement instanceof Reader) {
                readerForm.load((Reader) storyElement, environment);
                cards.show(this, ModelType.READER.name());
            }

            if (storyElement instanceof Schedule) {
                scheduleForm.load((Schedule) storyElement, environment);
                cards.show(this, ModelType.SCHEDULE.name());
            }

            if (storyElement instanceof Statistic) {
                statisticForm.load((Statistic) storyElement);
                cards.show(this, ModelType.STATISTIC.name());
            }

            if (storyElement instanceof AnatomyConfiguration) {
                anatomyConfigForm.load((AnatomyConfiguration) storyElement);
                cards.show(this, ModelType.ANATOMY_CONFIGURATION.name());
            }

            if (storyElement instanceof CommandConfiguration) {
                commandConfigForm.load((CommandConfiguration) storyElement);
                cards.show(this, ModelType.COMMAND_CONFIGURATION.name());
            }

            if (storyElement instanceof DialogConfiguration) {
                dialogConfigForm.load((DialogConfiguration) storyElement);
                cards.show(this, ModelType.DIALOG_CONFIGURATION.name());
            }

            if (storyElement instanceof MessageConfiguration) {
                messageConfigForm.load((MessageConfiguration) storyElement);
                cards.show(this, ModelType.MESSAGE_CONFIGURATION.name());
            }

            if (storyElement instanceof TimeConfiguration) {
                timeConfigForm.load((TimeConfiguration) storyElement);
                cards.show(this, ModelType.TIME_CONFIGURATION.name());
            }

            if (storyElement instanceof ExecutionSet) {
                executionSetForm.load((ExecutionSet) storyElement, environment);
                cards.show(this, ModelType.EXECUTION_SET.name());
            }

            if (storyElement instanceof ConditionSet) {
                conditionSetForm.load((ConditionSet) storyElement, environment);
                cards.show(this, ModelType.CONDITION_SET.name());
            }

            if (storyElement instanceof RangeDisplay) {
                rangeDisplayForm.load((RangeDisplay) storyElement);
                cards.show(this, ModelType.RANGE_DISPLAY.name());
            }

            if (storyElement instanceof BooleanDisplay) {
                booleanDisplayForm.load((BooleanDisplay) storyElement);
                cards.show(this, ModelType.BOOLEAN_DISPLAY.name());
            }

            Events.getInstance().getBus().post(new FormDirtyStatusEvent(false));
        }

    }

    /**
     *   A new environment has loaded.  Clear any current forms.
     *
     * @param loadEnvironmentEvent
     */
    @Subscribe
    public void respondToNewEnvionment(LoadEnvironmentEvent loadEnvironmentEvent) {
        cards.show(this, "blank");
        Events.getInstance().getBus().post(new FormDirtyStatusEvent(false));
    }

    private void showFolderForm(FolderNode node) {

        ModelType modelType = node.getContextFilter();
        logger.debug("Showing folder form for context : " + modelType);
    }

}
