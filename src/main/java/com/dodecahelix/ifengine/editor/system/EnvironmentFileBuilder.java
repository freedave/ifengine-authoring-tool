/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.system;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.dodecahelix.ifengine.data.serializer.JsonStoryBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.serializer.json.JsonStoryBundleLoader;
import com.dodecahelix.ifengine.editor.events.ErrorEvent;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.util.IoUtils;

public class EnvironmentFileBuilder {

    static final Logger logger = LoggerFactory.getLogger(EnvironmentFileBuilder.class.getName());

    public static Environment buildEnvironmentFromStream(InputStream is) {

        String story = null;
        try {
            story = IoUtils.readFileFromInputStreamAsString(is);
        } catch (IOException e) {
            String message = "Failure loading story lines from inputstream";
            logger.error(message, e);

            // throw up an error dialog
            Events.getInstance().getBus().post(new ErrorEvent(message));
        }

        return buildFromStoryString(story);
    }

    public static Environment buildEnvironmentFromFile(File file) {

        String story = null;
        try {
            story = IoUtils.readFileAsString(file);
        } catch (IOException e) {
            String message = "Failure loading story lines from file : " + file.getName();
            logger.error(message, e);

            // throw up an error dialog
            Events.getInstance().getBus().post(new ErrorEvent(message));
        }

        return buildFromStoryString(story);
    }

    private static Environment buildFromStoryString(String story) {
        JsonStoryBundleLoader environmentBuilder = new JsonStoryBundleLoader();

        //environmentBuilder.setSerializedStory(new CsvStoryBundle(story));
        environmentBuilder.setBundle(new JsonStoryBundle(story));

        Environment env = environmentBuilder.build();
        logger.info("Environment built.");

        return env;
    }

}
