/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.system;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.dao.traversal.EnvironmentTraversal;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.FixEnvironmentEvent;
import com.dodecahelix.ifengine.editor.events.LoadEnvironmentEvent;
import com.dodecahelix.ifengine.main.Events;
import com.google.common.eventbus.Subscribe;

/**
 *   Before loading the environment into the tree, perform any batch fix..
 *
 */
public class EnvironmentFixer {

    static final Logger logger = LoggerFactory.getLogger(EnvironmentFixer.class.getName());

    public EnvironmentFixer() {
        Events.getInstance().getBus().register(this);
    }

    @Subscribe
    public void fixEnvironment(FixEnvironmentEvent event) {
        Environment environment = event.getEnvironment();
        try {
            logger.info("FIXING ENVIRONMENT FOR STORY " + environment.getLibraryCard().getTitle());

            //runReferenceIdFix(environment);

            logger.info("No fixes found to apply.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Events.getInstance().getBus().post(new LoadEnvironmentEvent(environment, event.getFilePath()));
        }
    }

    /**
     *   Keep this around as a template to demonstrate how these fixes should be applied
     *
     * @param environment
     */
    @SuppressWarnings("unused")
    private void runReferenceIdFix(Environment environment) {
        logger.info("Adding default referenceId's to all commands and dialogs.");
        // add referenceId's to all commands and dialogs
        EnvironmentTraversal traversal = new CommandAndDialogFixEnvironmentTraversal();
        traversal.traverseEnvironment(environment);
        logger.info("referenceId fix applied");
    }

}
