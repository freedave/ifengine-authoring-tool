/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.system;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.Schedule;
import com.dodecahelix.ifengine.editor.events.EntityIdChangeEvent;
import com.dodecahelix.ifengine.editor.events.LoadEnvironmentEvent;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.util.StringUtils;
import com.google.common.eventbus.Subscribe;

/**
 *   If an Entity ID is updated in the form, the references to this ID need to be updated everywhere in the model
 *
 *   This may be an expensive and slow operation if there are a lot of objects in the tree.  
 *   TODO - performance profiling on this class
 *
 */
public class EntityIdChangeHandler {

    static final Logger logger = LoggerFactory.getLogger(EntityIdChangeHandler.class.getName());

    private Environment environment;

    public EntityIdChangeHandler() {
        Events.getInstance().getBus().register(this);
    }

    @Subscribe
    public void updateEnvironment(LoadEnvironmentEvent event) {
        this.environment = event.getEnvironment();
    }

    @Subscribe
    public void updateEntityIdReferences(EntityIdChangeEvent event) {
        String oldId = event.getOriginalId();
        String newId = event.getNewId();
        EntityType changedEntity = event.getEntityType();

        String msg = String.format("update references of %s from %s to %s", changedEntity, oldId, newId);
        logger.debug("attempting to " + msg);

        try {
            updateEntityIdReferences(changedEntity, oldId, newId);
        } catch (Exception e) {
            logger.error("unable to " + msg, e);
        }
    }

    public void updateEntityIdReferences(EntityType changedEntity, String oldId, String newId) {

        if (EntityType.ITEM == changedEntity) {
            updateContentItemReferences(environment, oldId, newId);
        }

        if (EntityType.NARRATOR == changedEntity) {
            if (StringUtils.equalsIgnoreCase(environment.getCurrentNarrator(), oldId)) {
                environment.setCurrentNarrator(newId);
            }
        }

        for (Reader narrator : environment.getNarrators()) {
            updateEntityIdReferencesInCommands(narrator.getCommands(), changedEntity, oldId, newId);

            if (EntityType.ITEM == changedEntity) {
                updateContentItemReferences(narrator, oldId, newId);
            }

            if (EntityType.LOCATION == changedEntity) {
                updateNarratorCurrentLocation(narrator, oldId, newId);
            }
        }


        for (Location location : environment.getLocations()) {
            if (EntityType.ITEM == changedEntity) {
                updateContentItemReferences(location, oldId, newId);
            }

            //  update location refs in exit
            if (EntityType.LOCATION == changedEntity) {
                for (Exit exit : location.getExits()) {
                    if (exit.getToLocationId().equalsIgnoreCase(oldId)) {
                        exit.setToLocationId(newId);
                    }
                }
            }

            //  update mass refs in location
            if (EntityType.MASS == changedEntity) {
                updateMassIdReferences(location, oldId, newId);
            }

            updateEntityIdReferencesInCommands(location.getCommands(), changedEntity, oldId, newId);
        }
        for (Mass mass : environment.getMasses()) {
            if (EntityType.ITEM == changedEntity) {
                updateContentItemReferences(mass, oldId, newId);
            }

            updateEntityIdReferencesInCommands(mass.getCommands(), changedEntity, oldId, newId);
        }
        for (Person person : environment.getPeople()) {
            if (EntityType.ITEM == changedEntity) {
                updateContentItemReferences(person, oldId, newId);
            }

            updateEntityIdReferencesInSchedules(person.getSchedules(), changedEntity, oldId, newId);

            updateEntityIdReferencesInCommands(person.getCommands(), changedEntity, oldId, newId);
        }

        for (Item item : environment.getItems()) {
            updateEntityIdReferencesInCommands(item.getCommands(), changedEntity, oldId, newId);
        }

        updateEntityIdReferencesInSharedConditions(environment.getSharedConditions(), changedEntity, oldId, newId);
        updateEntityIdReferencesInSharedExecutions(environment.getSharedExecutions(), changedEntity, oldId, newId);
    }

    private void updateNarratorCurrentLocation(Reader narrator, String oldId, String newId) {
        String currentLocation = narrator.getCurrentLocation();
        if (StringUtils.equalsIgnoreCase(oldId, currentLocation)) {
            narrator.setCurrentLocation(newId);
        }
    }

    /**
     *   Updates the location in schedule
     *
     * @param schedules
     * @param changedEntity
     * @param oldId
     * @param newId
     */
    private void updateEntityIdReferencesInSchedules(List<Schedule> schedules, EntityType changedEntity, String oldId, String newId) {
        if (EntityType.LOCATION == changedEntity) {
            for (Schedule schedule : schedules) {
                String locationId = schedule.getLocationId();
                if (StringUtils.equalsIgnoreCase(locationId, oldId)) {
                    schedule.setLocationId(newId);
                }
            }
        }
    }

    /**
     *   Updates all conditions in the shared condition sets (including nested conditions)
     *
     * @param sharedConditions
     * @param changedEntity
     * @param oldId
     * @param newId
     */
    private void updateEntityIdReferencesInSharedConditions(Set<ConditionSet> sharedConditions, EntityType changedEntity, String oldId, String newId) {
        for (ConditionSet conditionSet : sharedConditions) {
            for (Condition referenceCondition : conditionSet.getNestedConditions()) {
                updateEntityIdReferenceInCondition(referenceCondition, changedEntity, oldId, newId);
            }
        }
    }


    private void updateEntityIdReferencesInSharedExecutions(Set<ExecutionSet> sharedExecutions, EntityType changedEntity, String oldId, String newId) {
        for (ExecutionSet executionSet : sharedExecutions) {
            for (Execution execution : executionSet.getExecutions()) {
                updateEntityIdReferenceInExecution(execution, changedEntity, oldId, newId);
            }
        }
    }

    /**
     *  Update the conditions and executions of the commands of this entity
     *
     * @param entityToUpdate
     * @param changedEntity
     * @param oldId
     * @param newId
     */
    private void updateEntityIdReferencesInCommands(List<Command> commands, EntityType changedEntity, String oldId, String newId) {
        if (commands != null) {
            for (Command command : commands) {
                // TODO - what about subsets??
                for (Condition validation : command.getValidations().getNestedConditions()) {
                    updateEntityIdReferenceInCondition(validation, changedEntity, oldId, newId);
                }

                for (Execution execution : command.getDefaultResults().getExecutions()) {
                    updateEntityIdReferenceInExecution(execution, changedEntity, oldId, newId);
                }

                for (Action consequence : command.getOutcomes()) {
                    for (Condition condition : consequence.getValidations().getNestedConditions()) {
                        updateEntityIdReferenceInCondition(condition, changedEntity, oldId, newId);
                    }
                    for (Execution execution : consequence.getResults().getExecutions()) {
                        updateEntityIdReferenceInExecution(execution, changedEntity, oldId, newId);
                    }
                }
            }
        }

    }

    private void updateEntityIdReferenceInExecution(Execution execution, EntityType changedEntity, String oldId, String newId) {

        EntityHolder entityHolder = execution.getEntityHolder();

        if (changedEntity == entityHolder.getSubjectType()) {
            if (oldId.equalsIgnoreCase(entityHolder.getSubjectEntityId())) {
                entityHolder.setSubjectEntityId(newId);
            }
        }

        if (changedEntity == entityHolder.getTargetType()) {
            if (oldId.equalsIgnoreCase(entityHolder.getTargetEntityId())) {
                entityHolder.setTargetEntityId(newId);
            }
        }

    }

    private void updateEntityIdReferenceInCondition(Condition condition, EntityType changedEntity, String oldId, String newId) {

        EntityHolder entityHolder = condition.getEntityHolder();

        // check to see if subject changed
        if (changedEntity == entityHolder.getSubjectType()) {
            if (oldId.equalsIgnoreCase(entityHolder.getSubjectEntityId())) {
                entityHolder.setSubjectEntityId(newId);
            }
        }

        if (changedEntity == entityHolder.getTargetType()) {
            if (oldId.equalsIgnoreCase(entityHolder.getTargetEntityId())) {
                entityHolder.setTargetEntityId(newId);
            }
        }

    }

    private void updateMassIdReferences(Location location, String oldId, String newId) {
        if (location.getMasses() != null) {
            boolean removed = false;
            Iterator<String> iter = location.getMasses().iterator();
            while (iter.hasNext()) {
                if (iter.next().equalsIgnoreCase(oldId)) {
                    iter.remove();
                    removed = true;
                }
            }

            if (removed && newId != null) {
                location.addMass(newId);
            }
        }
    }

    private void updateContentItemReferences(Entity entity, String oldItemId, String newItemId) {
        if (entity.getContents() != null) {
            boolean removed = false;
            Iterator<String> iter = entity.getContents().iterator();
            while (iter.hasNext()) {
                if (iter.next().equalsIgnoreCase(oldItemId)) {
                    iter.remove();
                    removed = true;
                }
            }

            if (removed && newItemId != null) {
                entity.addContentItem(newItemId);
            }
        }
    }

}
