/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.system;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.dao.traversal.DefaultEnvironmentTraversal;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.util.Randomizer;

/**
 *
 *  This traversal will find all commands and dialogs and ensure that they both have reference ID's
 *
 */
public class CommandAndDialogFixEnvironmentTraversal extends DefaultEnvironmentTraversal {

    static final Logger logger = LoggerFactory.getLogger(DefaultEnvironmentTraversal.class.getName());

    @Override
    public void processCommand(Command command) {

        if (command.getReferenceId() == null) {
            logger.info("found a command without a referenceId : " + command.getCommandDisplay());

            String randomId = "command-" + Randomizer.getRandomString(10);
            command.setReferenceId(randomId);
        }
    }

    @Override
    public void processDialog(Dialog dialog) {
        if (dialog.getReferenceId() == null) {
            logger.info("found a dialog without a referenceId : " + dialog.getOptionDisplay());

            String randomId = "dialog-" + Randomizer.getRandomString(10);
            dialog.setReferenceId(randomId);
        }

        dialog.setOnce(false);
        dialog.setDisabled(false);
    }


}
