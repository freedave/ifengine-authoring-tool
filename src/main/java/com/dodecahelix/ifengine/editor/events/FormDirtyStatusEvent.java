/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.events;

/**
 *   Indicates whether the current form is clean or dirty (true = dirty; false = clean)
 *
 *   For any changes to text fields or dropdowns, the form will be set to dirty.
 *   If the form is saved or reset, the form will be set to clean.
 *
 *   The currently displayed form will not allow itself to be changed if it is dirty
 *
 */
public class FormDirtyStatusEvent {

    private boolean dirty = true;

    public FormDirtyStatusEvent(boolean isDirty) {
        this.dirty = isDirty;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

}
