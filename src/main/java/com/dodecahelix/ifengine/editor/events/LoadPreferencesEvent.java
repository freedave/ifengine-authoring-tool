/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.events;

import java.util.Properties;

public class LoadPreferencesEvent {

    private Properties preferences;

    public LoadPreferencesEvent(Properties props) {
        this.preferences = props;
    }

    public Properties getPreferences() {
        return preferences;
    }

    public void setPreferences(Properties preferences) {
        this.preferences = preferences;
    }


}
