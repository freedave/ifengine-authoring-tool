/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.events;

import javax.swing.tree.TreeNode;

/**
 *  Signal that a model change has been made and that the tree should be refreshed
 *
 */
public class TreeRefreshEvent {

    private TreeNode nodeToRefresh;

    public TreeRefreshEvent() {
    }

    public TreeRefreshEvent(TreeNode nodeToRefresh) {
        this.nodeToRefresh = nodeToRefresh;
    }

    public TreeNode getNodeToRefresh() {
        return nodeToRefresh;
    }
}
