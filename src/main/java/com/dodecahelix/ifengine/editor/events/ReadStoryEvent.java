/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.events;

public class ReadStoryEvent {

    private String storyFileName;

    public ReadStoryEvent(String storyFileName) {
        super();
        this.storyFileName = storyFileName;
    }

    public String getStoryFileName() {
        return storyFileName;
    }

    public void setStoryFileName(String storyFileName) {
        this.storyFileName = storyFileName;
    }

}
