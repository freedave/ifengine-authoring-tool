/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.events;

import com.dodecahelix.ifengine.data.Environment;

/**
 *   Event to indicate that a new environment model has been loaded into the application
 *
 */
public class LoadEnvironmentEvent {

    private Environment env;
    private String loadLocation;

    public LoadEnvironmentEvent(Environment environment, String loadLocation) {
        super();
        this.env = environment;
        this.loadLocation = loadLocation;
    }

    public Environment getEnvironment() {
        return env;
    }

    public String getLoadLocation() {
        return loadLocation;
    }

}
