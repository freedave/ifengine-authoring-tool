/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.events;

public class ErrorEvent {

    private String errorDisplay;
    private String title;

    public ErrorEvent(String errorDisplay) {
        this(null, errorDisplay);
    }

    public ErrorEvent(String title, String display) {
        this.errorDisplay = display;
        this.title = title;
    }

    public String getErrorDisplay() {
        return errorDisplay;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
