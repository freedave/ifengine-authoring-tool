/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.events;

import com.dodecahelix.ifengine.data.EntityType;

/**
 *
 * If an entity ID has changed, we must parse through the entire environment tree and update references to that entity
 *
 */
public class EntityIdChangeEvent {

    private String originalId;
    private String newId;
    private EntityType entityType;

    public EntityIdChangeEvent(EntityType entityType, String originalId, String newId) {
        this.originalId = originalId;
        this.newId = newId;
        this.entityType = entityType;
    }

    public String getOriginalId() {
        return originalId;
    }

    public String getNewId() {
        return newId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

}
