/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.events;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.tree.EnvironmentNode;

/**
 *   Event that indicates that a story element has been selected in the tree view
 *
 * @author dpeters
 *
 */
public class SelectEnvironmentNodeEvent {

    private Environment environment;
    private EnvironmentNode treeNode;

    public SelectEnvironmentNodeEvent(EnvironmentNode node, Environment environment) {
        this.treeNode = node;
        this.environment = environment;
    }

    public Object getStoryElement() {
        return treeNode.getUserObject();
    }

    public Environment getEnvironment() {
        return environment;
    }

    public EnvironmentNode getSelectedTreeNode() {
        return treeNode;
    }

}
