/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor;

import java.awt.Dimension;

import javax.swing.JSplitPane;

import com.alee.laf.panel.WebPanel;
import com.alee.laf.splitpane.WebSplitPane;
import com.dodecahelix.ifengine.editor.tree.EnvironmentTreePanel;

public class BuilderPanel extends WebPanel {

    private static final long serialVersionUID = 8121535513878747880L;

    public BuilderPanel() {

        EnvironmentTreePanel environmentTreePanel = new EnvironmentTreePanel();
        NodeEditorPanel nodeEditorPanel = new NodeEditorPanel();

        WebSplitPane splitPane = new WebSplitPane(JSplitPane.HORIZONTAL_SPLIT, environmentTreePanel, nodeEditorPanel);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(350);

        // Provide minimum sizes for the two components in the split pane
        Dimension minimumSize = new Dimension(100, 50);
        environmentTreePanel.setMinimumSize(minimumSize);
        nodeEditorPanel.setMinimumSize(minimumSize);

        this.add(splitPane);
    }

}
