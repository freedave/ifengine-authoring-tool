/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.entity;

import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.editor.forms.SubjectFormPanel;
import com.dodecahelix.ifengine.editor.forms.TargetFormPanel;
import com.dodecahelix.ifengine.result.ExecutionType;

/**
 *
 *  Populates the subject and target panels based on the ConditionType or ExecutionType selected
 *
 */
public class EntityHolderDefaultValuesTool {

    private SubjectFormPanel subjectPanel;
    private TargetFormPanel targetPanel;

    public EntityHolderDefaultValuesTool(SubjectFormPanel subjectPanel, TargetFormPanel targetPanel) {
        this.subjectPanel = subjectPanel;
        this.targetPanel = targetPanel;
    }

    public void populateByConditionType(ConditionType conditionType) {
        EntityType subjectEntityType;
        switch (conditionType) {
            case OWNED:
                subjectEntityType = EntityType.ITEM;
                break;
            case IS_WEARABLE:
                subjectEntityType = EntityType.ITEM;
                break;
            case IS_WEARING:
                subjectEntityType = EntityType.ITEM;
                break;
            case TOO_BIG_TO_CARRY:
                subjectEntityType = EntityType.ITEM;
                break;
            case IN_LOCATION:
                subjectEntityType = EntityType.READER;
                break;
            case KNOWS_TOPIC:
                subjectEntityType = EntityType.READER;
                break;
            case IS_LIGHT_SOURCE:
                subjectEntityType = EntityType.ITEM;
                break;
            case HOLDING_LIGHT_SOURCE:
                subjectEntityType = EntityType.READER;
                break;
            case IS_DARK:
                subjectEntityType = EntityType.LOCATION;
                break;
            default:
                subjectEntityType = EntityType.ENVIRONMENT;
        }
        populateSubjectEntityType(subjectEntityType);

        TargetType targetType;
        EntityType targetEntityType;
        switch (conditionType) {
            case OWNED:
                targetType = TargetType.ENTITY;
                targetEntityType = EntityType.READER;
                break;
            case IN_LOCATION:
                targetType = TargetType.ENTITY;
                targetEntityType = EntityType.CURRENT_LOCATION;
                break;
            case IS_WEARABLE:
                targetType = TargetType.ENTITY;
                targetEntityType = EntityType.READER;
                break;
            case IS_WEARING:
                targetType = TargetType.ENTITY;
                targetEntityType = EntityType.READER;
                break;
            case TOO_BIG_TO_CARRY:
                targetType = TargetType.ENTITY;
                targetEntityType = EntityType.READER;
                break;
            default:
                targetType = TargetType.STATIC;
                targetEntityType = null;
        }
        populateTargetTypeAndEntityType(targetType, targetEntityType);
    }

    public void populateByExecutionType(ExecutionType executionType) {
        EntityType subjectEntityType;
        switch (executionType) {
            case CHOWN_ITEM:
                subjectEntityType = EntityType.ITEM;
                break;
            case ADD_TOPIC:
                subjectEntityType = EntityType.READER;
                break;
            case WEAR_ITEM:
                subjectEntityType = EntityType.ITEM;
                break;
            case OVERRIDE_SCHEDULE:
                subjectEntityType = EntityType.PERSON;
                break;
            case ENABLE_SCHEDULE:
                subjectEntityType = EntityType.PERSON;
                break;
            case ENABLE_DIALOG:
                subjectEntityType = EntityType.PERSON;
                break;
            case MOVE_ENTITY:
                subjectEntityType = EntityType.READER;
                break;
            case SWITCH_NARRATOR:
                subjectEntityType = EntityType.NARRATOR;
                break;
            case SET_DARKNESS:
                subjectEntityType = EntityType.LOCATION;
                break;
            case SET_LIGHT_SOURCE:
                subjectEntityType = EntityType.ITEM;
                break;
            case FOLLOW:
                subjectEntityType = EntityType.PERSON;
                break;
            default:
                subjectEntityType = EntityType.ENVIRONMENT;
        }
        populateSubjectEntityType(subjectEntityType);

        TargetType targetType;
        EntityType targetEntityType;
        switch (executionType) {
            case CHOWN_ITEM:
                targetType = TargetType.ENTITY;
                targetEntityType = EntityType.READER;
                break;
            case WEAR_ITEM:
                targetType = TargetType.ENTITY;
                targetEntityType = EntityType.READER;
                break;
            case MOVE_ENTITY:
                targetType = TargetType.ENTITY;
                targetEntityType = EntityType.LOCATION;
                break;
            default:
                targetType = TargetType.STATIC;
                targetEntityType = null;
        }
        populateTargetTypeAndEntityType(targetType, targetEntityType);

    }

    private void populateSubjectEntityType(EntityType entityType) {
        EntityHolder holder = new EntityHolder();
        holder.setSubjectType(entityType);
        subjectPanel.load(holder);
    }

    private void populateTargetTypeAndEntityType(TargetType targetType, EntityType entityType) {
        EntityHolder holder = new EntityHolder();
        holder.setTargetType(entityType);

        targetPanel.load(targetType, holder);
    }

}
