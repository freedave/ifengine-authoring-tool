/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.label.WebLabel;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;

public class PropertyForm extends AbstractForm {

    private static final long serialVersionUID = 104446385640862219L;

    private Property property;

    private WebTextField propertyIdField;
    private WebTextField propertyValueField;

    public PropertyForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        propertyIdField = new TattletaleWebTextField();
        propertyValueField = new TattletaleWebTextField();

        form.add(new WebLabel("Property ID:"), "");
        form.add(propertyIdField, "wrap");

        form.add(new WebLabel("Property Value:"), "");
        form.add(propertyValueField, "wrap 15");
    }

    public void load(Property property) {
        this.property = property;

        this.clearForm();

        propertyIdField.setText(property.getPropertyId());
        propertyValueField.setText(property.getStringValue());
    }

    @Override
    public void saveForm() {
        super.saveForm();

        property.setPropertyId(propertyIdField.getText());
        property.parseObjectAndTypeFromString(propertyValueField.getText());
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        load(property);
    }

}
