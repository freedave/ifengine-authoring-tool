/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.entity.EntityHolderDefaultValuesTool;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;

public class ExecutionForm extends AbstractForm {

    private static final long serialVersionUID = -8407978494220484781L;

    private Execution loadedExecution;
    private Environment environment;

    WebComboBox executionTypeDropdown;
    WebComboBox executionOperatorDropdown;

    TargetFormPanel targetFormPanel;
    SubjectFormPanel subjectFormPanel;

    WebLabel executionTypeExplanation;

    public ExecutionForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        executionTypeDropdown = new TattletaleWebComboBox(ExecutionType.values());
        executionOperatorDropdown = new TattletaleWebComboBox(ExecutionOperator.values());

        form.add(new WebLabel("Type:"), "");
        form.add(executionTypeDropdown, "wrap");

        executionTypeExplanation = new WebLabel("");

        form.add(executionTypeExplanation, "w 325!, span 2, wrap");

        this.addFormSeparator();

        subjectFormPanel = new SubjectFormPanel();
        subjectFormPanel.buildForm(form);

        this.addFormSeparator();

        form.add(new WebLabel("Operator:"), "");
        form.add(executionOperatorDropdown, "wrap");

        this.addFormSeparator();

        targetFormPanel = new TargetFormPanel();
        targetFormPanel.buildForm(form);

        executionTypeDropdown.addItemListener(new ItemListener() {

            EntityHolderDefaultValuesTool defaultValuesTool = new EntityHolderDefaultValuesTool(subjectFormPanel, targetFormPanel);

            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                Object selectedItem = itemEvent.getItem();
                if (selectedItem instanceof ExecutionType) {
                    executionTypeExplanation.setText("<html>" + ((ExecutionType) selectedItem).getDescription() + "</html>");
                    defaultValuesTool.populateByExecutionType((ExecutionType) selectedItem);
                }
            }
        });
    }

    @Override
    public void clearForm() {
        executionOperatorDropdown.setSelectedIndex(-1);
        subjectFormPanel.clear();
        targetFormPanel.clear();
    }

    @Override
    public void resetForm() {
        super.resetForm();

        // reload the condition and environment
        if (loadedExecution != null) {
            this.load(loadedExecution, environment);
        } else {
            this.clearForm();
        }
    }

    public void load(Execution execution, Environment environment) {
        this.clearForm();

        this.loadedExecution = execution;
        this.environment = environment;
        subjectFormPanel.setEnvironment(environment);
        targetFormPanel.setEnvironment(environment);

        // order of loading is important - prepopulate the form with condition type before loading values
        executionTypeDropdown.setSelectedItem(execution.getExecutionType());
        if (execution.getOperator() != null) {
            executionOperatorDropdown.setSelectedItem(execution.getOperator());
        }

        subjectFormPanel.load(execution.getEntityHolder(), environment);
        targetFormPanel.load(execution.getTargetType(),
        execution.getEntityHolder(),
        execution.getTargetStaticValue(),
        environment);

    }

    @Override
    public void saveForm() {
        super.saveForm();

        EntityHolder entityHolder = new EntityHolder();
        entityHolder = subjectFormPanel.populateEntityHolder(entityHolder);
        entityHolder = targetFormPanel.populateEntityHolder(entityHolder);
        loadedExecution.setEntityHolder(entityHolder);

        loadedExecution.setExecutionType((ExecutionType) executionTypeDropdown.getSelectedItem());
        loadedExecution.setOperator((ExecutionOperator) executionOperatorDropdown.getSelectedItem());
        loadedExecution.setTargetStaticValue(targetFormPanel.getTargetStaticValue());
        loadedExecution.setTargetType(targetFormPanel.getTargetType());
    }

}
