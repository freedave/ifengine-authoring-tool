/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.Collection;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.list.WebList;
import com.alee.laf.list.WebListModel;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.menu.WebPopupMenu;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.editor.events.FormDirtyStatusEvent;
import com.dodecahelix.ifengine.editor.forms.layout.LayoutHelper;
import com.dodecahelix.ifengine.main.Events;

/**
 * Scrollable list box with a set of string values
 * <p>
 * Has an input field at the bottom to add new items to the list
 * <p>
 * Able to delete a string from its row in the field
 *
 *
 */
public class AppendableListField extends WebPanel {

    private static final long serialVersionUID = 5786891563314515040L;

    private WebList listBox;
    protected WebListModel<String> list;

    private WebTextField appendFieldComponent;

    private static ImageIcon deleteIcon = new ImageIcon(AppendableListField.class.getClassLoader().getResource("icons/delete.png"));

    public AppendableListField() {
        super(LayoutHelper.getPadlessFormLayout());

        this.setBackground(LayoutHelper.BG_COLOR);

        // Editable list
        list = new WebListModel<String>();
        listBox = new WebList(list);
        listBox.setVisibleRowCount(6);
        listBox.setSelectedIndex(0);
        listBox.setEditable(false);

        listBox.addMouseListener(listFieldMouseListener);

        this.add(new WebScrollPane(listBox), "span 2, growx, wrap");

        this.add(buildAppendFieldComponent(), "w 150!");

        URL imgUrl = AppendableListField.class.getClassLoader().getResource("icons/add.png");
        ImageIcon addIcon = new ImageIcon(imgUrl);
        WebButton addButton = new WebButton("Add", addIcon);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                appendField();
            }
        });

        this.add(addButton, "w 80!, wrap");

        WebLabel infoLabel = new WebLabel("NOTE: Right-click on selection to remove");
        infoLabel.setFontSize(10);
        this.add(infoLabel, "span 2, wrap");
    }

    protected JComponent buildAppendFieldComponent() {
        appendFieldComponent = new WebTextField(15);
        appendFieldComponent.setInputPrompt("Add a value...");
        appendFieldComponent.setInputPromptFont(appendFieldComponent.getFont().deriveFont(Font.ITALIC));

        return appendFieldComponent;
    }

    protected void appendField() {
        String newValue = appendFieldComponent.getText();
        list.addElement(newValue);

        Events.getInstance().getBus().post(new FormDirtyStatusEvent(true));

        appendFieldComponent.clear();
    }

    public void clear() {
        list.clear();
        list.removeAllElements();
    }

    public void loadList(Collection<String> entries) {
        list.addElements(entries);
    }

    public List<String> getValues() {
        return list.getElements();
    }

    public WebListModel<String> getList() {
        return list;
    }

    AbstractAction deleteFieldAction = new AbstractAction("remove", deleteIcon) {

        private static final long serialVersionUID = 3868141461285791771L;

        @Override
        public void actionPerformed(ActionEvent arg0) {
            int selectedItem = AppendableListField.this.listBox.getSelectedIndex();
            AppendableListField.this.list.remove(selectedItem);
        }

    };

    MouseListener listFieldMouseListener = new MouseListener() {

        @Override
        public void mouseClicked(MouseEvent event) {
            if (SwingUtilities.isRightMouseButton(event)) {

                WebPopupMenu contextMenu = new WebPopupMenu();

                WebMenuItem menuItem = new WebMenuItem(deleteFieldAction);
                contextMenu.add(menuItem);
                contextMenu.show(event.getComponent(), event.getX(), event.getY());
            }
        }

        @Override
        public void mouseEntered(MouseEvent arg0) {
        }

        @Override
        public void mouseExited(MouseEvent arg0) {
        }

        @Override
        public void mousePressed(MouseEvent arg0) {
        }

        @Override
        public void mouseReleased(MouseEvent arg0) {
        }
    };

}
