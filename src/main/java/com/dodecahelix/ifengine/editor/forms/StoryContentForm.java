/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.label.WebLabel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextArea;

public class StoryContentForm extends AbstractForm {

    private static final long serialVersionUID = 7094236703961873953L;

    private StoryContent loadedStory;

    private WebTextArea introductionField;

    public StoryContentForm(String title) {
        super(title);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void buildForm() {
        super.buildForm();

        introductionField = new TattletaleWebTextArea(6, 10);
        introductionField.setWrapStyleWord(true);
        introductionField.setLineWrap(true);
        WebScrollPane introductionScroll = new WebScrollPane(introductionField);

        form.add(new WebLabel("Introduction:"), "wrap");
        form.add(introductionScroll, "span 2, growx, wrap 15");

        form.add(new WebLabel("Right click on story content node to add passages to the story."), "span 2, wrap");
    }

    public void load(StoryContent story) {
        this.loadedStory = story;

        this.clearForm();

        introductionField.setText(story.getIntroduction());
    }

    @Override
    public void saveForm() {
        // TODO Auto-generated method stub
        super.saveForm();

        loadedStory.setIntroduction(introductionField.getText());
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        if (loadedStory != null) {
            this.load(loadedStory);
        }
    }


}
