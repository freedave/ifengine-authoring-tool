/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import com.alee.laf.text.WebTextArea;
import com.dodecahelix.ifengine.editor.events.FormDirtyStatusEvent;
import com.dodecahelix.ifengine.main.Events;

/**
 *   Sends out an event when text is entered into the field
 *
 */
public class TattletaleWebTextArea extends WebTextArea {

    private static final long serialVersionUID = 7591751014615041105L;

    public TattletaleWebTextArea() {
        super();

        addChangeListener();
    }

    public TattletaleWebTextArea(int rows, int columns) {
        super(rows, columns);

        addChangeListener();
    }

    private void addChangeListener() {

        this.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent ke) {
                // TODO - make dirty on a ctrl-v
                if (!ke.isControlDown()) {
                    Events.getInstance().getBus().post(new FormDirtyStatusEvent(true));
                }
            }
        });
    }

}
