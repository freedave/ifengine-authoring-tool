/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.config;

import com.alee.laf.label.WebLabel;
import com.dodecahelix.ifengine.data.configuration.AnatomyConfiguration;
import com.dodecahelix.ifengine.editor.forms.AbstractForm;
import com.dodecahelix.ifengine.editor.forms.components.AppendableListField;

public class AnatomyConfigurationForm extends AbstractForm {

    private static final long serialVersionUID = -2598068596131716930L;

    private AnatomyConfiguration anatomyConfig;
    private AppendableListField bodyPartsField;

    public AnatomyConfigurationForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        bodyPartsField = new AppendableListField();

        form.add(new WebLabel("Body Parts:"), "wrap");
        form.add(bodyPartsField, "span 2, growx");
    }

    @Override
    public void clearForm() {
        super.clearForm();

        bodyPartsField.clear();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        anatomyConfig.setBodyParts(bodyPartsField.getValues());
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        load(anatomyConfig);
    }


    public void load(AnatomyConfiguration anatomyConfig) {
        this.anatomyConfig = anatomyConfig;
        this.clearForm();

        bodyPartsField.loadList(anatomyConfig.getBodyParts());
    }

}
