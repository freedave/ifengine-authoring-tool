/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.entity;

import java.awt.Component;

import javax.swing.JList;

import com.alee.laf.combobox.WebComboBoxCellRenderer;
import com.alee.laf.label.WebLabel;
import com.dodecahelix.ifengine.data.Entity;

public class EntityIdComboboxItemRenderer extends WebComboBoxCellRenderer {

    @Override
    public Component getListCellRendererComponent(@SuppressWarnings("rawtypes") JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Component dropdownItem = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (value != null
        && (value instanceof Entity)
        && (dropdownItem instanceof WebLabel)) {

            Entity item = (Entity) value;
            ((WebLabel) dropdownItem).setText(item.getTitle());
        }

        return dropdownItem;
    }

}
