/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import java.awt.CardLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;
import com.dodecahelix.ifengine.editor.forms.entity.EntityComboBoxHelper;
import com.dodecahelix.ifengine.editor.forms.entity.EntityIdComboboxItemRenderer;
import com.dodecahelix.ifengine.editor.forms.entity.EntityIdDropdownListener;
import com.dodecahelix.ifengine.editor.forms.entity.EntityTypeDropdownListener;
import com.dodecahelix.ifengine.editor.forms.layout.FormWebPanel;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *   Reusable in forms, for selecting target type
 *
 */
public class TargetFormPanel extends FormWebPanel {

    private static final long serialVersionUID = -1482726918140687038L;

    private Environment loadedEnvironment;

    WebComboBox targetEntityTypeDropdown;
    WebComboBox targetEntityIdDropdown;
    WebComboBox targetEntityPropertyIdDropdown;

    WebComboBox targetTypeDropdown;

    WebTextField targetStaticValueField;

    // switchable panels (cards)
    WebPanel targetEntityPanel;
    WebPanel targetStaticValuePanel;

    // card panel
    WebPanel targetContentPanel;
    CardLayout contentCards;

    private EntityTypeDropdownListener entityTypeDropdownListener;
    private EntityIdDropdownListener entityIdDropdownListener;

    @SuppressWarnings("unchecked")
    public void buildForm(WebPanel form) {

        targetTypeDropdown = new TattletaleWebComboBox(TargetType.values());
        targetTypeDropdown.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                Object selectedItem = itemEvent.getItem();
                if (selectedItem instanceof TargetType) {
                    contentCards.show(targetContentPanel, ((TargetType) selectedItem).name());
                }
            }

        });

        targetEntityTypeDropdown = new TattletaleWebComboBox(EntityType.values());
        targetEntityIdDropdown = new TattletaleWebComboBox();
        targetEntityIdDropdown.setRenderer(new EntityIdComboboxItemRenderer());
        targetEntityPropertyIdDropdown = new TattletaleWebComboBox();

        // need to make this editable, since in the case of "local" the properties of all entity types are not known
        targetEntityPropertyIdDropdown.setEditable(true);

        targetStaticValueField = new TattletaleWebTextField();

        form.add(new WebLabel("Target:"), "");
        form.add(targetTypeDropdown, "wrap");

        targetEntityPanel = new FormWebPanel();
        targetEntityPanel.add(new WebLabel("Target Entity Type:"), "");
        targetEntityPanel.add(targetEntityTypeDropdown, "wrap");
        targetEntityPanel.add(new WebLabel("Target Entity ID:"), "");
        targetEntityPanel.add(targetEntityIdDropdown, "wrap");
        targetEntityPanel.add(new WebLabel("Target Property ID:"), "");
        targetEntityPanel.add(targetEntityPropertyIdDropdown, "wrap");

        targetStaticValuePanel = new FormWebPanel();
        targetStaticValuePanel.add(new WebLabel("Target Static Value:"), "");
        targetStaticValuePanel.add(targetStaticValueField, "wrap");

        contentCards = new CardLayout();
        targetContentPanel = new WebPanel(contentCards);
        targetContentPanel.add(targetEntityPanel, TargetType.ENTITY.name());
        targetContentPanel.add(targetStaticValuePanel, TargetType.STATIC.name());

        // don't show any of the subpanels
        targetContentPanel.add(new FormWebPanel(), "blank");
        form.add(targetContentPanel, "span 2, wrap");

        // add listeners to detect entity type or ID changes
        entityIdDropdownListener = new EntityIdDropdownListener(targetEntityTypeDropdown, targetEntityPropertyIdDropdown);
        entityTypeDropdownListener = new EntityTypeDropdownListener(targetEntityIdDropdown, targetEntityPropertyIdDropdown, false, true);
        targetEntityTypeDropdown.addItemListener(entityTypeDropdownListener);
        targetEntityIdDropdown.addItemListener(entityIdDropdownListener);
    }

    public void clear() {
        // not applicable
        this.targetTypeDropdown.setSelectedIndex(-1);
        this.targetEntityTypeDropdown.setSelectedIndex(-1);
        this.targetEntityIdDropdown.removeAllItems();
        this.targetEntityPropertyIdDropdown.removeAllItems();
        this.targetStaticValueField.clear();

        contentCards.show(targetContentPanel, "blank");
    }

    public void load(TargetType targetType,
                     EntityHolder entityHolder,
                     String targetStaticValue,
                     Environment environment) {

        this.clear();
        this.setEnvironment(environment);

        if (targetType != null) {
            targetTypeDropdown.setSelectedItem(targetType);

            updateTargetTypeComboboxes(targetType,
            entityHolder,
            targetStaticValue,
            environment);
        }
    }

    public void load(TargetType targetType, EntityHolder entityHolder) {
        targetTypeDropdown.setSelectedItem(targetType);
        updateTargetTypeComboboxes(targetType, entityHolder, "", loadedEnvironment);
    }

    /**
     *   loads up the environment for entity id and property population
     *
     * @param environment
     */
    public void setEnvironment(Environment environment) {
        this.loadedEnvironment = environment;
        entityIdDropdownListener.setEnvironment(environment);
        entityTypeDropdownListener.setEnvironment(environment);
    }


    @SuppressWarnings("unchecked")
    private void updateTargetTypeComboboxes(TargetType targetType,
                                            EntityHolder entityHolder,
                                            String staticValue,
                                            Environment environment) {

        if (TargetType.ENTITY.equals(targetType)) {
            contentCards.show(targetContentPanel, TargetType.ENTITY.name());

            if (entityHolder.getTargetType() != null) {
                targetEntityTypeDropdown.setSelectedItem(entityHolder.getTargetType());

                EntityComboBoxHelper.updateEntityIdCombobox(targetEntityIdDropdown,
                targetEntityPropertyIdDropdown,
                entityHolder.getTargetType(),
                entityHolder.getTargetEntityId(),
                entityHolder.getTargetProperty(),
                environment,
                false, true);
            }
        }

        if (TargetType.RANDOM.equals(targetType)) {
            contentCards.show(targetContentPanel, TargetType.STATIC.name());
            targetStaticValueField.setText(staticValue);
        }

        if (TargetType.STATIC.equals(targetType)) {
            contentCards.show(targetContentPanel, TargetType.STATIC.name());
            targetStaticValueField.setText(staticValue);
        }
    }

    public EntityHolder populateEntityHolder(EntityHolder holder) {
        EntityType entityType = (EntityType) targetEntityTypeDropdown.getSelectedItem();
        String entityId = (String) targetEntityIdDropdown.getSelectedItem();
        String propId = (String) targetEntityPropertyIdDropdown.getSelectedItem();

        holder.setTargetType(entityType);

        if (!StringUtils.isEmpty(entityId)) {
            holder.setTargetEntityId(entityId);
        }
        if (!StringUtils.isEmpty(propId)) {
            holder.setTargetProperty(propId);
        }

        return holder;
    }

    public String getTargetStaticValue() {
        return targetStaticValueField.getText();
    }

    public TargetType getTargetType() {
        return (TargetType) targetTypeDropdown.getSelectedItem();
    }

}
