/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.extended.button.WebSwitch;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.editor.forms.components.OverlayLabel;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebSwitch;

public class MassForm extends AbstractEntityForm<Mass> {

    private static final long serialVersionUID = 4934649628235732425L;

    private WebSwitch inOrOnField;

    public MassForm(String titleText) {
        super(titleText, true);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        inOrOnField = new TattletaleWebSwitch();
        form.add(new OverlayLabel("ON container (not IN): ", "<html>ON for an ON container (i.e; put on top of)<br>OFF for an IN container (i.e; put inside of)</html>"), "");
        form.add(inOrOnField, "w 75!, wrap");
        inOrOnField.setSelected(false);
    }

    public void load(Mass mass, Environment environment) {
        super.load(mass, environment);

        inOrOnField.setSelected(mass.isOnNotIn());
    }

    @Override
    public void clearForm() {
        super.clearForm();

        inOrOnField.setSelected(false);
    }

    @Override
    public void saveForm() {
        super.saveForm();

        loadedEntity.setOnNotIn(inOrOnField.isSelected());
    }

}
