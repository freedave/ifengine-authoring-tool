/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import java.util.List;

import javax.swing.JComponent;

import com.alee.laf.combobox.WebComboBox;

public class ComboboxAppendableListField extends AppendableListField {

    private static final long serialVersionUID = -9080467760849397753L;

    private WebComboBox comboboxAppendFieldComponent;

    @Override
    protected JComponent buildAppendFieldComponent() {
        comboboxAppendFieldComponent = new WebComboBox();
        return comboboxAppendFieldComponent;
    }

    @Override
    protected void appendField() {
        String newValue = (String) comboboxAppendFieldComponent.getSelectedItem();
        if (!list.getElements().contains(newValue)) {
            list.addElement(newValue);
        }
    }

    @Override
    public void clear() {
        super.clear();
        comboboxAppendFieldComponent.removeAllItems();
    }

    @SuppressWarnings("unchecked")
    public void loadOptions(List<String> options) {

        for (String option : options) {
            comboboxAppendFieldComponent.addItem(option);
        }

    }

}
