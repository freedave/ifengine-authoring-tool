/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import com.alee.laf.panel.WebPanel;

/**
 *   Multiple spinner fields, side by side
 * @author dpeters
 *
 */
public class MultiSpinnerPanel extends WebPanel {

    private static final long serialVersionUID = -4073298752101340824L;


}
