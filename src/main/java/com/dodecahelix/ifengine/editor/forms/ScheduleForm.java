/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.extended.button.WebSwitch;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Schedule;
import com.dodecahelix.ifengine.editor.forms.components.OverlayLabel;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebSwitch;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextArea;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;
import com.dodecahelix.ifengine.editor.forms.components.TimeRangeField;
import com.dodecahelix.ifengine.editor.forms.entity.EntityIdComboboxItemRenderer;

public class ScheduleForm extends AbstractForm {

    private static final long serialVersionUID = -4961757151470395397L;

    private Environment environment;
    private Schedule loadedSchedule;

    WebSwitch enabledField;
    WebTextField referenceIdField;
    WebTextArea actionMessageField;
    WebComboBox locationIdField;

    private TimeRangeField timeRangePanel;

    public ScheduleForm(String title) {
        super(title);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void buildForm() {
        super.buildForm();

        referenceIdField = new TattletaleWebTextField();
        form.add(new OverlayLabel("Reference ID: ", "<html>reference id to be used in executions<br>for setting override</html>"), "");
        form.add(referenceIdField, "wrap");

        actionMessageField = new TattletaleWebTextArea(5, 10);
        actionMessageField.setWrapStyleWord(true);
        actionMessageField.setLineWrap(true);
        WebScrollPane actionMsgScroll = new WebScrollPane(actionMessageField);

        form.add(new OverlayLabel("Action Message: ", "<html>when the reader enters the location<br>and this schedule is active<br>print this message to the console</html>"), "wrap");
        form.add(actionMsgScroll, "span 2, growx, wrap 15");

        locationIdField = new TattletaleWebComboBox();
        locationIdField.setRenderer(new EntityIdComboboxItemRenderer());
        form.add(new WebLabel("Location:"), "");
        form.add(locationIdField, "wrap");

        this.addFormSeparator();

        enabledField = new TattletaleWebSwitch();
        form.add(new OverlayLabel("Enabled: ", "<html>set to false if the schedule should be initially disabled</html>"), "");
        form.add(enabledField, "w 75!, wrap");
        enabledField.setSelected(true);

        timeRangePanel = new TimeRangeField();
        form.add(timeRangePanel, "span 2, wrap");
    }

    @SuppressWarnings("unchecked")
    public void load(Schedule schedule, Environment environment) {
        this.loadedSchedule = schedule;
        this.environment = environment;

        this.clearForm();

        this.referenceIdField.setText(schedule.getReferenceId());
        this.actionMessageField.setText(schedule.getActionMessage());
        this.enabledField.setSelected(schedule.isEnabled());

        for (Location location : environment.getLocations()) {
            locationIdField.addItem(location);
            if (schedule.getLocationId() != null && schedule.getLocationId().equalsIgnoreCase(location.getId())) {
                locationIdField.setSelectedItem(location);
            }
        }

        timeRangePanel.load(schedule.getTimeRange(), environment);
    }

    @Override
    public void clearForm() {
        super.clearForm();

        enabledField.setSelected(true);
        actionMessageField.setText("");
        locationIdField.removeAllItems();
        timeRangePanel.clear();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        loadedSchedule.setReferenceId(referenceIdField.getText());
        loadedSchedule.setActionMessage(actionMessageField.getText());
        loadedSchedule.setLocationId(((Location) locationIdField.getSelectedItem()).getId());
        loadedSchedule.setEnabled(enabledField.isSelected());

        timeRangePanel.save();
    }

    @Override
    protected void resetForm() {
        super.resetForm();
        timeRangePanel.reset();

        this.load(loadedSchedule, environment);
    }


}
