/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.entity;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import com.alee.laf.combobox.WebComboBox;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.util.StringUtils;

public class EntityIdDropdownListener implements ItemListener {

    private Environment environment;

    private WebComboBox entityPropertyDropdown;
    private WebComboBox entityTypeDropdown;

    public EntityIdDropdownListener(WebComboBox entityTypeDropdown,
                                    WebComboBox entityPropertyDropdown) {

        this.entityPropertyDropdown = entityPropertyDropdown;
        this.entityTypeDropdown = entityTypeDropdown;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void itemStateChanged(ItemEvent entityIdSelectionEvent) {

        String entityId = (String) entityIdSelectionEvent.getItem();

        if (entityIdSelectionEvent.getStateChange() == ItemEvent.SELECTED
        && environment != null
        && !StringUtils.isEmpty(entityId)) {

            EntityType entityType = (EntityType) entityTypeDropdown.getSelectedItem();
            EntityComboBoxHelper.updateEntityPropertyIdCombobox(entityPropertyDropdown,
            entityType,
            entityId,
            null,
            environment);
        }
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

}
