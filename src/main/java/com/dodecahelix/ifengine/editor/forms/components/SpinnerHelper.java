/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import javax.swing.SpinnerNumberModel;

import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.spinner.WebSpinner;
import com.dodecahelix.ifengine.editor.forms.layout.LayoutHelper;

public class SpinnerHelper {

    public static SpinnerNumberModel createSpinnerModel() {
        SpinnerNumberModel numberModel = new SpinnerNumberModel();
        numberModel.setMaximum(new Integer(100));
        return numberModel;
    }

    public static WebPanel createSpinnerPanel(WebSpinner spinner, String label, String tooltip) {
        WebPanel spinnerPanel = new WebPanel(LayoutHelper.getPadlessFormLayout());
        spinnerPanel.setBackground(LayoutHelper.BG_COLOR);

        if (tooltip == null) {
            spinnerPanel.add(new WebLabel(label), "");
        } else {
            spinnerPanel.add(new OverlayLabel(label, tooltip), "");
        }

        spinnerPanel.add(spinner, "");

        return spinnerPanel;
    }

    public static WebPanel createDoubleSpinnerPanel(WebPanel spinnerOne, WebPanel spinnerTwo) {
        WebPanel doublePanel = new WebPanel(LayoutHelper.getDefaultFormLayout());
        doublePanel.setBackground(LayoutHelper.BG_COLOR);
        doublePanel.add(spinnerOne, "gapright 20");
        doublePanel.add(spinnerTwo);

        return doublePanel;
    }
}
