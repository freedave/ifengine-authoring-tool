/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.editor.forms.components.ComboboxAppendableListField;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.entity.EntityIdComboboxItemRenderer;
import com.dodecahelix.ifengine.util.StringUtils;

public class ReaderForm extends AbstractEntityForm<Reader> {

    private static final long serialVersionUID = -4457504985538897782L;

    private WebComboBox startingLocationDropdown;
    private ComboboxAppendableListField topicsField;

    public ReaderForm(String title) {
        super(title, true);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void buildForm() {
        super.buildForm();

        topicsField = new ComboboxAppendableListField();

        startingLocationDropdown = new TattletaleWebComboBox();
        startingLocationDropdown.setRenderer(new EntityIdComboboxItemRenderer());
        form.add(new WebLabel("Starting Location:"), "");
        form.add(startingLocationDropdown, "wrap 25");

        form.add(new WebLabel("Known Topics: "), "wrap");
        form.add(topicsField, "span 2, growx, wrap 15");
    }

    @SuppressWarnings("unchecked")
    @Override
    public void load(Reader reader, Environment environment) {
        super.load(reader, environment);

        topicsField.loadList(reader.getKnownTopics());
        topicsField.loadOptions(environment.getConfiguration().getDialogConfiguration().getTopics());

        for (Location location : environment.getLocations()) {
            startingLocationDropdown.addItem(location);
            if (StringUtils.equalsIgnoreCase(reader.getCurrentLocation(), location.getId())) {
                startingLocationDropdown.setSelectedItem(location);
            }
        }

    }

    @Override
    public void clearForm() {
        super.clearForm();

        topicsField.clear();
        startingLocationDropdown.removeAllItems();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        this.loadedEntity.setKnownTopics(topicsField.getValues());

        Location location = (Location) startingLocationDropdown.getSelectedItem();
        this.loadedEntity.setCurrentLocation(location.getId());
    }


}
