/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.editor.events.FormDirtyStatusEvent;
import com.dodecahelix.ifengine.main.Events;

/**
 *  Sends out an event when text is entered into the field
 *
 */
public class TattletaleWebTextField extends WebTextField {

    private static final long serialVersionUID = 963893071957352790L;

    public TattletaleWebTextField() {
        super();

        this.addChangeListener();
    }

    private void addChangeListener() {
        this.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent ke) {
                // TODO - make dirty on a ctrl-v
                if (!ke.isControlDown()) {
                    Events.getInstance().getBus().post(new FormDirtyStatusEvent(true));
                }
            }
        });
    }

}
