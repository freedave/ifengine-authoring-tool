/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.config;

import com.dodecahelix.ifengine.data.configuration.MessageConfiguration;
import com.dodecahelix.ifengine.editor.forms.AbstractForm;

public class MessageConfigurationForm extends AbstractForm {

    private static final long serialVersionUID = -6949131199742819969L;

    public MessageConfigurationForm(String title) {
        super(title);
        // TODO Auto-generated constructor stub
    }

    public void load(MessageConfiguration storyElement) {
        // TODO Auto-generated method stub

    }

}
