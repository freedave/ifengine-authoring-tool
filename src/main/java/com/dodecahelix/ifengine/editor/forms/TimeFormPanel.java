/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import javax.swing.SpinnerNumberModel;

import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.spinner.WebSpinner;
import com.dodecahelix.ifengine.data.Time;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebSpinner;
import com.dodecahelix.ifengine.editor.forms.layout.FormWebPanel;
import com.dodecahelix.ifengine.editor.forms.layout.LayoutHelper;

public class TimeFormPanel extends FormWebPanel {

    private static final long serialVersionUID = 7792350868459630429L;

    private Time loadedTime;

    private WebPanel timeFieldPanel;

    WebSpinner incrementField;
    WebSpinner periodField;
    WebSpinner weekdayField;
    WebSpinner weekField;
    WebSpinner monthField;
    WebSpinner yearField;

    public void buildForm(WebPanel form, String label) {

        timeFieldPanel = new WebPanel(LayoutHelper.getPadlessFormLayout());

        incrementField = new TattletaleWebSpinner(generateSpinnerModel());
        addSpinnerToForm(incrementField, "Increment:");

        periodField = new TattletaleWebSpinner(generateSpinnerModel());
        addSpinnerToForm(periodField, "Period:");

        weekdayField = new TattletaleWebSpinner(generateSpinnerModel());
        addSpinnerToForm(weekdayField, "Weekday:");

        weekField = new TattletaleWebSpinner(generateSpinnerModel());
        addSpinnerToForm(weekField, "Week:");

        monthField = new TattletaleWebSpinner(generateSpinnerModel());
        addSpinnerToForm(monthField, "Month:");

        yearField = new TattletaleWebSpinner(generateSpinnerModel());
        addSpinnerToForm(yearField, "Year:");

        WebPanel timeFieldWrapper = new WebPanel(LayoutHelper.getPadlessFormLayout());
        timeFieldWrapper.setBackground(LayoutHelper.BG_COLOR);
        timeFieldWrapper.add(new WebLabel(label), "gapright 10");
        timeFieldWrapper.add(timeFieldPanel, "wrap");

        form.add(timeFieldWrapper, "span 2, wrap");
    }

    private SpinnerNumberModel generateSpinnerModel() {
        SpinnerNumberModel numberModel = new SpinnerNumberModel();
        numberModel.setMaximum(new Integer(1000));
        return numberModel;
    }

    private void addSpinnerToForm(WebSpinner spinner, String label) {
        WebPanel formPanel = new WebPanel(LayoutHelper.getPadlessFormLayout());
        formPanel.setBackground(LayoutHelper.BG_COLOR);
        formPanel.add(new WebLabel(label), "");
        formPanel.add(spinner, "");

        timeFieldPanel.add(formPanel, "span 2, wrap");
    }

    public void loadTime(Time time) {
        this.loadedTime = time;

        this.clear();

        incrementField.setValue(time.getIncrement());
        periodField.setValue(time.getPeriod());
        weekdayField.setValue(time.getWeekday());
        weekField.setValue(time.getWeek());
        monthField.setValue(time.getMonth());
        yearField.setValue(time.getYear());
    }

    public void saveTime() {
        this.loadedTime.setIncrement(Integer.parseInt(incrementField.getValue().toString()));
        this.loadedTime.setPeriod(Integer.parseInt(periodField.getValue().toString()));
        this.loadedTime.setWeekday(Integer.parseInt(weekdayField.getValue().toString()));
        this.loadedTime.setWeek(Integer.parseInt(weekField.getValue().toString()));
        this.loadedTime.setMonth(Integer.parseInt(monthField.getValue().toString()));
        this.loadedTime.setYear(Integer.parseInt(yearField.getValue().toString()));
    }

    public void clear() {
        // TODO Auto-generated method stub

    }
}
