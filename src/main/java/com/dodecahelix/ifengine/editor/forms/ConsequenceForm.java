/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.extended.button.WebSwitch;
import com.alee.laf.label.WebLabel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.editor.forms.components.OverlayLabel;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebSwitch;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextArea;

public class ConsequenceForm extends AbstractForm {

    private static final long serialVersionUID = -9010250430101555948L;

    private Action loadedConsequence;

    WebTextArea executionMessageField;
    //WebCheckBox soloCheckBox;
    WebSwitch soloCheckbox;

    public ConsequenceForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        //soloCheckBox = new WebCheckBox();
        soloCheckbox = new TattletaleWebSwitch();

        executionMessageField = new TattletaleWebTextArea(8, 10);
        executionMessageField.setWrapStyleWord(true);
        executionMessageField.setLineWrap(true);
        WebScrollPane msgScroll = new WebScrollPane(executionMessageField);

        form.add(new WebLabel("Execution Msg:"), "wrap");
        form.add(msgScroll, "span 2, growx, wrap");

        this.addFormSeparator();

        form.add(new OverlayLabel("Single Execution: ", "<html>If this value is set and the action is valid<br>no other actions will be fired.</html>"), "");
        form.add(soloCheckbox, "w 75!, wrap");
    }

    public void load(Action consequence) {
        this.loadedConsequence = consequence;

        executionMessageField.setText(consequence.getExecutionMessage());
        soloCheckbox.setSelected(consequence.isSolo());
    }

    @Override
    public void clearForm() {
        super.clearForm();

        executionMessageField.clear();
        soloCheckbox.setSelected(false);
    }

    @Override
    public void saveForm() {
        super.saveForm();

        this.loadedConsequence.setExecutionMessage(executionMessageField.getText());
        this.loadedConsequence.setSolo(soloCheckbox.isSelected());
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        if (loadedConsequence != null) {
            this.load(loadedConsequence);
        }
    }

}
