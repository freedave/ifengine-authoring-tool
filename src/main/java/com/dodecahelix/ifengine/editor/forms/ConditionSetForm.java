/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.List;

import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.forms.components.ComboboxAppendableListField;
import com.dodecahelix.ifengine.editor.forms.components.OverlayLabel;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;
import com.dodecahelix.ifengine.editor.forms.layout.FormWebPanel;
import com.dodecahelix.ifengine.util.StringUtils;

public class ConditionSetForm extends AbstractForm {

    private static final long serialVersionUID = -3432929213090068026L;

    private ConditionSet conditionSet;
    private Environment environment;

    /**
     * there are two types of ConditionSets - standard sets and shared sets
     */
    private boolean standardSet;

    private CardLayout setTypeCards;

    private WebPanel cardHolderPanel;
    private WebPanel sharedSetPanel;
    private WebPanel standardSetPanel;

    private WebTextField conditionSetIdField;
    private WebComboBox andOrDropdownStandard;
    private WebComboBox andOrDropdownShared;

    private ComboboxAppendableListField referenceConditionsField;

    public ConditionSetForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        setTypeCards = new CardLayout();
        cardHolderPanel = new WebPanel(setTypeCards);
        sharedSetPanel = new FormWebPanel();
        standardSetPanel = new FormWebPanel();
        cardHolderPanel.add(sharedSetPanel, "shared");
        cardHolderPanel.add(standardSetPanel, "standard");

        conditionSetIdField = new TattletaleWebTextField();
        andOrDropdownStandard = new TattletaleWebComboBox(new String[]{"AND", "OR"});
        andOrDropdownShared = new TattletaleWebComboBox(new String[]{"AND", "OR"});
        referenceConditionsField = new ComboboxAppendableListField();

        sharedSetPanel.add(new WebLabel("Reference ID:"), "");
        sharedSetPanel.add(conditionSetIdField, "wrap");

        sharedSetPanel.add(new OverlayLabel("Logic Type: ", "<html>AND - all conditions, reference conditions and subsets must pass<br>OR - only one must pass for the condition to be met</html>"), "");
        sharedSetPanel.add(andOrDropdownShared, "wrap 15");

        standardSetPanel.add(new OverlayLabel("Logic Type: ", "<html>AND - all conditions, reference conditions and subsets must pass<br>OR - only one must pass for the condition to be met</html>"), "");
        standardSetPanel.add(andOrDropdownStandard, "wrap 15");

        standardSetPanel.add(new WebLabel("Reference Condition Sets: "), "wrap");
        standardSetPanel.add(referenceConditionsField, "span 2, growx, wrap");

        form.add(cardHolderPanel, "span 2, wrap, growy");
        setTypeCards.show(cardHolderPanel, "standard");
    }

    @Override
    public void clearForm() {
        super.clearForm();

        this.conditionSetIdField.clear();
        referenceConditionsField.clear();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        boolean andSet = false;

        if (standardSet) {
            conditionSet.getReferenceConditions().clear();
            conditionSet.getReferenceConditions().addAll(referenceConditionsField.getValues());
            if (andOrDropdownStandard.getSelectedIndex() == 0) {
                andSet = true;
            }
        } else {
            conditionSet.setReferenceId(conditionSetIdField.getText());
            if (andOrDropdownShared.getSelectedIndex() == 0) {
                andSet = true;
            }
        }

        conditionSet.setAndSet(andSet);
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        load(conditionSet, environment);
    }

    public void load(ConditionSet storyElement, Environment environment) {
        clearForm();

        this.conditionSet = storyElement;
        this.environment = environment;

        if (storyElement.isAndSet()) {
            andOrDropdownStandard.setSelectedIndex(0);
            andOrDropdownShared.setSelectedIndex(0);
        } else {
            andOrDropdownStandard.setSelectedIndex(1);
            andOrDropdownShared.setSelectedIndex(1);
        }

        if (StringUtils.isEmpty(storyElement.getReferenceId())) {
            standardSet = true;
            setTypeCards.show(cardHolderPanel, "standard");

            referenceConditionsField.loadList(storyElement.getReferenceConditions());

            List<String> options = new ArrayList<String>();
            for (ConditionSet cset : environment.getSharedConditions()) {
                options.add(cset.getReferenceId());
            }
            referenceConditionsField.loadOptions(options);
        } else {
            standardSet = false;
            setTypeCards.show(cardHolderPanel, "shared");

            this.conditionSetIdField.setText(storyElement.getReferenceId());
        }

    }

}
