/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.label.WebLabel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.story.Passage;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextArea;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;

public class StoryPassageForm extends AbstractForm {

    private static final long serialVersionUID = 583227605450908615L;

    private Passage passage;

    private WebTextField passageIdField;
    private WebTextArea passageTextField;

    public StoryPassageForm(String title) {
        super(title);
    }


    @Override
    public void buildForm() {
        super.buildForm();

        passageIdField = new TattletaleWebTextField();

        passageTextField = new TattletaleWebTextArea(15, 10);
        passageTextField.setWrapStyleWord(true);
        passageTextField.setLineWrap(true);
        WebScrollPane passageTextScroll = new WebScrollPane(passageTextField);

        form.add(new WebLabel("Passage ID:"), "");
        form.add(passageIdField, "wrap");

        form.add(new WebLabel("Passage Text:"), "wrap");
        form.add(passageTextScroll, "span 2, growx, wrap 15");
    }

    @Override
    public void clearForm() {
        super.clearForm();

        passageIdField.clear();
        passageTextField.clear();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        passage.setPassageId(passageIdField.getText());
        passage.setPassageText(passageTextField.getText());
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        load(passage);
    }

    public void load(Passage storyElement) {
        this.passage = storyElement;

        this.clearForm();

        passageIdField.setText(storyElement.getPassageId());
        passageTextField.setText(storyElement.getPassageText());
    }

}
