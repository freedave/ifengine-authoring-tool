/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.extended.button.WebSwitch;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.forms.components.OverlayLabel;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebSwitch;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextArea;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;

public class DialogForm extends AbstractForm {

    private static final long serialVersionUID = 5753205642222579138L;

    private Dialog loadedDialog;
    private Environment environment;

    WebTextField referenceIdField;
    WebTextField optionDisplayField;
    WebComboBox topicField;
    WebTextArea responseField;

    WebSwitch onceField;
    WebSwitch disabledField;

    public DialogForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        referenceIdField = new TattletaleWebTextField();
        optionDisplayField = new TattletaleWebTextField();
        topicField = new TattletaleWebComboBox();
        responseField = new TattletaleWebTextArea(5, 10);
        onceField = new TattletaleWebSwitch();
        disabledField = new TattletaleWebSwitch();

        responseField.setWrapStyleWord(true);
        responseField.setLineWrap(true);
        WebScrollPane messageScroll = new WebScrollPane(responseField);

        form.add(new WebLabel("Reference ID:"), "");
        form.add(referenceIdField, "growx, wrap");

        form.add(new WebLabel("Option Display:"), "");
        form.add(optionDisplayField, "wrap");

        form.add(new WebLabel("Topic:"), "");
        form.add(topicField, "wrap 15");

        form.add(new WebLabel("Response:"), "wrap");
        form.add(messageScroll, "span 2, growx, wrap");

        form.add(new OverlayLabel("One Time: ", "<html>select if this dialog is only to be repeated once.</html>"), "");
        form.add(onceField, "w 75!, wrap");
        onceField.setSelected(false);

        form.add(new OverlayLabel("Disabled: ", "<html>select if this dialog should begin disabled</html>"), "");
        form.add(disabledField, "w 75!, wrap");
        disabledField.setSelected(false);
    }

    @SuppressWarnings("unchecked")
    public void load(Dialog dialog, Environment environment) {
        clearForm();

        this.loadedDialog = dialog;
        this.environment = environment;

        referenceIdField.setText(dialog.getReferenceId());
        optionDisplayField.setText(dialog.getOptionDisplay());
        responseField.setText(dialog.getResponse());
        onceField.setSelected(dialog.isOnce());
        disabledField.setSelected(dialog.isDisabled());

        String loadedTopic = dialog.getTopic();
        for (String topic : environment.getConfiguration().getDialogConfiguration().getTopics()) {
            topicField.addItem(topic);
            if (loadedTopic != null && loadedTopic.equalsIgnoreCase(topic)) {
                topicField.setSelectedItem(topic);
            }
        }
        if (loadedTopic == null) {
            topicField.setSelectedIndex(-1);
        }
    }

    @Override
    public void clearForm() {
        super.clearForm();

        onceField.setSelected(false);
        disabledField.setSelected(false);

        referenceIdField.clear();
        optionDisplayField.clear();
        responseField.clear();

        topicField.removeAllItems();
        topicField.setSelectedIndex(-1);
    }

    @Override
    public void saveForm() {
        super.saveForm();

        loadedDialog.setReferenceId(referenceIdField.getText());
        loadedDialog.setOptionDisplay(optionDisplayField.getText());
        loadedDialog.setTopic((String) topicField.getSelectedItem());
        loadedDialog.setResponse(responseField.getText());

        loadedDialog.setDisabled(disabledField.isSelected());
        loadedDialog.setOnce(onceField.isSelected());
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        this.load(loadedDialog, environment);
    }

}
