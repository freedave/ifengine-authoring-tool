/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.BooleanDisplay;
import com.dodecahelix.ifengine.editor.forms.components.OverlayLabel;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;

public class BooleanDisplayForm extends AbstractForm {

    private static final long serialVersionUID = -5218420492773009023L;

    private BooleanDisplay loadedDisplay;

    private WebTextField trueDisplayField;
    private WebTextField falseDisplayField;

    public BooleanDisplayForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        trueDisplayField = new TattletaleWebTextField();
        form.add(new OverlayLabel("True Display: ", "<html>Text to display when this value is true</html>"), "");
        form.add(trueDisplayField, "wrap 15");

        falseDisplayField = new TattletaleWebTextField();
        form.add(new OverlayLabel("False Display: ", "<html>Text to display when this value is false</html>"), "");
        form.add(falseDisplayField, "wrap 15");
    }

    public void load(BooleanDisplay display) {
        this.loadedDisplay = display;

        this.clearForm();

        this.trueDisplayField.setText(display.getTrueDisplay());
        this.falseDisplayField.setText(display.getFalseDisplay());
    }


    @Override
    public void clearForm() {
        super.clearForm();

        this.trueDisplayField.clear();
        this.falseDisplayField.clear();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        loadedDisplay.setTrueDisplay(trueDisplayField.getText());
        loadedDisplay.setFalseDisplay(falseDisplayField.getText());
    }


}
