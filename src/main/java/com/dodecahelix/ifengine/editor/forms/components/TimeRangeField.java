/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JLabel;

import com.alee.laf.panel.WebPanel;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.TimeRange;
import com.dodecahelix.ifengine.editor.forms.layout.LayoutHelper;

public class TimeRangeField extends WebPanel {

    private static final long serialVersionUID = -861479403434225355L;

    private TimeRange loadedRange;
    private Environment loadedEnvironment;

    private List<String> periodsOfDay;
    private List<String> daysOfWeek;
    private List<String> monthsOfYear;

    private ComboboxAppendableListField periodsOfDayField;
    private ComboboxAppendableListField daysOfWeekField;
    private ComboboxAppendableListField monthsOfYearField;

    public TimeRangeField() {
        super(LayoutHelper.getPadlessFormLayout());
        this.setBackground(LayoutHelper.BG_COLOR);

        periodsOfDayField = new ComboboxAppendableListField();
        daysOfWeekField = new ComboboxAppendableListField();
        monthsOfYearField = new ComboboxAppendableListField();

        this.add(new JLabel(""), "span 2, growx, wrap, gapbottom 15");

        this.add(new OverlayLabel("Periods of the Day: ", "<html>Which periods of the day<br>that this schedule is active<br>leave empty for all periods</html>"), "wrap");
        this.add(periodsOfDayField, "span 2, growx, wrap");

        this.add(new JLabel(""), "span 2, growx, wrap, gapbottom 15");

        this.add(new OverlayLabel("Days of the Week: ", "<html>Which days of the week<br>that this schedule is active<br>leave empty for all days</html>"), "wrap");
        this.add(daysOfWeekField, "span 2, growx, wrap");

        this.add(new JLabel(""), "span 2, growx, wrap, gapbottom 15");

        this.add(new OverlayLabel("Months of the Year: ", "<html>Which months of the year<br>that this schedule is active<br>leave empty for all months</html>"), "wrap");
        this.add(monthsOfYearField, "span 2, growx, wrap");
    }

    public void clear() {
        periodsOfDayField.clear();
        monthsOfYearField.clear();
        daysOfWeekField.clear();
    }

    public void load(TimeRange timeRange, Environment environment) {
        this.loadedRange = timeRange;
        this.loadedEnvironment = environment;

        periodsOfDay = environment.getConfiguration().getTimeConfiguration().getPeriodsOfDay();
        daysOfWeek = environment.getConfiguration().getTimeConfiguration().getDaysOfWeek();
        monthsOfYear = environment.getConfiguration().getTimeConfiguration().getMonthsOfYear();

        periodsOfDayField.loadOptions(periodsOfDay);
        daysOfWeekField.loadOptions(daysOfWeek);
        monthsOfYearField.loadOptions(monthsOfYear);

        List<String> podList = new ArrayList<String>();
        for (Integer periodNum : timeRange.getPeriodsOfDay()) {
            podList.add(periodsOfDay.get(periodNum - 1));
        }
        periodsOfDayField.loadList(podList);

        List<String> dowList = new ArrayList<String>();
        for (Integer dayNum : timeRange.getDaysOfWeek()) {
            dowList.add(daysOfWeek.get(dayNum - 1));
        }
        daysOfWeekField.loadList(dowList);

        List<String> moyList = new ArrayList<String>();
        for (Integer monthNum : timeRange.getMonthsOfYear()) {
            moyList.add(monthsOfYear.get(monthNum - 1));
        }
        monthsOfYearField.loadList(moyList);
    }

    public void save() {
        Set<Integer> podNums = new HashSet<Integer>();
        for (String periodName : periodsOfDayField.getValues()) {
            podNums.add(periodsOfDay.indexOf(periodName) + 1);
        }
        loadedRange.setPeriodsOfDay(podNums);

        Set<Integer> dowNums = new HashSet<Integer>();
        for (String dayName : daysOfWeekField.getValues()) {
            dowNums.add(daysOfWeek.indexOf(dayName) + 1);
        }
        loadedRange.setDaysOfWeek(dowNums);

        Set<Integer> moyNums = new HashSet<Integer>();
        for (String monthName : monthsOfYearField.getValues()) {
            moyNums.add(monthsOfYear.indexOf(monthName) + 1);
        }
        loadedRange.setMonthsOfYear(moyNums);
    }

    public void reset() {
        this.load(loadedRange, loadedEnvironment);
    }


}
