/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.layout;

import net.miginfocom.swing.MigLayout;

import com.alee.laf.panel.WebPanel;

public class FormWebPanel extends WebPanel {

    private static final long serialVersionUID = -7823751308503688291L;

    public FormWebPanel() {
        this(0);
    }

    public FormWebPanel(int margin) {
        this(LayoutHelper.getDefaultFormLayout(), margin);
    }

    public FormWebPanel(MigLayout layout, int margin) {
        super(layout);
        this.setBackground(LayoutHelper.BG_COLOR);
        this.setMargin(margin);
    }

}
