/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Person;

public class PersonForm extends AbstractEntityForm<Person> {

    private static final long serialVersionUID = 4811358447325731472L;

    public PersonForm(String titleText) {
        super(titleText, false);
    }

    @Override
    public void load(Person person, Environment environment) {
        super.load(person, environment);
    }

    @Override
    public void saveForm() {
        super.saveForm();
    }

    @Override
    protected void resetForm() {
        super.resetForm();
    }


}
