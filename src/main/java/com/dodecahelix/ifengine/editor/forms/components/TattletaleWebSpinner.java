/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import javax.swing.SpinnerModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.alee.laf.spinner.WebSpinner;
import com.dodecahelix.ifengine.editor.events.FormDirtyStatusEvent;
import com.dodecahelix.ifengine.main.Events;

public class TattletaleWebSpinner extends WebSpinner {

    private static final long serialVersionUID = 7421026983844481745L;

    public TattletaleWebSpinner() {
        super();

        addChangeListener();
    }

    public TattletaleWebSpinner(SpinnerModel model) {
        super(model);

        addChangeListener();
    }

    private void addChangeListener() {

        this.getModel().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent arg0) {
                Events.getInstance().getBus().post(new FormDirtyStatusEvent(true));
            }
        });

    }


}
