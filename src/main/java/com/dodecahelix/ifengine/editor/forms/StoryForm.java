/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.story.ContentRating;
import com.dodecahelix.ifengine.data.story.Genre;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.data.story.StorySize;
import com.dodecahelix.ifengine.editor.forms.components.OverlayLabel;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextArea;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;

public class StoryForm extends AbstractForm {

    private static final long serialVersionUID = -798683541818216385L;

    private LibraryCard loadedStory;

    WebTextField storyNameTextField;
    WebTextField authorNameTextField;
    WebTextField versionTextField;
    WebTextArea synopsisField;
    WebTextArea acknowledgmentField;
    WebComboBox genreDropdown;
    WebComboBox contentRatingDropdown;
    WebComboBox storySizeDropdown;

    public StoryForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        storyNameTextField = new TattletaleWebTextField();
        form.add(new WebLabel("Story Title:"), "");
        form.add(storyNameTextField, "wrap");

        authorNameTextField = new TattletaleWebTextField();
        form.add(new WebLabel("Author Name:"), "");
        form.add(authorNameTextField, "wrap");

        versionTextField = new TattletaleWebTextField();
        form.add(new OverlayLabel("Current Version: ", "<html>Versioning should take the form <br>major.minor (i.e; 1.2) using numbers only.<br>Stories in test status should have<br>a major version of 0. (i.e; 0.2)</html>"), "");
        form.add(versionTextField, "wrap, gapbottom 10");

        this.addFormSeparator();

        synopsisField = new TattletaleWebTextArea(5, 10);
        synopsisField.setWrapStyleWord(true);
        synopsisField.setLineWrap(true);
        WebScrollPane synopsisScroll = new WebScrollPane(synopsisField);
        form.add(new OverlayLabel("Synopsis: ", "<html>Please keep your synopsis short and sweet,<br>under 200 characters in length.</html>"), "wrap");
        form.add(synopsisScroll, "span 2, growx, wrap");

        this.addFormSeparator();

        genreDropdown = new TattletaleWebComboBox(Genre.values());
        contentRatingDropdown = new TattletaleWebComboBox(ContentRating.values());
        storySizeDropdown = new TattletaleWebComboBox(StorySize.values());

        form.add(new WebLabel("Genre:"), "");
        form.add(genreDropdown, "wrap");

        form.add(new OverlayLabel("Content Rating: ", "<html>Please rate your story based on the<br>maturity of your audience.</html>"), "");
        form.add(contentRatingDropdown, "wrap");

        form.add(new OverlayLabel("Story Size: ", "<html>As a general guideline, stories with<br>under five locations are Short,<br>between 5 and 20 are Novellas,<br>between 20 and 50 are Novels,<br>and over 50 are Epics</html>"), "");
        form.add(storySizeDropdown, "wrap");

        this.addFormSeparator();

        acknowledgmentField = new TattletaleWebTextArea(5, 10);
        acknowledgmentField.setWrapStyleWord(true);
        acknowledgmentField.setLineWrap(true);
        WebScrollPane ackScroll = new WebScrollPane(acknowledgmentField);
        form.add(new OverlayLabel("Acknowlegdments: ", "<html>This content is printed at the end of your story, following any conclusion.</html>"), "wrap");
        form.add(ackScroll, "span 2, growx, wrap");

        this.addFormSeparator();
    }

    public void load(LibraryCard storyElement) {
        this.loadedStory = storyElement;

        this.clearForm();

        storyNameTextField.setText(storyElement.getTitle());
        authorNameTextField.setText(storyElement.getAuthor());
        versionTextField.setText(storyElement.getVersion());
        synopsisField.setText(storyElement.getSynopsis());
        acknowledgmentField.setText(storyElement.getAcknowledgements());

        genreDropdown.setSelectedItem(storyElement.getGenre());
        contentRatingDropdown.setSelectedItem(storyElement.getContentRating());
        storySizeDropdown.setSelectedItem(storyElement.getStorySize());
    }

    @Override
    public void saveForm() {
        super.saveForm();

        loadedStory.setTitle(storyNameTextField.getText());
        loadedStory.setAuthor(authorNameTextField.getText());
        loadedStory.setVersion(versionTextField.getText());
        loadedStory.setSynopsis(synopsisField.getText());
        loadedStory.setGenre((Genre) genreDropdown.getSelectedItem());
        loadedStory.setContentRating((ContentRating) contentRatingDropdown.getSelectedItem());
        loadedStory.setStorySize((StorySize) storySizeDropdown.getSelectedItem());
        loadedStory.setAcknowledgements(acknowledgmentField.getText());
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        this.load(loadedStory);
    }

}
