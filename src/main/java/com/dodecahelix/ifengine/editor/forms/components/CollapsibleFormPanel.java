/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import java.awt.CardLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JComponent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.panel.WebPanel;
import com.dodecahelix.ifengine.editor.forms.AbstractForm;
import com.dodecahelix.ifengine.editor.forms.layout.FormWebPanel;
import com.dodecahelix.ifengine.editor.forms.layout.LayoutHelper;

/**
 *   A checkbox which expands or collapses a content panel beneath it, with other form data
 *
 *
 */
public class CollapsibleFormPanel extends WebPanel {

    private static final long serialVersionUID = 3068930467717621721L;

    static final Logger logger = LoggerFactory.getLogger(AbstractForm.class.getName());

    private JCheckBox toggleCheckbox;

    private WebPanel contents;
    private WebPanel expandedContents;
    private WebPanel collapsedContents;
    private CardLayout contentCards;

    public CollapsibleFormPanel(String label) {
        super(LayoutHelper.getDefaultFormLayout());
        this.setBackground(LayoutHelper.BG_COLOR);

        toggleCheckbox = new JCheckBox(label);
        this.add(toggleCheckbox, "span 2, wrap, left");

        contentCards = new CardLayout();
        contents = new WebPanel(contentCards);
        expandedContents = new FormWebPanel();
        collapsedContents = new FormWebPanel();

        contents.add(expandedContents, "expanded");
        contents.add(collapsedContents, "collapsed");

        this.add(contents, "span 2, wrap");

        toggleCheckbox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent event) {
                expandContents(toggleCheckbox.isSelected());
            }
        });

        expandContents(false);
    }

    public void addContentItem(JComponent component) {
        expandedContents.add(component, "span 2, wrap");
    }

    /**
     *   Expand or collapse the contents controlled by this checkbox
     * @param expand
     */
    public void expandContents(boolean expand) {
        logger.debug("expanding contents of collapsible form : " + expand);

        if (expand) {
            contentCards.show(contents, "expanded");
        } else {
            contentCards.show(contents, "collapsed");
        }
    }

}
