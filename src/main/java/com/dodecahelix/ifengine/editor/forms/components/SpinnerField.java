/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import javax.swing.SpinnerNumberModel;

import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.spinner.WebSpinner;
import com.dodecahelix.ifengine.editor.forms.layout.LayoutHelper;

public class SpinnerField extends WebPanel {

    private static final long serialVersionUID = -5289458458569080932L;

    private WebSpinner webSpinner;

    public SpinnerField(String label, String tooltip, int digits) {
        super(LayoutHelper.getPadlessFormLayout());

        SpinnerNumberModel numberModel = new SpinnerNumberModel();
        numberModel.setMaximum(new Integer((int) (Math.pow(10, digits) - 1)));
        webSpinner = new TattletaleWebSpinner(numberModel);

        this.setBackground(LayoutHelper.BG_COLOR);

        if (tooltip == null) {
            //this.add(new WebLabel(label),"w 90!");
            this.add(new WebLabel(label), "");
        } else {
            //this.add(new OverlayLabel(label, tooltip), "w 90!");
            this.add(new OverlayLabel(label, tooltip), "");
        }

        this.add(webSpinner, "left");
    }

    public int getValue() {
        return Integer.parseInt(webSpinner.getValue().toString());
    }

    public void setValue(int value) {
        webSpinner.setValue(value);
    }
}