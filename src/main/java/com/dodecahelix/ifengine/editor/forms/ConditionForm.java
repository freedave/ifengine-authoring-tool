/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.separator.WebSeparator;
import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.entity.EntityHolderDefaultValuesTool;

public class ConditionForm extends AbstractForm {

    private static final long serialVersionUID = -3903806154250284768L;

    private Condition loadedCondition;
    private Environment environment;

    WebComboBox conditionTypeDropdown;
    WebComboBox conditionOperatorDropdown;

    TargetFormPanel targetFormPanel;
    SubjectFormPanel subjectFormPanel;

    WebLabel conditionTypeExplanation;

    public ConditionForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        WebSeparator formSeparator = new WebSeparator();

        conditionTypeDropdown = new TattletaleWebComboBox(ConditionType.values());
        conditionTypeExplanation = new WebLabel("");
        conditionOperatorDropdown = new TattletaleWebComboBox(ConditionOperator.values());

        form.add(formSeparator, "span 2, wrap");

        form.add(new WebLabel("Type:"), "");
        form.add(conditionTypeDropdown, "wrap");
        form.add(conditionTypeExplanation, "w 325!, span 2, wrap");

        form.add(formSeparator, "span 2, wrap");

        subjectFormPanel = new SubjectFormPanel();
        subjectFormPanel.buildForm(form);

        form.add(formSeparator, "span 2, wrap");

        form.add(new WebLabel("Operator:"), "");
        form.add(conditionOperatorDropdown, "wrap");

        form.add(formSeparator, "span 2, wrap");

        targetFormPanel = new TargetFormPanel();
        targetFormPanel.buildForm(form);

        conditionTypeDropdown.addItemListener(new ItemListener() {

            EntityHolderDefaultValuesTool defaultValuesTool = new EntityHolderDefaultValuesTool(subjectFormPanel, targetFormPanel);

            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                Object selectedItem = itemEvent.getItem();
                if (selectedItem instanceof ConditionType) {
                    conditionTypeExplanation.setText("<html>" + ((ConditionType) selectedItem).getExplanation() + "</html>");
                    defaultValuesTool.populateByConditionType((ConditionType) selectedItem);
                }
            }
        });
    }

    @Override
    public void clearForm() {
        this.conditionOperatorDropdown.setSelectedIndex(-1);

        subjectFormPanel.clear();
        targetFormPanel.clear();
    }

    @Override
    public void resetForm() {
        super.resetForm();

        // reload the condition and environment
        if (loadedCondition != null) {
            this.load(loadedCondition, environment);
        } else {
            this.clearForm();
        }
    }

    public void load(Condition condition, Environment environment) {
        this.clearForm();

        this.loadedCondition = condition;
        this.environment = environment;
        subjectFormPanel.setEnvironment(environment);
        targetFormPanel.setEnvironment(environment);

        // this will prepopulate the fields, but will be overridden by load methods
        conditionTypeDropdown.setSelectedItem(condition.getType());
        conditionTypeExplanation.setText("<html>" + condition.getType().getExplanation() + "</html>");

        if (condition.getOperator() != null) {
            conditionOperatorDropdown.setSelectedItem(condition.getOperator());
        }

        subjectFormPanel.load(condition.getEntityHolder(), environment);
        targetFormPanel.load(condition.getTargetType(),
        condition.getEntityHolder(),
        condition.getTargetStaticValue(),
        environment);
    }

    @Override
    public void saveForm() {
        super.saveForm();

        EntityHolder holder = new EntityHolder();
        subjectFormPanel.populateEntityHolder(holder);
        targetFormPanel.populateEntityHolder(holder);
        loadedCondition.setEntityHolder(holder);

        loadedCondition.setOperator((ConditionOperator) conditionOperatorDropdown.getSelectedItem());
        loadedCondition.setType((ConditionType) conditionTypeDropdown.getSelectedItem());
        loadedCondition.setTargetStaticValue(targetFormPanel.getTargetStaticValue());
        loadedCondition.setTargetType(targetFormPanel.getTargetType());
    }

}
