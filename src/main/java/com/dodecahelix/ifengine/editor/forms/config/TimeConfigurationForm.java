/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.config;

import com.alee.laf.label.WebLabel;
import com.dodecahelix.ifengine.data.configuration.TimeConfiguration;
import com.dodecahelix.ifengine.editor.forms.AbstractForm;
import com.dodecahelix.ifengine.editor.forms.components.AppendableListField;
import com.dodecahelix.ifengine.editor.forms.components.SpinnerField;

public class TimeConfigurationForm extends AbstractForm {

    private static final long serialVersionUID = 9154214911893210291L;

    private TimeConfiguration timeConfig;

    private AppendableListField periodsOfDayField;
    private AppendableListField daysOfWeekField;
    private AppendableListField monthsOfYearField;

    SpinnerField incrementPerPeriodField;
    SpinnerField weeksPerMonthField;


    public TimeConfigurationForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        periodsOfDayField = new AppendableListField();
        daysOfWeekField = new AppendableListField();
        monthsOfYearField = new AppendableListField();

        incrementPerPeriodField = new SpinnerField("Increments per period: ", null, 1);
        weeksPerMonthField = new SpinnerField("Weeks in a month: ", null, 5);

        form.add(incrementPerPeriodField, "span 2, gapbottom10, wrap");

        form.add(new WebLabel("Periods of the day:"), "wrap");
        form.add(periodsOfDayField, "span 2, growx, wrap");

        this.addFormSeparator();

        form.add(new WebLabel("Days of the week:"), "wrap");
        form.add(daysOfWeekField, "span 2, growx, wrap");

        this.addFormSeparator();

        form.add(weeksPerMonthField, "span 2, wrap");

        this.addFormSeparator();

        form.add(new WebLabel("Months of the year:"), "wrap");
        form.add(monthsOfYearField, "span 2, growx, wrap");
    }

    public void load(TimeConfiguration timeConfig) {
        this.timeConfig = timeConfig;
        this.clearForm();

        periodsOfDayField.loadList(timeConfig.getPeriodsOfDay());
        daysOfWeekField.loadList(timeConfig.getDaysOfWeek());
        monthsOfYearField.loadList(timeConfig.getMonthsOfYear());

        incrementPerPeriodField.setValue(timeConfig.getIncrementsPerPeriod());
        weeksPerMonthField.setValue(timeConfig.getWeeksPerMonth());
    }

    @Override
    public void clearForm() {
        super.clearForm();

        periodsOfDayField.clear();
        daysOfWeekField.clear();
        monthsOfYearField.clear();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        timeConfig.setPeriodsOfDay(periodsOfDayField.getValues());
        timeConfig.setDaysOfWeek(daysOfWeekField.getValues());
        timeConfig.setMonthsOfYear(monthsOfYearField.getValues());

        timeConfig.setIncrementsPerPeriod(incrementPerPeriodField.getValue());
        timeConfig.setWeeksPerMonth(weeksPerMonthField.getValue());
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        load(timeConfig);
    }

}
