/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import com.alee.extended.image.WebImage;
import com.alee.extended.panel.GroupPanel;
import com.alee.extended.panel.WebOverlay;
import com.alee.laf.label.WebLabel;
import com.alee.managers.language.data.TooltipWay;
import com.alee.managers.tooltip.TooltipManager;
import com.dodecahelix.ifengine.editor.forms.layout.LayoutHelper;
import com.dodecahelix.ifengine.editor.utils.ModelIconBuilder;

public class OverlayLabel extends GroupPanel {

    private static final long serialVersionUID = 7912019928603070712L;

    public OverlayLabel(String labelText, String tooltip) {
        super(buildOverlayPanel(labelText, tooltip));
    }

    private static WebOverlay buildOverlayPanel(String labelText, String tooltip) {
        // Overlay
        WebOverlay overlayPanel = new WebOverlay();
        overlayPanel.setBackground(LayoutHelper.BG_COLOR);

        // Overlayed label
        WebLabel component = new WebLabel(labelText);
        overlayPanel.setComponent(component);

        // Image displayed as overlay
        URL imgUrl = ModelIconBuilder.class.getClassLoader().getResource("icons/information-small.png");
        ImageIcon defaultIcon = new ImageIcon(imgUrl);

        WebImage overlay = new WebImage(defaultIcon);
        TooltipManager.setTooltip(overlay, tooltip, TooltipWay.up, 0);
        overlayPanel.addOverlay(overlay, SwingConstants.TRAILING, SwingConstants.TOP);
        overlayPanel.setComponentMargin(0, 0, 0, overlay.getPreferredSize().width);

        return overlayPanel;
    }


}
