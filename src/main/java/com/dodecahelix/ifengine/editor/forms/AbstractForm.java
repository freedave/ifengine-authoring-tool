/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.ScrollPaneConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;

import com.dodecahelix.ifengine.editor.events.ErrorEvent;
import com.dodecahelix.ifengine.editor.events.FormDirtyStatusEvent;
import com.dodecahelix.ifengine.editor.events.SaveStoryEvent;
import com.dodecahelix.ifengine.editor.events.TreeRefreshEvent;
import com.dodecahelix.ifengine.editor.forms.layout.FormWebPanel;
import com.dodecahelix.ifengine.editor.forms.layout.LayoutHelper;
import com.dodecahelix.ifengine.main.Events;

public class AbstractForm extends FormWebPanel {

    private static final long serialVersionUID = -5237360246438159905L;

    static final Logger logger = LoggerFactory.getLogger(AbstractForm.class.getName());

    private WebLabel formTitle = new WebLabel();
    private WebButton saveStoryButton = new WebButton("Save");
    private WebButton resetButton = new WebButton("Reset");

    protected WebPanel form;

    public AbstractForm(String title) {
        super(LayoutHelper.FORM_MARGIN);

        formTitle.setText(title);
        formTitle.setBoldFont();

        // wrap the form in a scrollpane
        form = new FormWebPanel();
        buildForm();

        WebScrollPane formScrollpane = new WebScrollPane(form);
        formScrollpane.setBorder(null);
        formScrollpane.setBackground(LayoutHelper.BG_COLOR);
        formScrollpane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        this.add(formScrollpane, "span 2, wrap, grow");

        WebPanel buttonPanel = new FormWebPanel();
        buttonPanel.add(saveStoryButton);
        buttonPanel.add(resetButton);
        this.add(buttonPanel, "wrap");

        addButtonListeners();
    }

    private void addButtonListeners() {
        saveStoryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                if (AbstractForm.this.validateFormData()) {
                    AbstractForm.this.saveForm();

                    // refresh the tree
                    Events.getInstance().getBus().post(new TreeRefreshEvent());

                    // form is no longer dirty
                    Events.getInstance().getBus().post(new FormDirtyStatusEvent(false));

                    Events.getInstance().getBus().post(new SaveStoryEvent());
                }
            }
        });

        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                AbstractForm.this.resetForm();

                // form is no longer dirty
                Events.getInstance().getBus().post(new FormDirtyStatusEvent(false));
            }
        });
    }

    protected void addFormSeparator() {
        form.add(new JLabel(""), "span 2, growx, wrap, gapbottom 10");
    }

    public void buildForm() {
        this.add(formTitle, "span 2, wrap");
        this.add(new JSeparator(), "span 2, growx, wrap");
    }

    public void clearForm() {
    }

    public void saveForm() {
        logger.debug("Saving..");
    }

    protected void resetForm() {
        logger.debug("Resetting form..");
    }

    /**
     *  validates the form, sends out a dialog if invalid
     *
     * @return true if the form is valid
     */
    private final boolean validateFormData() {
        List<String> invalidData = validateFormEntry();
        if (invalidData == null || invalidData.isEmpty()) {
            return true;
        } else {
            StringBuffer errorMessages = new StringBuffer("<html><b>This form is invalid</b>: ");
            for (String errorMessage : invalidData) {
                errorMessages.append("<p> - ");
                errorMessages.append(errorMessage);
                errorMessages.append("</p>");
            }
            errorMessages.append("</html>");
            Events.getInstance().getBus().post(new ErrorEvent("Invalid Form Entry", errorMessages.toString()));
            return false;
        }
    }

    protected List<String> validateFormEntry() {
        return new ArrayList<String>();
    }

}
