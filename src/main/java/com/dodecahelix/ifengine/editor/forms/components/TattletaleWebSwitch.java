/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import java.awt.event.MouseEvent;

import javax.swing.event.MouseInputAdapter;

import com.alee.extended.button.WebSwitch;
import com.dodecahelix.ifengine.editor.events.FormDirtyStatusEvent;
import com.dodecahelix.ifengine.main.Events;

public class TattletaleWebSwitch extends WebSwitch {

    private static final long serialVersionUID = -5329732011046998696L;

    public TattletaleWebSwitch() {
        this(false);
    }

    public TattletaleWebSwitch(boolean selected) {
        super(selected);

        addChangeListener();
    }

    private void addChangeListener() {

        this.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                Events.getInstance().getBus().post(new FormDirtyStatusEvent(true));
                super.mouseClicked(event);
            }
        });

    }

}
