/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.entity.EntityComboBoxHelper;
import com.dodecahelix.ifengine.editor.forms.entity.EntityIdComboboxItemRenderer;
import com.dodecahelix.ifengine.editor.forms.entity.EntityIdDropdownListener;
import com.dodecahelix.ifengine.editor.forms.entity.EntityTypeDropdownListener;
import com.dodecahelix.ifengine.editor.forms.layout.FormWebPanel;
import com.dodecahelix.ifengine.util.StringUtils;

public class SubjectFormPanel {

    private Environment loadedEnvironment;

    WebPanel subjectEntityPanel;

    WebComboBox subjectEntityTypeDropdown;
    WebComboBox subjectEntityIdDropdown;
    WebComboBox subjectEntityPropertyIdDropdown;

    private EntityTypeDropdownListener entityTypeDropdownListener;
    private EntityIdDropdownListener entityIdDropdownListener;

    @SuppressWarnings("unchecked")
    public void buildForm(WebPanel form) {

        subjectEntityTypeDropdown = new TattletaleWebComboBox(EntityType.values());

        subjectEntityIdDropdown = new TattletaleWebComboBox();
        subjectEntityIdDropdown.setRenderer(new EntityIdComboboxItemRenderer());
        subjectEntityPropertyIdDropdown = new TattletaleWebComboBox();

        // need to make this editable, since in the case of "this" the properties of all entity types are not known
        subjectEntityPropertyIdDropdown.setEditable(true);

        subjectEntityPanel = new FormWebPanel();

        subjectEntityPanel.add(new WebLabel("Type:"), "");
        subjectEntityPanel.add(subjectEntityTypeDropdown, "wrap");

        subjectEntityPanel.add(new WebLabel("Entity:"), "");
        subjectEntityPanel.add(subjectEntityIdDropdown, "wrap");

        subjectEntityPanel.add(new WebLabel("Property:"), "");
        subjectEntityPanel.add(subjectEntityPropertyIdDropdown, "wrap");

        form.add(new WebLabel("Subject"), "");
        form.add(subjectEntityPanel, "wrap");

        // add listeners to detect entity type or ID changes
        entityIdDropdownListener = new EntityIdDropdownListener(subjectEntityTypeDropdown, subjectEntityPropertyIdDropdown);
        entityTypeDropdownListener = new EntityTypeDropdownListener(subjectEntityIdDropdown, subjectEntityPropertyIdDropdown, true, false);
        subjectEntityTypeDropdown.addItemListener(entityTypeDropdownListener);
        subjectEntityIdDropdown.addItemListener(entityIdDropdownListener);
    }

    public void clear() {
        subjectEntityTypeDropdown.setSelectedIndex(-1);
        this.subjectEntityIdDropdown.removeAllItems();
        this.subjectEntityPropertyIdDropdown.removeAllItems();
    }

    public void load(EntityHolder entityHolder, Environment environment) {
        this.clear();
        setEnvironment(environment);
        load(entityHolder);
    }

    @SuppressWarnings("unchecked")
    public void load(EntityHolder entityHolder) {

        EntityType subjectEntityType = entityHolder.getSubjectType();
        subjectEntityTypeDropdown.setSelectedItem(subjectEntityType);

        EntityComboBoxHelper.updateEntityIdCombobox(subjectEntityIdDropdown,
        subjectEntityPropertyIdDropdown,
        subjectEntityType,
        entityHolder.getSubjectEntityId(),
        entityHolder.getSubjectProperty(),
        loadedEnvironment,
        true, false);
    }

    public void setEnvironment(Environment environment) {
        this.loadedEnvironment = environment;
        entityIdDropdownListener.setEnvironment(environment);
        entityTypeDropdownListener.setEnvironment(environment);
    }

    public EntityHolder populateEntityHolder(EntityHolder holder) {
        EntityType entityType = (EntityType) subjectEntityTypeDropdown.getSelectedItem();
        String entityId = (String) subjectEntityIdDropdown.getSelectedItem();
        String propId = (String) subjectEntityPropertyIdDropdown.getSelectedItem();

        holder.setSubjectType(entityType);

        if (!StringUtils.isEmpty(entityId)) {
            holder.setSubjectEntityId(entityId);
        }
        if (!StringUtils.isEmpty(propId)) {
            holder.setSubjectProperty(propId);
        }

        return holder;
    }

}
