/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.Time;
import com.dodecahelix.ifengine.data.configuration.TimeConfiguration;
import com.dodecahelix.ifengine.editor.forms.components.SpinnerField;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.entity.EntityIdComboboxItemRenderer;
import com.dodecahelix.ifengine.util.StringUtils;

public class EnvironmentForm extends AbstractForm {

    private static final long serialVersionUID = -3251943918239014854L;

    private Environment loadedEnvironment;
    private TimeConfiguration timeConfig;

    private WebComboBox startingNarratorDropdown;

    SpinnerField startYearField;
    SpinnerField startDayOfMonthField;

    WebComboBox startMonthDropdown;
    WebComboBox startPeriodDropdown;

    public EnvironmentForm(String text) {
        super(text);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void buildForm() {
        super.buildForm();

        startingNarratorDropdown = new TattletaleWebComboBox();
        startingNarratorDropdown.setRenderer(new EntityIdComboboxItemRenderer());
        form.add(new WebLabel("Narrator (starting):"), "");
        form.add(startingNarratorDropdown, "wrap 25");

        this.addFormSeparator();

        startYearField = new SpinnerField("Starting Year: ", null, 5);
        startDayOfMonthField = new SpinnerField("Starting Day of Month: ", null, 2);
        startPeriodDropdown = new TattletaleWebComboBox();
        startMonthDropdown = new TattletaleWebComboBox();

        form.add(new WebLabel("Start Period: "), "");
        form.add(startPeriodDropdown, "wrap");
        form.add(new WebLabel("Start Month: "), "");
        form.add(startMonthDropdown, "wrap");
        form.add(startDayOfMonthField, "span 2, wrap");
        form.add(startYearField, "span 2, wrap");
    }

    @SuppressWarnings("unchecked")
    public void load(Environment environment) {
        this.loadedEnvironment = environment;

        startingNarratorDropdown.removeAllItems();

        for (Reader narrator : environment.getNarrators()) {
            startingNarratorDropdown.addItem(narrator);
            if (StringUtils.equalsIgnoreCase(environment.getCurrentNarrator(), narrator.getId())) {
                startingNarratorDropdown.setSelectedItem(narrator);
            }
        }

        timeConfig = environment.getConfiguration().getTimeConfiguration();
        startYearField.setValue(timeConfig.getStartingYear());
        startDayOfMonthField.setValue(timeConfig.getStartDayOfMonth());

        updateStartPeriodDropdown();
        startPeriodDropdown.setSelectedIndex(timeConfig.getStartPeriod() - 1);

        updateStartMonthDropdown();
        startMonthDropdown.setSelectedIndex(timeConfig.getStartMonth() - 1);
    }

    @SuppressWarnings("unchecked")
    private void updateStartPeriodDropdown() {
        startPeriodDropdown.removeAllItems();
        for (String periodName : timeConfig.getPeriodsOfDay()) {
            startPeriodDropdown.addItem(periodName);
        }
        startPeriodDropdown.setSelectedIndex(0);
    }

    @SuppressWarnings("unchecked")
    private void updateStartMonthDropdown() {
        startMonthDropdown.removeAllItems();
        for (String monthName : timeConfig.getMonthsOfYear()) {
            startMonthDropdown.addItem(monthName);
        }
        startMonthDropdown.setSelectedIndex(0);
    }

    @Override
    public void clearForm() {
        super.clearForm();

        startingNarratorDropdown.removeAllItems();

        startMonthDropdown.removeAllItems();
        startPeriodDropdown.removeAllItems();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        Reader narrator = (Reader) startingNarratorDropdown.getSelectedItem();
        this.loadedEnvironment.setCurrentNarrator(narrator.getId());

        timeConfig.setStartingYear(startYearField.getValue());
        timeConfig.setStartDayOfMonth(startDayOfMonthField.getValue());
        timeConfig.setStartPeriod(startPeriodDropdown.getSelectedIndex() + 1);
        timeConfig.setStartMonth(startMonthDropdown.getSelectedIndex() + 1);

        // use this configuration to set the current time
        this.loadedEnvironment.setCurrentTime(new Time(timeConfig));
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        this.load(loadedEnvironment);
    }


}
