/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.entity;

import javax.swing.JComboBox;

import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.util.StringUtils;

public class EntityComboBoxHelper {

    /**
     *   Updates the entity id dropdown (either subject or target) and the child property id dropdown
     *
     * @param entityIdDropdown
     * @param entityPropertyIdDropdown
     * @param entityType
     * @param entityId
     * @param propertyId
     * @param environment
     */
    public static void updateEntityPropertyIdCombobox(JComboBox<String> entityPropertyIdDropdown,
                                                      EntityType entityType,
                                                      String entityId,
                                                      String propertyId,
                                                      Environment environment) {

        // start with empty CB
        entityPropertyIdDropdown.removeAllItems();

        if (EntityType.ENVIRONMENT.equals(entityType)) {
            updatePropertyIdDropdown(entityPropertyIdDropdown, environment, propertyId);
        }

        if (EntityType.NARRATOR.equals(entityType)) {
            for (Reader narrator : environment.getNarrators()) {
                if (StringUtils.equalsIgnoreCase(entityId, narrator.getId())) {
                    updatePropertyIdDropdown(entityPropertyIdDropdown, narrator, propertyId);
                }
            }
        }

        if (EntityType.READER.equals(entityType)) {
            for (Reader narrator : environment.getNarrators()) {
                if (StringUtils.equalsIgnoreCase(environment.getCurrentNarrator(), narrator.getId())) {
                    updatePropertyIdDropdown(entityPropertyIdDropdown, narrator, propertyId);
                }
            }
        }

        if (EntityType.CURRENT_LOCATION.equals(entityType)) {
            for (Reader narrator : environment.getNarrators()) {
                if (StringUtils.equalsIgnoreCase(environment.getCurrentNarrator(), narrator.getId())) {
                    String currentLocation = narrator.getCurrentLocation();
                    for (Location location : environment.getLocations()) {
                        if (StringUtils.equalsIgnoreCase(currentLocation, location.getId())) {
                            updatePropertyIdDropdown(entityPropertyIdDropdown, location, propertyId);
                        }
                    }
                }
            }
        }

        if (EntityType.ITEM.equals(entityType)) {
            for (Item item : environment.getItems()) {
                if (entityId != null && entityId.equalsIgnoreCase(item.getId())) {
                    updatePropertyIdDropdown(entityPropertyIdDropdown, item, propertyId);
                }
            }
        }

        if (EntityType.LOCATION.equals(entityType)) {
            for (Location location : environment.getLocations()) {
                if (entityId != null && entityId.equalsIgnoreCase(location.getId())) {
                    updatePropertyIdDropdown(entityPropertyIdDropdown, location, propertyId);
                }
            }
        }

        if (EntityType.PERSON.equals(entityType)) {
            for (Person person : environment.getPeople()) {
                if (entityId != null && entityId.equalsIgnoreCase(person.getId())) {
                    updatePropertyIdDropdown(entityPropertyIdDropdown, person, propertyId);
                }
            }
        }

        if (EntityType.MASS.equals(entityType)) {
            for (Mass mass : environment.getMasses()) {
                if (entityId != null && entityId.equalsIgnoreCase(mass.getId())) {
                    updatePropertyIdDropdown(entityPropertyIdDropdown, mass, propertyId);
                }
            }
        }

    }


    /**
     *   Updates the entity id dropdown (either subject or target) and the child property id dropdown
     *
     * @param entityIdDropdown
     * @param entityPropertyIdDropdown
     * @param entityType
     * @param entityId
     * @param propertyId
     * @param environment
     */
    public static void updateEntityIdCombobox(
    JComboBox<String> entityIdDropdown,
    JComboBox<String> entityPropertyIdDropdown,
    EntityType entityType,
    String entityId,
    String propertyId,
    Environment environment,
    boolean includeThis,
    boolean includeLocal) {

        // start with empty CB's
        entityIdDropdown.removeAllItems();
        entityIdDropdown.addItem(" ");
        entityIdDropdown.setSelectedIndex(-1);

        entityPropertyIdDropdown.removeAllItems();

        if (EntityType.ENVIRONMENT.equals(entityType)) {
            updatePropertyIdDropdown(entityPropertyIdDropdown, environment, propertyId);
        }

        if (EntityType.NARRATOR.equals(entityType)) {
            for (Reader narrator : environment.getNarrators()) {
                entityIdDropdown.addItem(narrator.getId());
                if (StringUtils.equalsIgnoreCase(entityId, narrator.getId())) {
                    entityIdDropdown.setSelectedItem(narrator.getId());
                    updatePropertyIdDropdown(entityPropertyIdDropdown, narrator, propertyId);
                }
            }
        }

        if (EntityType.READER.equals(entityType)) {
            for (Reader narrator : environment.getNarrators()) {
                if (StringUtils.equalsIgnoreCase(environment.getCurrentNarrator(), narrator.getId())) {
                    updatePropertyIdDropdown(entityPropertyIdDropdown, narrator, propertyId);
                }
            }
        }

        if (EntityType.CURRENT_LOCATION.equals(entityType)) {
            for (Reader narrator : environment.getNarrators()) {
                if (StringUtils.equalsIgnoreCase(environment.getCurrentNarrator(), narrator.getId())) {
                    String currentLocation = narrator.getCurrentLocation();
                    for (Location location : environment.getLocations()) {
                        if (StringUtils.equalsIgnoreCase(currentLocation, location.getId())) {
                            updatePropertyIdDropdown(entityPropertyIdDropdown, location, propertyId);
                        }
                    }
                }
            }
        }

        if (EntityType.ITEM.equals(entityType)) {
            for (Item item : environment.getItems()) {
                entityIdDropdown.addItem(item.getId());
                if (entityId != null && entityId.equalsIgnoreCase(item.getId())) {
                    entityIdDropdown.setSelectedItem(item.getId());
                    updatePropertyIdDropdown(entityPropertyIdDropdown, item, propertyId);
                }
            }
        }

        if (EntityType.LOCATION.equals(entityType)) {
            for (Location location : environment.getLocations()) {
                entityIdDropdown.addItem(location.getId());
                if (entityId != null && entityId.equalsIgnoreCase(location.getId())) {
                    entityIdDropdown.setSelectedItem(location.getId());
                    updatePropertyIdDropdown(entityPropertyIdDropdown, location, propertyId);
                }
            }
        }

        if (EntityType.PERSON.equals(entityType)) {
            for (Person person : environment.getPeople()) {
                entityIdDropdown.addItem(person.getId());
                if (entityId != null && entityId.equalsIgnoreCase(person.getId())) {
                    entityIdDropdown.setSelectedItem(person.getId());
                    updatePropertyIdDropdown(entityPropertyIdDropdown, person, propertyId);
                }
            }
        }

        if (EntityType.MASS.equals(entityType)) {
            for (Mass mass : environment.getMasses()) {
                entityIdDropdown.addItem(mass.getId());
                if (entityId != null && entityId.equalsIgnoreCase(mass.getId())) {
                    entityIdDropdown.setSelectedItem(mass.getId());
                    updatePropertyIdDropdown(entityPropertyIdDropdown, mass, propertyId);
                }
            }
        }


        // include the "this" entity ID (for EntityType global conditions and executions)
        if (includeThis) {
            entityIdDropdown.addItem(EntityHolder.THIS_SUBJECT_ID);
            if (StringUtils.equalsIgnoreCase(entityId, EntityHolder.THIS_SUBJECT_ID)) {
                entityIdDropdown.setSelectedItem(entityId);

                entityPropertyIdDropdown.removeAllItems();
                // TODO - on save, make sure "this-prop" is a valid property
                if (propertyId == null) {
                    propertyId = "this-prop";
                }
                entityPropertyIdDropdown.addItem(propertyId);
                entityPropertyIdDropdown.setSelectedItem(propertyId);
            }
        }

        // include the "local" entity ID in the Target dropdown (for multi-target commands)
        if (includeLocal) {
            entityIdDropdown.addItem(EntityHolder.LOCAL_TARGET_ID);
            if (StringUtils.equalsIgnoreCase(entityId, EntityHolder.LOCAL_TARGET_ID)) {
                entityIdDropdown.setSelectedItem(entityId);

                entityPropertyIdDropdown.removeAllItems();

                // TODO - on save, make sure "this-prop" is a valid property
                if (propertyId == null) {
                    propertyId = "this-prop";
                }
                entityPropertyIdDropdown.addItem(propertyId);
                entityPropertyIdDropdown.setSelectedItem(propertyId);
            }
        }

    }

    private static void updatePropertyIdDropdown(JComboBox<String> entityPropertyIdDropdown, Entity entity, String propertyId) {
        entityPropertyIdDropdown.removeAllItems();
        entityPropertyIdDropdown.addItem(" ");
        entityPropertyIdDropdown.setSelectedIndex(-1);

        if (entity.getProperties() != null) {
            for (Property property : entity.getProperties()) {
                String propertyName = property.getPropertyId();
                entityPropertyIdDropdown.addItem(propertyName);
                if (propertyId != null && propertyName.equalsIgnoreCase(propertyId)) {
                    entityPropertyIdDropdown.setSelectedItem(propertyName);
                }
            }
        }
    }

}
