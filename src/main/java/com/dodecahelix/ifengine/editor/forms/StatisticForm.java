/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.label.WebLabel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.Statistic;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextArea;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;

public class StatisticForm extends AbstractForm {

    private static final long serialVersionUID = 7865444888508026521L;

    WebTextField propertyIdField;
    WebTextField categoryField;
    WebTextField displayNameField;
    WebTextArea descriptionField;
    WebTextField defaultValueField;
    WebTextField minValueField;
    WebTextField maxValueField;

    public StatisticForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        propertyIdField = new TattletaleWebTextField();
        this.add(new WebLabel("Property ID:"), "");
        this.add(propertyIdField, "wrap");

        categoryField = new TattletaleWebTextField();
        this.add(new WebLabel("Category:"), "");
        this.add(categoryField, "wrap");

        displayNameField = new TattletaleWebTextField();
        this.add(new WebLabel("Display Name:"), "");
        this.add(displayNameField, "wrap");

        descriptionField = new TattletaleWebTextArea();
        descriptionField.setWrapStyleWord(true);
        descriptionField.setLineWrap(true);
        WebScrollPane descriptionScroll = new WebScrollPane(descriptionField);
        this.add(new WebLabel("Description:"), "");
        this.add(descriptionScroll, "wrap");

        defaultValueField = new TattletaleWebTextField();
        this.add(new WebLabel("Default Value:"), "");
        this.add(defaultValueField, "wrap");

        minValueField = new TattletaleWebTextField();
        this.add(new WebLabel("Min Value:"), "");
        this.add(minValueField, "wrap");

        maxValueField = new TattletaleWebTextField();
        this.add(new WebLabel("Max Value:"), "");
        this.add(maxValueField, "wrap");
    }

    public void load(Statistic stat) {
        propertyIdField.setText(stat.getPropertyId());
        displayNameField.setText(stat.getDisplayName());
        descriptionField.setText(stat.getDescription());
        categoryField.setText(stat.getCategory());
        defaultValueField.setText(String.valueOf(stat.getDefaultValue()));
        minValueField.setText(String.valueOf(stat.getMinValue()));
        maxValueField.setText(String.valueOf(stat.getMaxValue()));
    }

}
