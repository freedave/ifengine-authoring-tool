/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import java.util.List;

import com.alee.laf.label.WebLabel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.events.EntityIdChangeEvent;
import com.dodecahelix.ifengine.editor.forms.components.SpinnerField;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextArea;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;
import com.dodecahelix.ifengine.main.Events;
import com.dodecahelix.ifengine.util.StringUtils;

public abstract class AbstractEntityForm<T extends Entity> extends AbstractForm {

    private static final long serialVersionUID = 2701646013244467006L;

    protected T loadedEntity;
    protected Environment environment;

    WebTextField idField;
    WebTextField titleField;
    WebTextArea descriptionField;

    SpinnerField carrySizeField;

    public AbstractEntityForm(String titleText, boolean showCarryFields) {
        super(titleText);

        if (showCarryFields) {
            form.add(carrySizeField, "span 2, wrap");
        }
    }

    @Override
    public void buildForm() {
        super.buildForm();

        idField = new TattletaleWebTextField();
        titleField = new TattletaleWebTextField();
        descriptionField = new TattletaleWebTextArea(5, 10);

        descriptionField.setWrapStyleWord(true);
        descriptionField.setLineWrap(true);
        WebScrollPane descriptionScroll = new WebScrollPane(descriptionField);
        //descriptionScroll.setPreferredSize ( new Dimension ( 200, 150 ) );

        form.add(new WebLabel("Id:"), "");
        form.add(idField, "wrap");

        form.add(new WebLabel("Title:"), "");
        form.add(titleField, "wrap 15");

        form.add(new WebLabel("Description:"), "span 2, wrap");
        form.add(descriptionScroll, "span 2, growx, wrap 15");

        carrySizeField = new SpinnerField("Carry Capacity: ", "<html>Upper limit on the sum of all item sizes<br>held on or in this entity.  Set to zero<br>if this is not a container.</html>", 4);
    }

    @Override
    public void clearForm() {
        super.clearForm();

        idField.clear();
        titleField.clear();
        descriptionField.clear();

        carrySizeField.setValue(0);
    }

    @Override
    public void saveForm() {
        super.saveForm();

        // if the idField has changed, update all references to this ID!
        String originalId = loadedEntity.getId();
        String newId = idField.getText();
        if (!originalId.equalsIgnoreCase(newId)) {
            Events.getInstance().getBus().post(new EntityIdChangeEvent(loadedEntity.getEntityType(), originalId, newId));
        }

        loadedEntity.setId(idField.getText());
        loadedEntity.setTitle(titleField.getText());
        loadedEntity.setDescription(descriptionField.getText());

        loadedEntity.setCarrySize(carrySizeField.getValue());
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        load(loadedEntity, environment);
    }

    public void load(T entity, Environment environment) {
        clearForm();

        this.loadedEntity = entity;
        this.environment = environment;

        carrySizeField.setValue(entity.getCarrySize());

        this.idField.setText(entity.getId());
        this.titleField.setText(entity.getTitle());
        this.descriptionField.setText(entity.getDescription());
    }

    @Override
    protected List<String> validateFormEntry() {
        List<String> validationMessages = super.validateFormEntry();

        // verify that no other type has the same ID
        String newId = idField.getText();
        String oldId = loadedEntity.getId();
        if (environment != null && !StringUtils.equalsIgnoreCase(newId, oldId)) {
            logger.debug("changing {} id from {} to {}", loadedEntity.getEntityType(), oldId, newId);
            // ID change.  Check to see if the ID is already taken
            try {
                EnvironmentDAO simpleDao = new SimpleEnvironmentDAO(environment, false);
                EntityType loadType = loadedEntity.getEntityType();
                if (loadType == EntityType.READER) {
                    // TODO - need to clean up narrator/reader references.  This is ambiguous
                    loadType = EntityType.NARRATOR;
                }
                Entity duplicate = simpleDao.getEntityById(loadType, newId);
                if (duplicate != null) {
                    validationMessages.add("another " + loadType.getAbbreviation() + " with this ID already exists");
                }
            } catch (Exception e) {
                e.printStackTrace();
                validationMessages.add("exception thrown : " + e.getMessage());
            }
        }

        return validationMessages;
    }


}
