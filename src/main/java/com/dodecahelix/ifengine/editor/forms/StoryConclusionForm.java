package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.label.WebLabel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.story.Conclusion;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextArea;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;

/**
 * Created on 8/1/2016.
 */
public class StoryConclusionForm extends AbstractForm {

    private static final long serialVersionUID = 583227605450908615L;

    private Conclusion conclusion;

    private WebTextField conclusionIdField;
    private WebTextArea conclusionTextField;

    public StoryConclusionForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        conclusionIdField = new TattletaleWebTextField();

        conclusionTextField = new TattletaleWebTextArea(15, 10);
        conclusionTextField.setWrapStyleWord(true);
        conclusionTextField.setLineWrap(true);
        WebScrollPane passageTextScroll = new WebScrollPane(conclusionTextField);

        form.add(new WebLabel("Conclusion ID:"), "");
        form.add(conclusionIdField, "wrap");

        form.add(new WebLabel("Conclusion Text:"), "wrap");
        form.add(passageTextScroll, "span 2, growx, wrap 15");
    }

    @Override
    public void clearForm() {
        super.clearForm();

        conclusionIdField.clear();
        conclusionTextField.clear();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        conclusion.setConclusionId(conclusionIdField.getText());
        conclusion.setConclusionText(conclusionTextField.getText());
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        load(conclusion);
    }

    public void load(Conclusion storyElement) {
        this.conclusion = storyElement;

        this.clearForm();

        conclusionIdField.setText(storyElement.getConclusionId());
        conclusionTextField.setText(storyElement.getConclusionText());
    }

}
