/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.RangeDisplay;
import com.dodecahelix.ifengine.editor.forms.components.OverlayLabel;
import com.dodecahelix.ifengine.editor.forms.components.SpinnerField;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;

public class RangeDisplayForm extends AbstractForm {

    private static final long serialVersionUID = 2375351225826662755L;

    private RangeDisplay loadedRangeDisplay;

    private WebTextField displayField;
    private SpinnerField minRangeField;
    private SpinnerField maxRangeField;

    public RangeDisplayForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        displayField = new TattletaleWebTextField();
        form.add(new OverlayLabel("Display Value: ", "<html>If the value of this property<br>is above or equal to the low value<br>and below or equal to the high value<br>this display value will be substituted in console output</html>"), "");
        form.add(displayField, "wrap 15");

        minRangeField = new SpinnerField("Low Value: ", null, 5);
        maxRangeField = new SpinnerField("High Value: ", null, 5);

        form.add(minRangeField, "span 2, wrap 15");
        form.add(maxRangeField, "span 2, wrap 15");
    }

    public void load(RangeDisplay rangeDisplay) {
        this.loadedRangeDisplay = rangeDisplay;

        this.clearForm();

        this.minRangeField.setValue(rangeDisplay.getLowRange());
        this.maxRangeField.setValue(rangeDisplay.getHighRange());
        this.displayField.setText(rangeDisplay.getDisplay());
    }


    @Override
    public void clearForm() {
        super.clearForm();
        this.minRangeField.setValue(0);
        this.maxRangeField.setValue(0);
        this.displayField.clear();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        loadedRangeDisplay.setLowRange(minRangeField.getValue());
        loadedRangeDisplay.setHighRange(maxRangeField.getValue());
        loadedRangeDisplay.setDisplay(displayField.getText());
    }

}
