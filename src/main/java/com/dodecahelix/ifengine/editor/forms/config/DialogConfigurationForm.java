/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.config;

import com.alee.laf.label.WebLabel;
import com.dodecahelix.ifengine.data.configuration.DialogConfiguration;
import com.dodecahelix.ifengine.editor.forms.AbstractForm;
import com.dodecahelix.ifengine.editor.forms.components.AppendableListField;

public class DialogConfigurationForm extends AbstractForm {

    private static final long serialVersionUID = -2639521492130496956L;

    private DialogConfiguration dialogConfig;
    private AppendableListField dialogTopicsField;

    public DialogConfigurationForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        dialogTopicsField = new AppendableListField();

        form.add(new WebLabel("Dialog Topics:"), "wrap");
        form.add(dialogTopicsField, "span 2, growx");
    }

    @Override
    public void clearForm() {
        super.clearForm();

        dialogTopicsField.clear();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        dialogConfig.setTopics(dialogTopicsField.getValues());
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        load(dialogConfig);
    }

    public void load(DialogConfiguration dialogConfig) {
        this.dialogConfig = dialogConfig;

        this.clearForm();

        dialogTopicsField.loadList(dialogConfig.getTopics());
    }

}
