/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.components;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import com.alee.laf.combobox.WebComboBox;
import com.dodecahelix.ifengine.editor.events.FormDirtyStatusEvent;
import com.dodecahelix.ifengine.main.Events;

public class TattletaleWebComboBox extends WebComboBox {

    private static final long serialVersionUID = 3800029215081537524L;

    public TattletaleWebComboBox() {
        super();
        this.addChangeListener();
    }

    public TattletaleWebComboBox(Object[] values) {
        super(values);

        this.addChangeListener();
    }

    private void addChangeListener() {
        this.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if ((ItemEvent.SELECTED == e.getStateChange())
                && (TattletaleWebComboBox.this.isFocusOwner())) {
                    Events.getInstance().getBus().post(new FormDirtyStatusEvent(true));
                }
            }
        });
    }

}
