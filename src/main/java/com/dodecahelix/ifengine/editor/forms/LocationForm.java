/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.extended.button.WebSwitch;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.editor.forms.components.OverlayLabel;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebSwitch;

public class LocationForm extends AbstractEntityForm<Location> {

    private static final long serialVersionUID = 5598754608175493344L;

    private WebSwitch isDarkField;

    public LocationForm(String titleText) {
        super(titleText, false);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        isDarkField = new TattletaleWebSwitch();
        form.add(new OverlayLabel("Is Dark: ", "<html>ON for dark, OFF for lit<br>a light source is required for entering a dark location<br>darkness may be set (and reset) by an execution</html>"), "");
        form.add(isDarkField, "w 75!, wrap");
        isDarkField.setSelected(false);
    }

    @Override
    public void load(Location location, Environment environment) {
        super.load(location, environment);

        isDarkField.setSelected(location.isDark());
    }

    @Override
    public void clearForm() {
        super.clearForm();

        isDarkField.setSelected(false);
    }

    @Override
    public void saveForm() {
        super.saveForm();

        loadedEntity.setDark(isDarkField.isSelected());
    }


}
