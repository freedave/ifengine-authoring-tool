/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;
import com.dodecahelix.ifengine.editor.forms.entity.EntityIdComboboxItemRenderer;

public class ExitForm extends AbstractForm {

    private static final long serialVersionUID = -1803329005683153479L;

    private Exit loadedExit;
    private Environment environment;

    WebTextField exitNameTextField;
    WebComboBox toLocationDropdown;

    public ExitForm(String title) {
        super(title);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void buildForm() {
        super.buildForm();

        exitNameTextField = new TattletaleWebTextField();
        form.add(new WebLabel("Exit Name:"), "");
        form.add(exitNameTextField, "wrap");

        toLocationDropdown = new TattletaleWebComboBox();
        toLocationDropdown.setRenderer(new EntityIdComboboxItemRenderer());
        form.add(new WebLabel("To Location:"), "");
        form.add(toLocationDropdown, "wrap");
    }


    @SuppressWarnings("unchecked")
    public void load(Exit exit, Environment environment) {
        this.loadedExit = exit;
        this.environment = environment;

        this.clearForm();

        toLocationDropdown.removeAllItems();
        exitNameTextField.setText(exit.getExitName());

        for (Location location : environment.getLocations()) {
            toLocationDropdown.addItem(location);
            if (exit.getToLocationId() != null && exit.getToLocationId().equalsIgnoreCase(location.getId())) {
                toLocationDropdown.setSelectedItem(location);
            }
        }
    }

    @Override
    public void saveForm() {
        super.saveForm();

        Location location = (Location) toLocationDropdown.getSelectedItem();

        // must have a location, null is unacceptable
        this.loadedExit.setToLocationId(location.getId());
        this.loadedExit.setExitName(exitNameTextField.getText());
    }

    @Override
    protected void resetForm() {
        super.resetForm();
        this.load(loadedExit, environment);
    }


}
