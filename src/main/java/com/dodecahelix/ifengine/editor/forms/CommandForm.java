/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import java.util.List;

import javax.swing.SpinnerNumberModel;

import com.alee.extended.button.WebSwitch;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.spinner.WebSpinner;
import com.alee.laf.text.WebTextArea;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.command.defaults.BuiltinCommandBuilder;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.CommandType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.editor.forms.components.OverlayLabel;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebSpinner;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebSwitch;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextArea;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;
import com.dodecahelix.ifengine.editor.forms.layout.FormWebPanel;
import com.dodecahelix.ifengine.editor.forms.layout.LayoutHelper;
import com.dodecahelix.ifengine.util.StringUtils;

public class CommandForm extends AbstractForm {

    private static final long serialVersionUID = -4545990541108098601L;

    private Command loadedCommand;
    private Environment loadedEnvironment;

    WebTextField referenceIdField;
    WebComboBox overrideField;
    WebTextField displayField;
    WebSpinner timeToExecuteField;
    WebTextArea defaultExecutionMessageField;
    WebComboBox commandTypeField;

    WebSwitch multiTargetCheckbox;

    public CommandForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        referenceIdField = new TattletaleWebTextField();
        overrideField = new TattletaleWebComboBox();

        displayField = new TattletaleWebTextField();
        SpinnerNumberModel timeModel = new SpinnerNumberModel();
        timeModel.setMaximum(new Integer(100));
        timeToExecuteField = new TattletaleWebSpinner(timeModel);

        defaultExecutionMessageField = new TattletaleWebTextArea(5, 10);

        defaultExecutionMessageField.setWrapStyleWord(true);
        defaultExecutionMessageField.setLineWrap(true);
        WebScrollPane messageScroll = new WebScrollPane(defaultExecutionMessageField);

        form.add(new WebLabel("Reference ID:"), "");
        form.add(referenceIdField, "growx, wrap");

        form.add(new OverlayLabel("Overrides: ", "<html>Select to override an entity-type command (by reference ID)</html>"), "");
        form.add(overrideField, "growx, wrap");

        form.add(new WebLabel("Display:"), "");
        form.add(displayField, "growx, wrap");

        commandTypeField = new TattletaleWebComboBox(CommandType.values());
        form.add(new OverlayLabel("Type: ", "<html>Type will determine the icon for this command<br>and where it appears in the option tree</html>"), "");
        form.add(commandTypeField, "growx, wrap");

        this.addFormSeparator();

        form.add(new OverlayLabel("Default Message: ", "<html>If no alternative outcomes are defined,<br>or no alternative outcomes are valid<br>this message will be displayed<br>and the default executions will run</html>"), "wrap");
        form.add(messageScroll, "span 2, growx, wrap 15");

        WebPanel timePanel = new FormWebPanel(LayoutHelper.getPadlessFormLayout(), 0);
        timePanel.add(new WebLabel("Time to Execute (in increments):"), "");
        timePanel.add(timeToExecuteField, "wrap");
        form.add(timePanel, "span 2, wrap 15");

        multiTargetCheckbox = new TattletaleWebSwitch();
        form.add(new OverlayLabel("Multi Target: ", "<html>select if this command can apply to multiple targets<br>such as for any person in the room</html>"), "");
        form.add(multiTargetCheckbox, "w 75!, wrap");
        multiTargetCheckbox.setSelected(false);
    }

    public void load(Command command, Environment environment) {
        this.loadedCommand = command;
        this.loadedEnvironment = environment;

        this.clearForm();
        this.loadReferences();

        referenceIdField.setText(command.getReferenceId());
        if (!StringUtils.isEmpty(command.getOverrideId())) {
            overrideField.setSelectedItem(command.getOverrideId());
        } else {
            overrideField.setSelectedIndex(0);
        }

        displayField.setText(command.getCommandDisplay());
        defaultExecutionMessageField.setText(command.getDefaultExecutionMessage());

        timeToExecuteField.setValue(command.getTimeToExecute());
        multiTargetCheckbox.setSelected(command.isMultiTarget());

        commandTypeField.setSelectedItem(command.getCommandType());
    }

    @SuppressWarnings("unchecked")
    private void loadReferences() {
        loadReferences(loadedEnvironment);

        // How to add the default commands when they aren't even loaded yet??
        BuiltinCommandBuilder bcb = new BuiltinCommandBuilder();
        Environment emptyEnvironment = new Environment();
        // prevents a NPE when building intro command
        emptyEnvironment.getStoryContent().setIntroduction("");
        bcb.buildCommands(emptyEnvironment);

        loadReferences(emptyEnvironment);

        overrideField.insertItemAt(" ", 0);
    }

    private void loadReferences(Environment environment) {
        CommandConfiguration commandConfig = environment.getConfiguration().getCommandConfiguration();

        loadCommandReferences(commandConfig.getItemCommands());
        loadCommandReferences(commandConfig.getLocationCommands());
        loadCommandReferences(commandConfig.getMassCommands());
        loadCommandReferences(commandConfig.getNarratorCommands());
        loadCommandReferences(commandConfig.getPeopleCommands());
    }

    @SuppressWarnings("unchecked")
    private void loadCommandReferences(List<Command> commands) {
        for (Command command : commands) {
            if (!StringUtils.isEmpty(command.getReferenceId())) {
                overrideField.addItem(command.getReferenceId());
            }
        }
    }

    @Override
    public void clearForm() {
        super.clearForm();

        commandTypeField.setSelectedIndex(-1);

        referenceIdField.clear();
        overrideField.removeAllItems();

        displayField.clear();
        defaultExecutionMessageField.clear();

        timeToExecuteField.setValue(0);
        multiTargetCheckbox.setSelected(false);
    }

    @Override
    public void saveForm() {
        super.saveForm();

        this.loadedCommand.setReferenceId(referenceIdField.getText());
        this.loadedCommand.setCommandDisplay(displayField.getText());
        this.loadedCommand.setDefaultExecutionMessage(defaultExecutionMessageField.getText());
        this.loadedCommand.setTimeToExecute(Integer.parseInt(timeToExecuteField.getValue().toString()));
        this.loadedCommand.setMultiTarget(multiTargetCheckbox.isSelected());
        this.loadedCommand.setCommandType((CommandType) commandTypeField.getSelectedItem());

        if (!StringUtils.isEmpty((String) overrideField.getSelectedItem())) {
            this.loadedCommand.setOverrideId((String) overrideField.getSelectedItem());
        } else {
            this.loadedCommand.setOverrideId(null);
        }
    }

    @Override
    protected void resetForm() {
        super.resetForm();

        if (loadedCommand != null) {
            this.load(loadedCommand, loadedEnvironment);
        }
    }

}
