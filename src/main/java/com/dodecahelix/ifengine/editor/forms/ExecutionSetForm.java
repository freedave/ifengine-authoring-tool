/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.List;

import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.text.WebTextField;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.editor.forms.components.ComboboxAppendableListField;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebTextField;
import com.dodecahelix.ifengine.editor.forms.layout.FormWebPanel;
import com.dodecahelix.ifengine.util.StringUtils;

public class ExecutionSetForm extends AbstractForm {

    private static final long serialVersionUID = -1273600445979776450L;

    private Environment environment;
    private ExecutionSet executionSet;

    /**
     * there are two types of ExecutionSets - standard sets and shared sets
     */
    private boolean standardSet;

    private CardLayout setTypeCards;

    private WebPanel cardHolderPanel;
    private WebPanel sharedSetPanel;
    private WebPanel standardSetPanel;

    private WebTextField executionSetIdField;
    private ComboboxAppendableListField referenceExecutionSetsField;

    public ExecutionSetForm(String title) {
        super(title);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        setTypeCards = new CardLayout();
        cardHolderPanel = new WebPanel(setTypeCards);
        sharedSetPanel = new FormWebPanel();
        standardSetPanel = new FormWebPanel();
        cardHolderPanel.add(sharedSetPanel, "shared");
        cardHolderPanel.add(standardSetPanel, "standard");

        executionSetIdField = new TattletaleWebTextField();
        referenceExecutionSetsField = new ComboboxAppendableListField();

        sharedSetPanel.add(new WebLabel("Reference ID:"), "");
        sharedSetPanel.add(executionSetIdField, "growy, wrap");

        standardSetPanel.add(new WebLabel("Reference Execution Sets: "), "wrap");
        standardSetPanel.add(referenceExecutionSetsField, "span 2, growx, wrap");

        form.add(cardHolderPanel, "span 2, wrap, growy");
        setTypeCards.show(cardHolderPanel, "standard");
    }

    @Override
    public void clearForm() {
        super.clearForm();

        this.executionSetIdField.clear();
        referenceExecutionSetsField.clear();
    }

    @Override
    public void saveForm() {
        super.saveForm();

        if (standardSet) {
            executionSet.getReferenceExecutions().clear();
            executionSet.getReferenceExecutions().addAll(referenceExecutionSetsField.getValues());
        } else {
            executionSet.setReferenceId(executionSetIdField.getText());
        }

    }

    @Override
    protected void resetForm() {
        super.resetForm();

        load(executionSet, environment);
    }

    public void load(ExecutionSet storyElement, Environment environment) {
        clearForm();

        this.executionSet = storyElement;
        this.environment = environment;

        if (StringUtils.isEmpty(storyElement.getReferenceId())) {
            standardSet = true;
            setTypeCards.show(cardHolderPanel, "standard");

            referenceExecutionSetsField.loadList(storyElement.getReferenceExecutions());

            List<String> options = new ArrayList<String>();
            for (ExecutionSet eset : environment.getSharedExecutions()) {
                options.add(eset.getReferenceId());
            }
            referenceExecutionSetsField.loadOptions(options);
        } else {
            standardSet = false;
            setTypeCards.show(cardHolderPanel, "shared");

            this.executionSetIdField.setText(storyElement.getReferenceId());
        }

    }


}
