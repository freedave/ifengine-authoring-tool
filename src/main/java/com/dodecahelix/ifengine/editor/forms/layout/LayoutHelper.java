/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.layout;

import java.awt.Color;

import net.miginfocom.swing.MigLayout;

public class LayoutHelper {

    public static final Color BG_COLOR = Color.getHSBColor(0.0f, 0.0f, 0.961f);

    public static final int FORM_MARGIN = 10;

    public static MigLayout getDefaultFormLayout() {
        return new MigLayout("fillx",       // Layout Constraints
        "[left]rel[grow,fill]",     // Column constraints
        "[]5[]");
    }

    public static MigLayout getPadlessFormLayout() {
        return new MigLayout("insets 0",    // Layout Constraints
        "[left]rel[]",              // Column constraints
        "[]0[]");
    }

    public static MigLayout getDialogLayout() {
        return new MigLayout("wrap 2");
    }
}
