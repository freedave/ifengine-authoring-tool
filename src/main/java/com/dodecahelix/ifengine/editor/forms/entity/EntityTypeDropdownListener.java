/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms.entity;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import com.alee.laf.combobox.WebComboBox;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;

public class EntityTypeDropdownListener implements ItemListener {

    private Environment environment;

    private WebComboBox entityIdDropdown;
    private WebComboBox entityPropertyDropdown;

    // whether to include the "this" in entity ID dropdown
    private boolean includeThis;

    // whether to include the "local" in entity ID dropdown
    private boolean includeLocal;

    public EntityTypeDropdownListener(WebComboBox entityIdDropdown,
                                      WebComboBox entityPropertyDropdown,
                                      boolean includeThis,
                                      boolean includeLocal) {
        this.entityIdDropdown = entityIdDropdown;
        this.entityPropertyDropdown = entityPropertyDropdown;

        this.includeThis = includeThis;
        this.includeLocal = includeLocal;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void itemStateChanged(ItemEvent entityTypeChangeEvent) {

        if (entityTypeChangeEvent.getStateChange() == ItemEvent.SELECTED
        && environment != null
        && entityTypeChangeEvent.getItem() != null) {

            EntityType entityType = (EntityType) entityTypeChangeEvent.getItem();
            EntityComboBoxHelper.updateEntityIdCombobox(entityIdDropdown,
            entityPropertyDropdown,
            entityType,
            null,
            null,
            environment,
            includeThis,
            includeLocal);
        }
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

}
