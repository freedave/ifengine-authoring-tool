/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.forms;

import com.alee.extended.button.WebSwitch;
import com.alee.laf.combobox.WebComboBox;
import com.alee.laf.label.WebLabel;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.editor.forms.components.CollapsibleFormPanel;
import com.dodecahelix.ifengine.editor.forms.components.OverlayLabel;
import com.dodecahelix.ifengine.editor.forms.components.SpinnerField;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebComboBox;
import com.dodecahelix.ifengine.editor.forms.components.TattletaleWebSwitch;
import com.dodecahelix.ifengine.util.StringUtils;

public class ItemForm extends AbstractEntityForm<Item> {

    private static final long serialVersionUID = -1721635544326083640L;

    SpinnerField sizeField;
    WebComboBox wearPartField;
    CollapsibleFormPanel containerToggle;
    WebSwitch isWornField;
    WebSwitch isLightSourceField;

    public ItemForm(String titleText) {
        super(titleText, true);
    }

    @Override
    public void buildForm() {
        super.buildForm();

        sizeField = new SpinnerField("Size: ", "<html>How much space this item takes up<br>in its parent container.<br>", 4);

        form.add(sizeField, "span 2, wrap 15");

        // wearable section
        wearPartField = new TattletaleWebComboBox();
        form.add(new WebLabel("Wear Part:"), "");
        form.add(wearPartField, "wrap 15");

        isWornField = new TattletaleWebSwitch();
        form.add(new OverlayLabel("Is Worn: ", "<html>select if this item is worn by its current owner<br>(must be a narrator, and wear part must be set)</html>"), "");
        form.add(isWornField, "w 75!, wrap");
        isWornField.setSelected(false);

        isLightSourceField = new TattletaleWebSwitch();
        form.add(new OverlayLabel("Is Light Source: ", "<html>a light source is required for entering a dark location<br>light source may be set (and reset) by an execution</html>"), "");
        form.add(isLightSourceField, "w 75!, wrap");
        isLightSourceField.setSelected(false);
    }


    @SuppressWarnings("unchecked")
    @Override
    public void load(Item item, Environment environment) {
        super.load(item, environment);

        sizeField.setValue(item.getSize());

        String wearBodypart = item.getWearPart();
        for (String bodyPart : environment.getConfiguration().getAnatomyConfiguration().getBodyParts()) {
            wearPartField.addItem(bodyPart);
            if (StringUtils.equalsIgnoreCase(wearBodypart, bodyPart)) {
                wearPartField.setSelectedItem(bodyPart);
            }
        }
        wearPartField.insertItemAt(" ", 0);

        if (wearBodypart == null) {
            wearPartField.setSelectedIndex(-1);
        }

        isWornField.setSelected(item.isWorn());
        isLightSourceField.setSelected(item.isLightSource());
    }

    @Override
    public void clearForm() {
        super.clearForm();

        sizeField.setValue(0);

        wearPartField.removeAllItems();
        wearPartField.setSelectedIndex(-1);
        isWornField.setSelected(false);
        isLightSourceField.setSelected(false);
    }

    @Override
    public void saveForm() {
        super.saveForm();

        loadedEntity.setSize(sizeField.getValue());

        String wearPart = (String) wearPartField.getSelectedItem();
        // this could be an empty space
        if (!StringUtils.isEmpty(wearPart)) {
            loadedEntity.setWearPart(wearPart);
        } else {
            loadedEntity.setWearPart(null);
        }

        loadedEntity.setWorn(isWornField.isSelected());
        loadedEntity.setLightSource(isLightSourceField.isSelected());
    }


}
