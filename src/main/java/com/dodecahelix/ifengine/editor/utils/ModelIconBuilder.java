/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.utils;

import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.Schedule;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.data.configuration.Configuration;
import com.dodecahelix.ifengine.data.configuration.DialogConfiguration;
import com.dodecahelix.ifengine.data.configuration.TimeConfiguration;
import com.dodecahelix.ifengine.data.story.Conclusion;
import com.dodecahelix.ifengine.data.story.Passage;

public class ModelIconBuilder {

    public static Icon getIconForModelClass(Class<?> modelClass) {
        URL imgUrl = ModelIconBuilder.class.getClassLoader().getResource(getIconPath(modelClass));
        ImageIcon defaultIcon = new ImageIcon(imgUrl);
        return defaultIcon;
    }

    public static String getIconPath(Class<?> modelClass) {
        String path = "icons/bullet_black.png";

        if (Environment.class == modelClass) {
            path = "icons/world.png";
        }
        if (Person.class == modelClass) {
            path = "icons/user_green.png";
        }
        if (Location.class == modelClass) {
            path = "icons/house.png";
        }
        if (Mass.class == modelClass) {
            path = "icons/shape_square.png";
        }
        if (Item.class == modelClass) {
            path = "icons/star.png";
        }
        if (Command.class == modelClass) {
            path = "icons/hand.png";
        }
        if (Execution.class == modelClass) {
            path = "icons/lightning.png";
        }
        if (Reader.class == modelClass) {
            path = "icons/status_online.png";
        }
        if (Dialog.class == modelClass) {
            path = "icons/comment.png";
        }
        if (Exit.class == modelClass) {
            path = "icons/door_in.png";
        }
        if (Schedule.class == modelClass) {
            path = "icons/clock.png";
        }
        if (Configuration.class == modelClass) {
            path = "icons/screwdriver.png";
        }
        if (TimeConfiguration.class == modelClass) {
            path = "icons/clock.png";
        }
        if (DialogConfiguration.class == modelClass) {
            path = "icons/comment.png";
        }
        if (CommandConfiguration.class == modelClass) {
            path = "icons/hand.png";
        }
        if (Passage.class == modelClass) {
            path = "icons/book_open.png";
        }
        if (Conclusion.class == modelClass) {
            path = "icons/book.png";
        }

        return path;
    }

}
