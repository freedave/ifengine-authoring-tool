/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.ifengine.editor.utils;

import java.io.InputStream;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.editor.system.EnvironmentFileBuilder;

public class DefaultEnvironmentBuilder {

    private static final String DEFAULT_ENVIRONMENT_NAME = "env/default.json";

    public static Environment build() {
        InputStream is = DefaultEnvironmentBuilder.class.getClassLoader().getResourceAsStream(DEFAULT_ENVIRONMENT_NAME);
        return EnvironmentFileBuilder.buildEnvironmentFromStream(is);
    }

}
