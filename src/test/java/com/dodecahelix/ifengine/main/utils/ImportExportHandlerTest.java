package com.dodecahelix.ifengine.main.utils;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.editor.utils.DefaultEnvironmentBuilder;
import com.dodecahelix.ifengine.main.ix.ExportHandler;
import com.dodecahelix.ifengine.main.ix.ImportHandler;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created on 10/13/2016.
 */
public class ImportExportHandlerTest {

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void testExportAndImport() throws IOException {
        Environment environment = DefaultEnvironmentBuilder.build();
        File exportFolder = tempFolder.newFolder("exports");
        File file = new File(exportFolder, "export.txt");

        environment.addItem(new Item("pencil", "A number two pencil."));

        ExportHandler.exportEnvironmentToFile(environment, file);

        environment = ImportHandler.importEnvironmentEntitiesFromFile(environment, file);

        boolean foundPencil = false;
        for (Item item : environment.getItems()) {
            if ("pencil".equalsIgnoreCase(item.getTitle())) {
                foundPencil = true;
                assertEquals("A number two pencil.", item.getDescription());
            }
        }
        assertTrue(foundPencil);
    }

}
