# IF-Engine Authoring Tool #

### What is this repository for? ###

* The IF-Engine Authoring Tool is a java swing application for creating the interactive fiction story files used by the [IF-Engine](https://bitbucket.org/freedave/ddx-ifengine) library.
* Version 1.0.0

### How do I get set up? ###

* This is a java application based on JRE 1.7+
* Maven 3+ is required to build

```
mvn clean package
cd target
java -jar ifengine-authoring-1.0.0-SNAPSHOT-jar-with-dependencies.jar
```

* Dependencies:
    * [WebLAF - Web Look and Feel](http://weblookandfeel.com/) version 1.27 : Swing look and feel library
    * [MigLayout](http://www.miglayout.com/) version 3.7.3.1 : library for Swing component layout
    * [Google GSON](https://github.com/google/gson) version 2.2.4 - for model data serialization to JSON format
    * [Google Guava](https://github.com/google/guava) version 17.0
    * [SLF4J](http://www.slf4j.org/) version 1.7.13 - logging facade
    * [J-Unit](http://junit.org/) version 4.11 - unit testing
    * [Mockito](http://mockito.org/) version 1.9.5 - mocks for unit testing
    * [Hamcrest](http://hamcrest.org/) version 1.3 - matchers for unit testing


### Generating windows and mac executables ###

This project uses [libgdx/packr](https://github.com/libgdx/packr) to create windows and mac binaries with a bundled JRE, so that users do not need to install java.  Packr is run from one of the three ".bat" files in the root directory. dependent upon the target platform, and generates its output in the out-<platform> directory.  The resultant executable can be debugged using the "--cli --console" arguments:

```
ifengine-authoring-tool.exe --cli --console
```

### Contribution guidelines ###

* If you are interested in contributing to this codebase, please contact freedavep@gmail.com

### Who can I contact concerning this code? ###

* Contact : freedavep@gmail.com